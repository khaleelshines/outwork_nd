import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmailProcessApiService } from 'src/services/emailintegration.services';

declare var gapi: any;
@Component({
  selector: 'app-oauth-email',
  templateUrl: './oauth-email.component.html',
  styleUrls: ['./oauth-email.component.css']
})

export class OauthEmailComponent implements OnInit {
  auth2:any;
  userContacts:any;
  transformToMailListModel:any;
  credential:any;
  authCode:any;
  authResponse:any;
  constructor(private route: ActivatedRoute,public emailService: EmailProcessApiService) { }

  ngOnInit() : void {
   setTimeout(() => this.signIn(), 1000);
  }

// signIn() {
//   gapi.load('auth2', () => {
//       this.auth2 = gapi.auth2.init({
//           client_id: '724400070494-a8t7vdojq9iis8efugmr5t3ga3ejmo9h.apps.googleusercontent.com',
//           //cookie_policy: 'single_host_origin',
//          // approval_prompt='force',
//           access_type: 'offline',
//           //grant_type: 'authorization_code',
//           //response_type: 'code',
//           redirect_uri:'http://localhost:4200',
//           scope: 'https://www.googleapis.com/auth/gmail.readonly'
//       });
//       this.auth2.attachClickHandler(document.getElementById('googleres'), {}, this.onSignIn, this.onFailure);
//       this.auth2.grantOfflineAccess();
//       console.log(this.auth2);
//   })
// }

signIn() {
  this.authCode=this.route.snapshot.queryParams.code;
  console.log(this.authCode);
if(this.authCode!=null||this.authCode!=''){
  this.emailService.CreateEmailOauthDetails(this.authCode)
    .subscribe(data => {
     if(data.Status === 'Success'&&data.Data!=""){
       console.log(data.Data);
     }
     else
     alert('Something went wrong while creating');
    });
  }
else{
  alert('Something went wrong');
}
}

// onSignIn = (data: any) => {
//   //console.log('data is' + data);
//   if(data!=""){

//   }
//   this.credential={
//     AccessToken:data.Zi.access_token,
//     ExpiresAt:data.Zi.expires_at,
//     ExpiresIn:data.Zi.expires_in,
//     IssuedDateUTC:data.Zi.first_issued_at,
//     IdToken:data.Zi.id_token,
//     IdpId:data.Zi.idpId,
//     LoginHint:data.Zi.login_hint,
//     Scope:data.Zi.scope,
//     TokenType:data.Zi.token_type,  
//     Email:data.w3.U3,
//     Code: this.route.snapshot.paramMap.get("code")
//   };
//   console.log(this.credential);
//   //setTimeout(() => this.fetchmail(), 1000);
// }



// onFailure=(data:any)=>{
//   console.log(data);
// }


}
