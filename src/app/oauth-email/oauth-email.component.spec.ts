import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OauthEmailComponent } from './oauth-email.component';

describe('OauthEmailComponent', () => {
  let component: OauthEmailComponent;
  let fixture: ComponentFixture<OauthEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OauthEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OauthEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
