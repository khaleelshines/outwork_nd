import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/userservice.services';
import { UtilitiesService } from "src/services/utilities.services";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  invalidLogin = false;
  submitted = false;
  get f() { return this.registerForm.controls; }
  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService,
    public utilities: UtilitiesService) { }
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      loginId:  ['', [Validators.required, Validators.email,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', [Validators.required]],
      companyName: ['', [Validators.required]],
      lastName: [],
      // Email: ['', [Validators.required, Validators.email,
      //   Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]]
    });
  }
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.userService.register(this.registerForm.value).subscribe(data => {
      if (data.Code === 'SUC-200') {
        this.router.navigate(['/login']);
      }
      else {

      }
    });
  }


}
