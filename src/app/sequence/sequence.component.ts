import { Component, OnInit, ViewChild, ElementRef, OnDestroy, TemplateRef } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";
import { Router } from "@angular/router";
import { SequenceApiService } from "../../services/sequenceservice.services";
import { AuthService } from "../guards/authguard/auth.service";
import { DataService } from "../../services/DataService";
import { AlertPromise } from "selenium-webdriver";
import { tap } from "rxjs/operators";
import { NgxSpinnerService } from "ngx-spinner";
import { CommonFieldsService } from 'src/services/commonfields.services';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";

import "ag-grid-enterprise";
import { AgGridAngular } from 'ag-grid-angular';
import { IGetRowsParams, IDatasource, GridOptions, CellClickedEvent } from 'ag-grid-community';

@Component({
  selector: "app-sequence",
  templateUrl: "./sequence.component.html",
  styleUrls: ["./sequence.component.css"]
})
export class SequenceComponent implements OnInit,OnDestroy {
  sequenceForm: FormGroup;
  sequenceStepForm: FormGroup;
  Sequences: any;
  selectedSequence: any;
  sequenceSteps: any;
  sequenceStepType: any;
  submitted = false;
  selectedmastertype: string;
  templates: any;
  //#region  sequence form
  selectedIndustryId:number;
  selectedIndustry:string;
  IndustriesList:any;
  selectedDesig:any;
  items:any;
  //#endregion
  subscriptions:Subscription [] = [];

  public gridApi;
  public gridColumnApi
  public columnDefs;
  public defaultColDef;
  public cacheOverflowSize;
  public rowData: any;
  public rowSelection;
  public recordscount: any;
  public contactsData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
    //#region Sequence
    responseInfo:string;
    //#endregion
  
    //#region modal
    modalResponseRef:BsModalRef;
    //#endregion

  @ViewChild("closeAddSequenceModal", { static: false })
  closeAddSequenceModal: ElementRef;
  get f() {
    return this.sequenceForm.controls;
  }
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private apiService: SequenceApiService,
    private auth: AuthService,
    private dataService: DataService,
    private spinner: NgxSpinnerService,
    private commonService:CommonFieldsService,
    private modalBsServiceRef:BsModalService,
  ) {
    this.columnDefs = [
      { headerName: "Sequence Name", field: 'SequenceName', sortable: true, filter: 'agTextColumnFilter', width:380,
        filterParams: { suppressAndOrCondition: true },
        cellRenderer: (data) => { if (data.value != undefined) { return '<a href="javascript:void(0);">' + data.value + '</a>'; } }
      },
      {headerName: "Steps", field: "StepsCount", sortable: true, width:150, },
      {headerName: "Status", field: "SequenceStatus",
      filter:true,
      cellStyle: function(params) {  
        if(params.value == "Active") {
          return { 'background-color': '#82d249','color':'#fff' };
        }else if(params.value == 'Completed'){
          return { 'background-color': '#ffb300','color':'#fff' };
        } else{
          return { 'background-color': '#fb7065','color':'#fff' };
        }   
        
      },
       sortable: true, width:120, },
      { headerName: "Contacts", field: "Subscribers", sortable: true, width:150, },
      { headerName: "Reached", field: "Reached", sortable: true, width:150, },
      { headerName: "Opened", field: "Opened", sortable: true, width:150, },
      { headerName: "Responses", field: "Responses", sortable: true, width:150, },
      { headerName: "Created On", field: "ElapsedDuration", sortable: true, width:200, }
    ];

    this.defaultColDef = {
        editable: true,
        enableRowGroup: true,
        enablePivot: false,
        enableValue: true,
        sortable: true,
        resizable: true,
        filter: true,
    };
  }
  
  gridOptions: GridOptions = {
    pagination: true,
    cacheBlockSize: 10,
    paginationPageSize: 10,
    rowHeight: 50,
    headerHeight: 52, 
  }; 

  onSelect(sequnceData: any): void {
    this.selectedSequence = sequnceData;
    this.dataService.setOptionForSequenceSteps(
      "sequencestepid",
      this.selectedSequence.SequenceIdInt
    );
    this.router.navigate([
      "/viewsequence/" + this.selectedSequence.SequenceIdInt
    ]);
  }

  ngOnInit() {
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 2000);

    this.DesignationItems();
    this.getIndustriesList();
    if (!window.localStorage.getItem("token")) {
      this.router.navigate(["login"]);
    }
    this.sequenceForm = this.formBuilder.group({
      SequenceName: ["", Validators.compose([Validators.required])],
      Description: [],      
      designationids:[],
      industryid: ["", Validators.required], //this.selectedIndustryId
      Priority: ["", Validators.compose([Validators.required])],
      industry:[]
    });
    this.sequenceStepForm = this.formBuilder.group({});
    this.getSequences();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }

  selectIndustryChangeHandler(event: any) {
    this.selectedIndustryId = event.target.value;
    let selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
    this.selectedIndustry = selectElementText;
  }

  getIndustriesList(){
 let industriesSub =   this.commonService.getCommonFields(1,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.IndustriesList = JSON.parse(data.Data);
      }
     });
     this.subscriptions.push(industriesSub);
  }

  DesignationItems(){
    let designationArray =[];
   let designationSub = this.commonService.getCommonFields(2,"").subscribe(data=>{
      if (data.Status === "Success") {
        let designationItems  = JSON.parse(data.Data);
        designationItems.forEach(element => {
          let singleItem ={
            id :element.id,
            name:element.labeltitle
          };
          designationArray.push(singleItem);
        });
        this.items = designationArray;
      }
     });
     this.subscriptions.push(designationSub);
    }

    getValues() {
      console.log(this.selectedDesig);   
    }

  addsequence(template: TemplateRef<any>) {
    this.submitted = true;
    let designationArray = this.selectedDesig;
    let designationIds="";
    if(designationArray != undefined && designationArray != ""){
    designationArray.forEach(element => {
      designationIds += element.id+",";
    });
    designationIds = designationIds.replace(/,\s*$/, "");
  }
    this.sequenceForm.patchValue({ designationids: designationIds });
    this.sequenceForm.patchValue({ industry: this.selectedIndustry });
    if (this.sequenceForm.invalid) {
      return;
    }
    this.apiService.createSequence(this.sequenceForm.value).subscribe(data => {
      if (data.Code === "SUC-200") {
        this.getSequences();        
        this.onSeqReset();
      }else if(data.Code === 'ERR-4001' && data.Data === 'duplicate'){
        this.responseInfo = `<div class="alert alert-danger mb-0">     
        <h6>Sequence name is already exists, Please try with another one..!</h6> 
              </div> `;  
      this.modalResponseRef = this.modalBsServiceRef.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 3000);     
      }else{
        this.responseInfo = `<div class="alert alert-danger mb-0">     
        <h6>Failed to add sequence..!</h6> 
              </div> `;  
      this.modalResponseRef = this.modalBsServiceRef.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 3000);    
      }
      this.closeAddSequenceModal.nativeElement.click();
       
    });
  }

  onSeqReset() {
    this.submitted = false;
    this.sequenceForm.reset();
  }
  stepSelection() {
    this.apiService.getSequenceMasterStepType().subscribe(data => {
      if (data.Status === "Success") {
        this.sequenceStepType = JSON.parse(data.Data);
       
      }
    });
  }

  dataChanged(obj) {
    this.apiService.getTemplate("contacttemplate").subscribe(data => {
      if (data.Status === "Success") {
        this.templates = JSON.parse(data.Data);
        console.log("Completed");
      }
    });
  }

  public getSequences(): any {
  let sequencesSub =  this.apiService.getSequences().subscribe(data => {
      this.Sequences = data;
    });
    this.subscriptions.push(sequencesSub);
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
  
  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText != "") {
      this.gridOptions.api.setQuickFilter(searchText);
    } else {
      this.gridOptions.api.setQuickFilter(null);
      // this.gridOptions.api.onFilterChanged();
    }
  }

  
  onCellClicked($event: CellClickedEvent) {
    let id = $event.data.SequenceIdInt;
    if($event.colDef.field == "SequenceName"){    
    this.router.navigate(['/viewsequence/'+id]);
    }
  }    
}