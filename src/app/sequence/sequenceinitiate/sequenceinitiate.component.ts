import { Component, OnInit, OnDestroy, TemplateRef, ViewChild } from '@angular/core';
import { AccountApiService } from 'src/services/accounts.services';
import { FormBuilder, Validators } from '@angular/forms';
import { SequenceApiService } from 'src/services/sequenceservice.services';
import { Router } from '@angular/router';
import { GridOptions, CellClickedEvent } from 'ag-grid-community';
import { CommonFieldsService } from 'src/services/commonfields.services';
import { Subscription } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { AgGridAngular } from 'ag-grid-angular';
import { AuthService } from 'src/app/guards/authguard/auth.service';


@Component({
  selector: 'app-sequenceinitiate',
  templateUrl: './sequenceinitiate.component.html',
  styleUrls: ['./sequenceinitiate.component.css']
})
export class SequenceinitiateComponent implements OnInit,OnDestroy {
  MasterTypes:any;
  SubTypes:any;
  IndustriesList:any;
  SequencesList:any;
  SelectedIndusrtyName: string;
  sequenceIntId:number;
  disabled:boolean= true;
  disabledFetchContacts:boolean = true;
  initiateBtnDisabled:boolean= false;
  IsSequenceContainerHidden:boolean=true;
  IsContactsListHidden: boolean = true;
  initiateModalRef: BsModalRef;
  initiateCountInfo:string;
  finalInitiateContactsList:any;
  isInitiatedBtnVisible:boolean = false;
  selectedIndustry:string;
  selectedSubType:string;
  contactsCount:number;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowGroupPanelShow;
  public pivotPanelShow;
  public sideBar;
  public rowData: any;
  public contactsData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
  items:any;
  selected:any;
  selectedDesignations:any;
  IsChecked:boolean = false;
  IsContactRelatedItems:boolean = false;
  fetchType = 'account';
  //#region sequence initiage
  timerCount:number;
  timeInterval;
  isTimerStart=false;
  campaigntypeList:any;
  selectedFetchType:string;
  contactFetchValue:string;
  IsProceedBtnEnabled:boolean = true;
  checkboxSelectionCount:number = 0;
  //#endregion
  subscriptions:Subscription [] = [];
  @ViewChild('agGrid', { static: false }) agGrid: AgGridAngular;
  constructor(
  private accountsService:AccountApiService,
  private formBuilder: FormBuilder,
  private sequenceService:SequenceApiService,
  private router:Router,
  private commonService:CommonFieldsService,
  private modalService: BsModalService,
  private spinner: NgxSpinnerService,
  private auth:AuthService
  ) { 

    this.sideBar = {
      toolPanels: [
        {
          id: "columns",
          labelDefault: "Columns",
          labelKey: "columns",
          iconKey: "columns",
          toolPanel: "agColumnsToolPanel"
        },
        {
          id: "filters",
          labelDefault: "Filters",
          labelKey: "filters",
          iconKey: "filter",
          toolPanel: "agFiltersToolPanel"
        }
      ],
      defaultToolPanel: ""
    };
    this.columnDefs = [     
  //     {
  //       field: 'RowSelect',
  //  headerName: ' ',
  //  checkboxSelection: true,
  //  suppressMenu: true,
  //  suppressSorting: true,
  //  headerCheckboxSelection: true,

  //     },
      {
        headerName: "Contact Name",
        field: "contactname",    
        headerCheckboxSelection: true,    
        checkboxSelection: true,
      },
      {
        headerName: "Account Name",
        field: "AccountName"
      },   
      {
        headerName: "Industry",
        field: "Industry"
      },
      {
        headerName: "Campaign Type",
        field: "campaigntype"
      },       
      // {
      //   headerName: "No Of Contacted",
      //   field: "NoOfContacted"
      // },
       {
        headerName: "Email",
        field: "email",
      },     
      {
        headerName: "Stage",
        field: "stage",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: {
          values: [
            "Not Started",
            "Cold",
            "Replied",
            "Unresponsive",
            "do not contact",
            "Bad Contact Info",
            "Interested",
            "Not Interested"
          ]
        }
      },   
      
    ];
    this.autoGroupColumnDef = {
      headerName: "Group",
      //  width: 200,
      
      field: "customername",
      valueGetter: function(params) {
        if (params.node.group) {
          return params.node.key;
        } else {
          return params.data[params.colDef.field];
        }
      },
      headerCheckboxSelection: true,
      cellRenderer: "agGroupCellRenderer",
      cellRendererParams: { checkbox: true }
    };
    this.defaultColDef = {
      editable: false,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true

    };
 
    this.rowSelection = "multiple";
    this.rowGroupPanelShow = "false";
    this.pivotPanelShow = "always";
    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;  

    
  }

  gridOptions: GridOptions = {
    rowHeight: 50,
    headerHeight: 52,   
  };
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  //  this.getContactsData();
  }
  ngOnInit() {  
    
   this.getAccountsMasterTypes();
   this.DesignationItems();
   
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }

  checkValue(event: any) {
    let isChecked = event.target.checked;
    if (isChecked === true) {
      this.IsChecked = true;     
    } else {
      this.IsChecked = false;     
    }
  
  }
 
  DesignationItems(){
    let designationArray =[];
    let designationsSub = this.commonService.getCommonFields(2,"").subscribe(data=>{
      if (data.Status === "Success") {
        let designationItems  = JSON.parse(data.Data);
        designationItems.forEach(element => {
          let singleItem ={
            id :element.id,
            name:element.labeltitle
          };
          designationArray.push(singleItem);
        });
        this.items = designationArray;
      }
     });
   this.subscriptions.push(designationsSub);
   
  }

  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText != "") {
      this.gridOptions.api.setQuickFilter(searchText);
    } else {
      this.gridOptions.api.setQuickFilter(null);
     
    }
  }

  getValues() {
    console.log(this.selected);   
  }

  IndustryTypeChangedHandler(selectedValue:string) {
    selectedValue = selectedValue.toLowerCase();
    if(selectedValue != "select industry"){
    this.SelectedIndusrtyName = selectedValue;
    this.disabled = false;
    this.IsContactsListHidden = true;
    }else{
      this.disabled = true;
            
    }
    this.disabledFetchContacts = true;
    this.IsSequenceContainerHidden = true;
    this.selectedFetchType = "";
    this.contactFetchValue = "";
    this.IsContactRelatedItems = false;
    this.fetchType = 'account';
  // let subTypeValuesObject = this.MasterTypes.filter(a=>a.type === selectedValue);
  // let subTypesValues = subTypeValuesObject[0].subtypes;
  // this.disabled = true;
  // if(subTypesValues.length > 0){
  //   this.disabled = false;
  // }  
  // this.selectedIndustry = selectedValue;
  // this.SubTypes = subTypesValues;
  }

  SubTypeChangedHandler(selectedValue:string) {
    selectedValue = selectedValue.toLowerCase();
    this.selectedSubType = selectedValue;
  }
  onItemChange(value){
    this.sequenceIntId = value; 
    if(value){
         this.disabledFetchContacts = false;
    }  
 } 


 

  onCellClicked($event: CellClickedEvent) {
    if($event.colDef.field == "RowSelect"){
    let singleRowData = $event.data;
    }
  }
 
  
  //#region Sequence Initiate
  stopInitiating(){
    this.initiateBtnDisabled = false;
    clearInterval(this.timeInterval);
  }
 
  startCountdown(seconds) {
    let counter = seconds; 
    this.isTimerStart = true;     
    this.timeInterval = setInterval(() => {     
      counter--;
      this.timerCount = counter;
      if (this.timerCount <= 0 ) {
        clearInterval(this.timeInterval);
       this.finalInitiateTriggered();
      }
    }, 1000);
  }

  cancelInitiateSequence(){
    this.initiateModalRef.hide();
    clearInterval(this.timeInterval);
    this.isTimerStart = false;  
    this.timerCount = 15;  
    this.initiateBtnDisabled = false;
  }

  initiateSequence(){
    this.initiateBtnDisabled = true;
    this.startCountdown(16);    
  }

  finalInitiateTriggered(){
    let sequenceIntId = this.sequenceIntId;
    let contactids = [];
    this.finalInitiateContactsList.forEach(element => {
      contactids.push(element.contactid);
    });
   
    if(sequenceIntId != 0){
      let seqObj = {
        "sequenceintid":sequenceIntId,
        "contactids":contactids,
        "thread":this.IsChecked
      };
      this.initiateModalRef.hide();
        this.sequenceService.proceedSequence(seqObj).subscribe(data=>{
        if (data.Status === "Success") {
          this.router.navigate(['/sequenceview/'+sequenceIntId]);        
        }
      }
    );   
    }
  }

  initiateSequenceBtn(template: TemplateRef<any>){

    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map( node => node.data );
 
     // this.initiateCountInfo = "Today already initiated contacts = "+ initiatedContactsCount +
     // "<br> Remaining quota for the day is = "+ (maxInitiatedLimit - initiatedContactsCount);
 
     let initiatedCountSub = this.sequenceService.initiatedContactsCount().subscribe(data =>{
       if(data.Status.toLowerCase() == "success"){
        let initiatedContactsCount = parseInt(data.Data);
        let contactsList = this.rowData; 
        let maxInitiatedLimit = 150;
        if(initiatedContactsCount >= maxInitiatedLimit){
         this.isInitiatedBtnVisible = false;
         this.initiateCountInfo = "Today's quota was completed. please try for the next day..!";
        }else if(initiatedContactsCount < 1 && selectedData.length < 1){
         this.isInitiatedBtnVisible = false;
        // this.finalInitiateContactsList = contactsList.slice(0,maxInitiatedLimit - initiatedContactsCount);
         this.initiateCountInfo = "Today already initiated contacts = "+ initiatedContactsCount +
         "<br> Please select the contacts for initiating..!";
        }
        else if(initiatedContactsCount < maxInitiatedLimit && selectedData.length > (maxInitiatedLimit - initiatedContactsCount)){
       //  this.finalInitiateContactsList = contactsList.slice(0,maxInitiatedLimit - initiatedContactsCount);       
         this.isInitiatedBtnVisible = false; 
         this.initiateCountInfo = "Today already initiated contacts = "+ initiatedContactsCount +
           "<br> You can select contacts for initiating sequence = "+ (maxInitiatedLimit - initiatedContactsCount);
         
         
       }else{
         this.isInitiatedBtnVisible = true;
       //  this.finalInitiateContactsList = contactsList.slice(0,maxInitiatedLimit - initiatedContactsCount);
           this.finalInitiateContactsList = selectedData;
         this.initiateCountInfo = "Today already initiated contacts = "+ initiatedContactsCount +
         "<br> You are selected contacts for initiating sequence = "+ selectedData.length;
       }
        this.initiateModalRef = this.modalService.show(template, { class: "modal-m" });
       }
    });
   
   // let initiatedCountSub = this.sequenceService.initiatedContactsCount().subscribe(data =>{
   //     if(data.Status.toLowerCase() == "success"){
   //      let initiatedContactsCount = parseInt(data.Data);
   //      let contactsList = this.rowData; 
   //      let maxInitiatedLimit = 25;
   //      if(initiatedContactsCount >= maxInitiatedLimit){
   //       this.isInitiatedBtnVisible = false;
   //       this.initiateCountInfo = "Today's quota was completed. please try for the next day..!";
   //      }else {
   //        this.initiateCountInfo = "Today already initiated contacts = "+ initiatedContactsCount +
   //        "<br> Remaining quota for the day is = "+ (maxInitiatedLimit - initiatedContactsCount);
   //      }
   //     }
   //     this.initiateModalRef = this.modalService.show(template, { class: "modal-m" });
   //  });
    this.subscriptions.push(initiatedCountSub);
   }

  //#endregion

//#region Sequence Initiating
fetchTypeChangeHandler(clickedText) {
  clickedText = clickedText.toLowerCase();  
  this.IsContactRelatedItems= false; 
  if(clickedText == "contact"){
   this.IsContactRelatedItems= true;
   this.getCampaignTypeList();  
  }
  this.selectedFetchType = clickedText;
  this.contactFetchValue = "";  
}

getCampaignTypeList(){
  this.commonService.getCommonFields(3,"").subscribe(data=>{
    if (data.Status === "Success") {
      this.campaigntypeList = JSON.parse(data.Data);
    }
  });
}

compTypeChangeHandler(selectedValue){
  selectedValue = selectedValue.toLowerCase();  
  this.contactFetchValue = selectedValue;  
}

onRowSelected(event) { 
  let selectionStatus = event.node.selected;
  if(selectionStatus == true){ this.checkboxSelectionCount += 1; }
  else{ this.checkboxSelectionCount -= 1; }
   
  if(this.checkboxSelectionCount > 0){ this.IsProceedBtnEnabled = false; }
  else{this.IsProceedBtnEnabled = true;}
}
//#endregion
  
 
  getAccountsMasterTypes(){

  let industriesSub = this.commonService.getCommonFields(1,"").subscribe(data=>{
    if (data.Status === "Success") {
      this.IndustriesList = JSON.parse(data.Data);
    }
    this.subscriptions.push(industriesSub);
   });

    // this.accountsService.getAccountsMasterTypes().subscribe(data => {
    //   if (data.Status === "Success") {
    //     this.MasterTypes = JSON.parse(data.Data);     
    //     this.items = [];
    //    let industryInfo  = this.MasterTypes.filter(a=>a.type === "industry"); 
    //    if(industryInfo && industryInfo.length > 0){
    //    this.IndustriesList = industryInfo[0].subtypes;
    //    }
    //    let designationInfo  = this.MasterTypes.filter(a=>a.type === "designation"); 
    //    if(designationInfo && designationInfo.length > 0){
    //     designationInfo.forEach(element => {
            
    //       });
    //     }
    //   }
    // });
  }



  searchSequence(){    
    let configCheck = this.auth.getEmailConfig();
    if(configCheck == "true"){
    this.IsChecked = false;   
    let industryId= this.SelectedIndusrtyName;
    let designationArray = this.selected;
    let designationIds="";
    if(designationArray != undefined){    
    designationArray.forEach(element => {
      designationIds += element.id+",";
    });
    designationIds = designationIds.replace(/,\s*$/, "");
  }
    let filterObj = {
      "industryid": industryId,
      "designationids":designationIds
    };
    this.sequenceService.getSearchSequence(filterObj).subscribe(data=>{
      if (data.Status.toLowerCase() === "success") {
        this.IsSequenceContainerHidden = false;
       this.SequencesList = JSON.parse(data.Data);
       this.disabled = true;
       
      }
    });
  }else{
    alert("Mail configuration required");
    this.router.navigate(['/user/profile']);
  }
  // }else{
  //   alert("Please select designation");
  // }
  //   if(this.SelectedIndusrtyName != ""){
  //     this.IsSequenceContainerHidden = false;
  //     let filterObj = {
  //       "type":"",
  //       "subtype":this.SelectedIndusrtyName
  //     };
  //   this.sequenceService.getInitiateSequence(filterObj).subscribe(data=>{
  //     if (data.Status.toLowerCase() === "success") {
  //      this.SequencesList = JSON.parse(data.Data);
  //      this.disabled = true;
  //     }
  //   });
  // }
  }

  fetchContacts(){
    this.spinner.show(); 
    this.IsSequenceContainerHidden = true;
    let industryId= this.SelectedIndusrtyName;
    let designationArray = this.selected;
    let designationIds="";
    if(designationArray != undefined && designationArray != ""){    
    designationArray.forEach(element => {
      designationIds += element.id+",";
    });
    designationIds = designationIds.replace(/,\s*$/, "");
  }
    let filterObj = {
      "industryid": industryId,
      "designationids":designationIds,
      "fetchtype": this.selectedFetchType,
      "contactfetchvalue": this.contactFetchValue
    };
    this.sequenceService.previewContacts(filterObj).subscribe(data=>{
      if (data.Status.toLowerCase() === "success") {
        let contactsList = JSON.parse(data.Data);
        this.rowData = contactsList;
       
        this.contactsCount = contactsList.length;
      } else {
        this.rowData = [];
      }
      this.IsContactsListHidden = false;
      this.disabled = true;
      this.spinner.hide(); 
    });
  //   if(this.SelectedIndusrtyName != ""){
  //     this.IsSequenceContainerHidden = true;
  //     let filterObj1 = {
  //       "type":"",
  //       "subtype":this.SelectedIndusrtyName
  //     };
  //   this.sequenceService.fetchContacts(filterObj1).subscribe(data=>{
  //     if (data.Status.toLowerCase() === "success") {
  //       let contactsList = JSON.parse(data.Data);  
  //       this.rowData = contactsList;     
  //      this.IsContactsListHidden = false;
  //      this.disabled = true;
  //     }
  //   });
  // }
  }
  
  
}
