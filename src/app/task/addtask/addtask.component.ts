import { Component, OnInit, ViewChild, ElementRef, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TaskApiService } from 'src/services/taskservice.services';
import { teammanagementservice } from 'src/services/teamsmanagement.services';
import { TaskService } from '../task.service';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { ContactApiService } from 'src/services/contacts.services';
import { ViewcontactComponent } from 'src/app/contacts/viewcontact/viewcontact.component';
import { ViewaccountsComponent } from 'src/app/accounts/viewaccounts/viewaccounts.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { AccountApiService } from 'src/services/accounts.services';
import { idLocale } from 'ngx-bootstrap/chronos/i18n/id';

interface TaskInfo {
  TaskId: string;
  TaskTitle: string;
  TaskDescription: string;
  DueDate: number;
  CompletedDate: number;
  TaskType: string;
  AssignedTo: string;
  AssigneeName: string;
  ShortAssigneeName: string;
  Priority: string;
  FirstName: string;
  Designation: string;
  TaskStatus: string;
  SequenceDetailId: number;
  TaskIntId: number;
  ElapsedDuration: string;
  AccountName: string;
  ObjectId: string;
  ObjectIntId: number;
  ContactEmail: string;
}
interface priority {
  priority: string;
  prioritycount: number;
}
@Component({
  selector: 'app-addtask',
  templateUrl: './addtask.component.html',
  styleUrls: ['./addtask.component.css'],
  providers: [ViewcontactComponent, ViewaccountsComponent]
})
export class AddtaskComponent implements OnInit,OnDestroy {
  priorityCollection$: Observable<priority[]>;
  tasklist$: Observable<TaskInfo[]> = new BehaviorSubject<TaskInfo[]>([]);
  total$: Observable<number>;
  TaskForm: FormGroup;
  usersData: any;
  Contacts: any;
  submitted = false;
  bsValue = new Date();
  mytime: Date = new Date();
  loggedInUserRole: string;
  contactInfo:any;
  tasks: any;
  activeTab = 'sequences';
  //#region declaration
  contactsList:any;
  selContactInfo:any;
  contactObj:any;
  contactnamerequired:boolean = false;
  contactTextFieldRequired:boolean = true;
  accountId:number;
  accountContactsList:any;
  //#endregion
  IsMember: boolean;
  subscriptions:Subscription [] = [];
  activityresponse: any;
  getContactsData: any;
  get f() { return this.TaskForm.controls; }
  @ViewChild('closeAddTaskModal', { static: false }) closeAddTaskModal: ElementRef;

  @Input() contactClickedFrom;
  @Input() accountClickedFrom;
  constructor(private formBuilder: FormBuilder, private accountService: AccountApiService,private contactview:ViewcontactComponent,   
    private taskService: TaskApiService,private teamService: teammanagementservice,public service: TaskService,
    private contactService: ContactApiService, private accountview:ViewaccountsComponent, private spinner: NgxSpinnerService) { }

  ngOnInit() {
   // this.getContacts();   
    this.TaskForm = this.formBuilder.group({
      ObjectId: [''],
      Priority: ['',Validators.required],
      Title:  [ '',Validators.required],
      TaskType:  [ '',Validators.required],
      AssignedTo: [''],
      DueDate: [],
     // DueTime: [],
      Description: [],
      ObjectIntId: [],
      TeamId: [],
      AssignedTeamIntId: [],
      AssignedToIntId: ['',Validators.required],
      AccountId:[]   
    });

   let teamMembersSub = this.teamService.getTeamMembersWithoutTeamId()
    .subscribe(data => {
      this.usersData = data;
    });
   this.subscriptions.push(teamMembersSub);

   // getting single contact info while adding task from contacts view page
   if(this.contactClickedFrom != undefined){
      // alert(this.contactClickedFrom);
      this.contactTextFieldRequired = false;
      this.contactChangeHandler("",this.contactClickedFrom,'viewpage');
      // let contactActivitySub =  this.contactService.getContactActivityCount(this.contactClickedFrom).subscribe(data => {
      //     if (data.Status === 'Success') {
      //       let responseData = JSON.parse(data.Data);
      //       this.contactInfo = {name:responseData.ContactInfo.firstname,contactid:responseData.ContactInfo.contactid};
      //     }
      // });
      //   this.subscriptions.push(contactActivitySub);
    }else if(this.accountClickedFrom != undefined){
      this.accountId = parseInt(this.accountClickedFrom);
      let accountSubscription =  this.accountService.getAccountActivityCount(this.accountId).subscribe(data => {
        if (data.Status === 'Success') {
          this.activityresponse = JSON.parse(data.Data);
          this.getContactsData = this.activityresponse.ContactList;
          let contactsArray = [];          
          this.getContactsData.forEach(element => {
            let singleItem: any;
            singleItem = {
              id: element.id,
              name: element.contactname,
              accountid: this.accountId
            };
            contactsArray.push(singleItem);
          });
          this.accountContactsList = contactsArray;
          this.selContactInfo = contactsArray[0];
        }
      });
      
    }

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
   }

  // getContacts() {
  //   this.contactService.getContactsrawdata().subscribe(data => {
  //     if(data.Status == "Success"){
  //       let masterContactsData = JSON.parse(data.Data);
  //       this.Contacts = masterContactsData.ListOfContactedContacts;
  //       console.log(this.Contacts);
  //     }
      
  //   });
   
  // }

  //#region contacts dropdown
  selectContact(){
    // this.accountintiderror = false;  
    // this.accountnameerror = false; 
     console.log(this.selContactInfo);
   }

  contactChangeHandler(selectedObject: any,id:number,actionFrom) {
    let searchTerm = selectedObject.term?selectedObject.term:'';
    let contactId = id;
    let contactsArray = [];
    if ((searchTerm != undefined && searchTerm.length >= 3) || contactId > 0) {
      this.contactnamerequired = false;
      let accountsSub = this.contactService.getContactsbyname(searchTerm,contactId).subscribe(data => {
        if (data.Status == "Success") {
          let accounts = JSON.parse(data.Data);
          accounts.forEach(element => {
            let singleItem: any;
            singleItem = {
              id: element.id,
              name: element.contactname,
              accountid: element.accountintid
            };
            contactsArray.push(singleItem);
          });
          this.contactsList = contactsArray;
          if(actionFrom=='viewpage')   {
            this.selContactInfo = contactsArray[0];
          }  
        }
      });
      this.subscriptions.push(accountsSub);
    }

  }

    getContactInfoById(contactIntId:number):any{
     this.contactService.getContactView(contactIntId).subscribe(data => {
        if(data.Status == "Success"){
        this.contactObj = JSON.parse(data.Data);         
       // this.contactsList = contactsArray;         
        }
       });
    }
  //#endregion

  addTask() {
    this.submitted = true;
    let accountIntId = 0;
    let contactIntId = 0;
    if(this.selContactInfo != undefined && this.selContactInfo.id > 0){  
      accountIntId = this.selContactInfo.accountid;
      contactIntId = this.selContactInfo.id;
    }else{
      this.contactnamerequired = true;
      return;
    }
    if (this.TaskForm.invalid) {
      return;
    }
    this.spinner.show(); 

    console.log(this.contactClickedFrom);
    let date = new Date(this.bsValue);
    let timestamp = Math.floor(date.getTime() / 1000.0); 
    
    // let fullDate = date.setHours(this.mytime.getHours(), this.mytime.getMinutes());
    // let timestamp = Math.floor(new Date(fullDate).getTime() / 1000.0);
    if(contactIntId > 0 && accountIntId > 0){
   // let contactObj;
   // contactObj = this.Contacts.find(x => x.contactid == this.TaskForm.value.ObjectId);
   // let userObj = this.usersData.find(y => y.UserId == this.TaskForm.value.AssignedTo);
    this.TaskForm.patchValue({ DueDate: timestamp });
    this.TaskForm.patchValue({ ObjectIntId: contactIntId });
    this.TaskForm.patchValue({ AccountId: accountIntId });
   // this.TaskForm.patchValue({ AssignedToIntId: JSON.stringify(userObj.IntUserId) });
  // this.TaskForm.patchValue({ AssignedToIntId:  });
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.TaskForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.TaskForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    this.taskService.addTask(this.TaskForm.value).subscribe(data => {
      if(this.contactClickedFrom != undefined){
         this.activeTab = 'tasks';
         this.contactview.getSingleContactInfo(parseInt(this.contactClickedFrom));
         //this.contactview.gettasks(parseInt(this.contactClickedFrom));
         this.contactview.getActivities(parseInt(this.contactClickedFrom));
         
      }else if(this.accountClickedFrom != undefined){
        this.activeTab = 'tasks';
        this.contactview.gettasks(parseInt(this.accountClickedFrom));
        this.contactview.getActivities(parseInt(this.accountClickedFrom));
        this.contactview.getSingleContactInfo(parseInt(this.accountClickedFrom));
     }else{
      if(this.loggedInUserRole === "ta"){
        this.service.TeamAdminTaskReload();
      }else if(this.loggedInUserRole === "member"){
        this.service.MemberTaskReload();
      }else{
        this.service.adminTaskReload();
      }
      this.tasklist$ = this.service.countries$;
      this.total$ = this.service.total$;
      this.priorityCollection$ = this.service.priorityCollection$;     
      this.mytime = new Date();      
      this.gettasks();
    }
    this.closeAddTaskModal.nativeElement.click();
    this.onReset();
      this.spinner.hide();
    });
    }
  }

  onReset() {
    this.submitted = false;
    this.contactnamerequired = false;
    this.selContactInfo = null;
    this.bsValue = new Date();
    this.TaskForm.reset();
}

  gettasks() {
    this.taskService.getTaskByObjectId(this.contactClickedFrom)
      .subscribe(data => {
        if (data.Status === 'Success') {
          let tasks1 = JSON.parse(data.Data);
          this.tasks = tasks1.ListOfTasks;
        }
      });
  }

}
