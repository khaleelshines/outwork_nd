import { Component, OnInit, Pipe } from "@angular/core";
import { AuthService } from "src/app/guards/authguard/auth.service";
import { UtilitiesService } from "src/services/utilities.services";
import { UserService } from 'src/services/userservice.services';
declare var jQuery: any;

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
@Pipe({ name: "orderByAsc" })
export class HeaderComponent implements OnInit {
  MenuInfo = JSON.parse(localStorage.getItem("menuInfo"));

  selectedItem: any;
  userInfo: any;
  MainMenuInfo: any;
  logoClickedUrl: string = "#";

  constructor(public auth: AuthService, public utilities: UtilitiesService,
    public userService:UserService) {}

  ngOnInit() {
    let role = this.auth.getRole();
    let isLoggedIn = this.auth.isLoggedIn();
    let userDetails = localStorage.getItem("userInfo");
    if (userDetails != null && userDetails != "") {
      let userDetails1 = JSON.parse(userDetails);
      let loggedInUserRole = "";
      switch (userDetails1.ObjectType.toLowerCase()) {
        case "ta":
          loggedInUserRole = "Team Admin";
          break;
        case "admin":
          loggedInUserRole = "Admin";
          break;
        default:
          loggedInUserRole = "Member";
          break;
      }
      this.userInfo = { name: userDetails1.Name, role: loggedInUserRole };
    }
    if (isLoggedIn) {
      if (role === "admin") {
        this.logoClickedUrl = "/dashboard";
      } else {
        this.logoClickedUrl = "/dashboard/member";
      }
    }

    let mainMenuItems = this.MenuInfo.filter(item => item.IsAdmin !== 1);
    this.MainMenuInfo = this.utilities.sortBy(mainMenuItems, "MenuOrder");

    
    
    jQuery(document).ready(function($) {
      /*Sidebar Navigation*/
      $("#toggle_nav_btn").on("click", function() {
        $(".wrapper").toggleClass("slide-nav-toggle");
      });
      $("#sbmenu").click(function() {
        $(".fixed-sidebar-left").toggleClass("expand");
        $(".page-wrapper").toggleClass("expand");
      });
    });
  }

  selectedIndex: number;
  select(index: number) {
    this.selectedIndex = index;
  }
 
}
