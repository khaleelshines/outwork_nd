import { Component, OnInit } from '@angular/core';
import { templateService } from "src/services/templates.services";
import { Router } from "@angular/router";
import { GridOptions, CellClickedEvent } from 'ag-grid-community';
import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from 'rxjs';
import { AgGridAngular } from 'ag-grid-angular';
import { teamPerformanceService } from 'src/services/teamperformance.service';

@Component({
  selector: 'app-teamperformance',
  templateUrl: './teamperformance.component.html',
  styleUrls: ['./teamperformance.component.css']
})
export class TeamperformanceComponent implements OnInit {
  public columnDefs;
  private gridApi;
  private gridColumnApi;
  public defaultColDef;
  paginationPageSize: number;
  cacheBlockSize: number;
  //#region performance
  public performanceList:any;
  performancePeriodSelect:string = 'thismonth';
  //#endregion

  constructor(
    private templateApiService: templateService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private teamPerformance:teamPerformanceService,
    ) {

      this.buildColdefs();  

     }

  ngOnInit() {
    
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getTeamPerformanceData('thismonth');
  }

  PeriodChangedHandler(selectedValue:string){
    this.getTeamPerformanceData(selectedValue.toLowerCase());
  }

  getTeamPerformanceData(period:string){
    this.spinner.show();
    this.teamPerformance.getTeamPerformance(period).subscribe(data=>{
      if (data.Code === "SUC-200") {
        let teamPerformanceData = JSON.parse(data.Data);
         this.performanceList = teamPerformanceData;
         this.spinner.hide();
        
      }
    });
  }

  gridOptions: GridOptions = {
    pagination: true,
    rowHeight: 40,
    headerHeight: 47,   
    cacheBlockSize: 10,    
  };

  buildColdefs() {
   
    this.columnDefs = [
      {
        headerName: '',
        children: [
          {
            headerName: 'Owner',
            field: 'MemberName',
            sortable: true,
            filter: true,
            width: 200,
            pinned: 'left'
          }
        ]
      },
      {
        headerName: 'Market Research Related',
        children: [
          {
            headerName: '# Contacts Added',
            field: 'ContactsAdded',
            sortable: true,
            width: 250,
            suppressSizeToFit: true,
          },
          {
            headerName: '# Accounts Added',
            field: 'AccountsAdded',
            sortable: true,
            width: 250,
            suppressSizeToFit: true,
          }
        ]
      },
      {
        headerName: 'Email / Sequence Metrics',
        children: [
          {
            headerName: '# Contacts Engaged',
            field: 'ContactsEngaged',
            sortable: true,
            width: 200,
            suppressSizeToFit: true,

          },
          {
            headerName: '# Accounts Engaged',
           field: 'AccountsEngaged',
                sortable: true,            
                width: 200,
                suppressSizeToFit: true,
                enableRowGroup: true,
            
          },
          {
            headerName: '# Emails Sent',
            field: 'EmailsSent',
            sortable: true,
            width: 200,
            suppressSizeToFit: true,
          },
          {
            headerName: '# Emails Delivered',
            field: 'EmailsDelivered',
            sortable: true,
            width: 200,
            suppressSizeToFit: true,
          },
          {
            headerName: '# Emails Opened',
            field: 'EmailsOpened',
            sortable: true,
            width: 200,
            suppressSizeToFit: true,
          },
          {
            headerName: '# Clicks',
            field: 'EmailsClicked',
            sortable: true,
            width: 200,
            suppressSizeToFit: true,

          },
          {
            headerName: '# Replies',
            field: 'EmailsReplied',
            sortable: true,
            width: 200,
            suppressSizeToFit: true,
          }
        ]
      },
      {
        headerName: 'Analytics',
        children: [
          {
            headerName: 'Deliverability',
            field: 'Deliverability',
            sortable: true,
            width: 200,
            suppressSizeToFit: true,

          },
          {
            headerName: 'Open Rate',
            field: 'OpenRate',
            sortable: true,
            width: 200,
            suppressSizeToFit: true,

          },
          {
            headerName: 'Clickthrough Rate',
            field: 'ClickRate',
            sortable: true,
            width: 200,
            suppressSizeToFit: true,

          },
          {
            headerName: 'New Meetings Scheduled',
            field: 'ScheduledMeetings',
            sortable: true,
            width: 200,
            suppressSizeToFit: true,
          },
          {
            headerName: 'Meetings Conducted',
            field: 'ConductedMeetings',
            sortable: true,
            filter: 'agTextColumnFilter',
            width: 200,
            suppressSizeToFit: true,

          }
        ]
      }
    ];

    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,   
    };

    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;

  }


}
