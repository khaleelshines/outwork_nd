import { Component, OnInit, NgZone } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);
@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {
  private chart: am4charts.XYChart;
  constructor(private zone: NgZone) {}

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      var chart = am4core.create("monthlyVisits", am4charts.XYChart);

      chart.colors.step = 2;
      chart.maskBullets = false;
      // Add data
      chart.data = [{
          "date": "2012-01-01",
          "visitcount": 227,         
      }, {
          "date": "2012-01-02",
          "visitcount": 371,
          
      }, {
          "date": "2012-01-03",
          "visitcount": 433,
         
      }, {
          "date": "2012-01-04",
          "visitcount": 345,
        
      }, {
          "date": "2012-01-05",
          "visitcount": 480,
         
      }, {
          "date": "2012-01-06",
          "visitcount": 386,
         
      }, {
          "date": "2012-01-07",
          "visitcount": 348,
         
      }, {
          "date": "2012-01-08",
          "visitcount": 238,
         
      }, {
          "date": "2012-01-09",
          "visitcount": 218,
        
      }, {
          "date": "2012-01-10",
          "visitcount": 349,
          
      }, {
          "date": "2012-01-11",
          "visitcount": 603,
        
      }, {
          "date": "2012-01-12",
          "visitcount": 534         
      }, {
          "date": "2012-01-13",         
          "visitcount": 425        
      }, {
          "date": "2012-01-14",          
          "visitcount": 470         
      }, {
          "date": "2012-01-15"
      }, {
          "date": "2012-01-16"
      }, {
          "date": "2012-01-17"
      }];
      
      // Create axes
      var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.renderer.grid.template.location = 0;
      dateAxis.renderer.grid.template.disabled = true;
      dateAxis.renderer.fullWidthTooltip = true;
      
      var visitcountAxis = chart.yAxes.push(new am4charts.ValueAxis());
      visitcountAxis.title.text = "Customer Visits";
      visitcountAxis.renderer.grid.template.disabled = true;     
      
      
      var latitudeAxis = chart.yAxes.push(new am4charts.ValueAxis());
      latitudeAxis.renderer.grid.template.disabled = true;
      latitudeAxis.renderer.labels.template.disabled = true;
      
      // Create series
      var visitcountSeries = chart.series.push(new am4charts.ColumnSeries());
      visitcountSeries.dataFields.valueY = "visitcount";
      visitcountSeries.dataFields.dateX = "date";
      visitcountSeries.yAxis = visitcountAxis;
      visitcountSeries.tooltipText = "{valueY} visits";
      visitcountSeries.name = "Customer Visits";
      visitcountSeries.columns.template.fillOpacity = 0.7;
      visitcountSeries.columns.template.propertyFields.strokeDasharray = "dashLength";
      visitcountSeries.columns.template.propertyFields.fillOpacity = "alpha";
      
      var disatnceState = visitcountSeries.columns.template.states.create("hover");
      disatnceState.properties.fillOpacity = 0.9;     
      // Add legend
      chart.legend = new am4charts.Legend();
      chart.legend.position = "top";

      // Add cursor
      chart.cursor = new am4charts.XYCursor();
      chart.cursor.fullWidthLineX = true;
      chart.cursor.xAxis = dateAxis;
      chart.cursor.lineX.strokeOpacity = 0;
      chart.cursor.lineX.fill = am4core.color("#000");
      chart.cursor.lineX.fillOpacity = 0.1;
    });
  }

  
  
  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
