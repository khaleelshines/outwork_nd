import { Component, OnInit, NgZone } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  private chart: am4charts.XYChart;
  constructor(private zone: NgZone) {} 
  
  ngOnInit() {
   
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      let chart = am4core.create("chartdiv", am4charts.XYChart);
      let chartdata = [{
        "year": "Khaleel",       
        "count": 18
      },{
        "year": "Ketan",        
        "count": 22
      },{
        "year": "Sri Vastava",       
        "count": 23
      },{
        "year": "Mastan",        
        "count": 25
      },{
        "year": "Jony Dieol",        
        "count": 25
      }];
     // let finalData = this.getOrder(chartdata,"DESC");
      let finalData = chartdata.sort(function (a, b) 
    {
      return b.count - a.count;
    });

      chart.data = finalData;
      // Create axes
      var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "year";
     // categoryAxis.numberFormatter.numberFormat = "#";        
      categoryAxis.renderer.inversed = true;
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.cellStartLocation = 0.1;
      categoryAxis.renderer.cellEndLocation = 0.9;

      var  valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
      valueAxis.renderer.opposite = true;

      // Create series
      function createSeries(field, name) {
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueX = field;
        series.dataFields.categoryY = "year";
        series.name = name;       
        series.columns.template.tooltipText = "[bold]{year}[/] has added [bold]{valueX}[/] new customers today";
        series.columns.template.height = am4core.percent(100);
        series.sequencedInterpolation = true;
        series.columns.template.height = 50;

      
        var valueLabel = series.bullets.push(new am4charts.LabelBullet());
        valueLabel.label.text = "{valueX}";
        valueLabel.label.horizontalCenter = "left";
        valueLabel.label.dx = 10;
        valueLabel.label.hideOversized = false;
        valueLabel.label.truncate = false;
      
        var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
        categoryLabel.label.text = "";
        categoryLabel.label.horizontalCenter = "right";
        categoryLabel.label.dx = -10;
        categoryLabel.label.fill = am4core.color("#fff");
        categoryLabel.label.hideOversized = false;
        categoryLabel.label.truncate = false;
      }     
      
      createSeries("count", "New Customers Added");
      this.chart = chart;
    });
  }

  
  
  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

}
