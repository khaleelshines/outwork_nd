import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivitiesChartComponent } from './activitieschart.component';

describe('ActivitiesChartComponent', () => {
  let component: ActivitiesChartComponent;
  let fixture: ComponentFixture<ActivitiesChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivitiesChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitiesChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
