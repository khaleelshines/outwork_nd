import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, NavigationEnd } from "@angular/router";
import { UserService } from "src/services/userservice.services";
import { RoleApiService } from "src/services/roles.services";
import { CellCustomComponent } from "../custom_components/cell-custom/cell-custom.component";
import { GridOptions } from "ag-grid-community";
import { UsersActionsComponent } from "../custom_components/users-actions/users-actions.component";
import { UtilitiesService } from "src/services/utilities.services";
import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from 'rxjs';

@Component({
  selector: "app-usermanagement",
  templateUrl: "./usermanagement.component.html",
  styleUrls: ["./usermanagement.component.css"]
})
export class UsermanagementComponent implements OnInit,OnDestroy {
  userForm: FormGroup;
  submitted = false;
  selectedRoleIntId: Int32Array;
  Roles: any;
  UserData: any;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowGroupPanelShow;
  public pivotPanelShow;
  public sideBar;
  public rowData: any;
  public usersData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
  selectedRoleName: string;
  ObjectType: string;
  event: object;
  interval: any;
  subscriptions:Subscription [] = [];
  @ViewChild("closeAddUserModal", { static: false })
  closeAddUserModal: ElementRef;

  public getContextMenuItems(params) {
    let columnName = params.column.colDef.headerName;
    if (columnName === "Actions") {
      return [];
    } else {
      var result = ["copy", "paste", "export", "autoSizeAll"];
      return result;
    }
  }
  get f() {
    return this.userForm.controls;
  }
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private roleApiService: RoleApiService,
    public utilities: UtilitiesService,
    private spinner: NgxSpinnerService
  ) {
    this.sideBar = {
      toolPanels: [
        {
          id: "columns",
          labelDefault: "Columns",
          labelKey: "columns",
          iconKey: "columns",
          toolPanel: "agColumnsToolPanel"
        },
        {
          id: "filters",
          labelDefault: "Filters",
          labelKey: "filters",
          iconKey: "filter",
          toolPanel: "agFiltersToolPanel"
        }
      ],
      defaultToolPanel: ""
    };
    this.columnDefs = [
      {
        headerName: "Name",
        field: "Name"
      },
      {
        headerName: "Login Id",
        field: "LoginId"
      },
      {
        headerName: "Role",
        field: "ObjectType"
      },
      {
        headerName: "Status",
        field: "Status"
      },
      {
        headerName: "Email",
        field: "Email"
      },
      {
        headerName: "Phone No",
        field: "MobileNumber"
      },
      {
        headerName: "Actions",
        pinned: "right",
        width: 130,
        editable: false,
        cellStyle: { "padding-top": "2px" },
        cellRendererFramework: UsersActionsComponent
      }
    ];

    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true
      //   width: 200,
    };
    this.rowSelection = "multiple";
    this.rowGroupPanelShow = "always";
    this.pivotPanelShow = "always";
    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;
  }
 
  gridOptions: GridOptions = {
    rowHeight: 52,
    headerHeight: 44,

    getContextMenuItems: this.getContextMenuItems
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.userService.getUsers().subscribe(data => {
      this.usersData = JSON.parse(data.Data);
      this.rowData = this.usersData;
    });
  }

  ngOnInit() {
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 2000);

    if (!window.localStorage.getItem("token")) {
      this.router.navigate(["login"]);
    }

    this.refreshData();
    this.interval = setInterval(() => {
      this.refreshData();
    }, 5000);

    this.userForm = this.formBuilder.group({
      FirstName: ["", Validators.compose([Validators.required])],
      LastName: ["", Validators.compose([Validators.required])],
      Email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]
      ],
      MobileNumber: [""],
      RoleId: [
        this.selectedRoleIntId,
        Validators.compose([Validators.required])
      ],
      LoginId: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]
      ],
      ObjectType: []
    });

    this.getCompanyRoles();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }
  refreshData() {
   let usersSub = this.userService.getUsers().subscribe(data => {
      this.usersData = JSON.parse(data.Data);
      this.rowData = this.usersData;
    });
    this.subscriptions.push(usersSub);
  }

  // selectRoleChangeHandler(selectedvalue:Int32Array) {
  //   this.selectedRoleIntId = selectedvalue;
  // }

  selectRoleChangeHandler(event) {
    let selectElementText =
      event.target["options"][event.target["options"].selectedIndex].text;
    this.selectedRoleIntId = event.target.RoleIntId;
    this.selectedRoleName = selectElementText;
  }

  addUser() {
    this.submitted = true;
    if (this.userForm.invalid) {
      return;
    }
    this.userForm.value.ObjectType =
      this.selectedRoleName == "employee" ? "member" : this.selectedRoleName;
    this.userService.addUser(this.userForm.value).subscribe(data => {
      if (data.Status === "Success") {
        this.closeAddUserModal.nativeElement.click();
        this.userService.getUsers().subscribe(data => {
          this.usersData = JSON.parse(data.Data);
          this.rowData = this.usersData;
          this.userForm.reset();
        });
      } else {
      }
    });
  }

  getCompanyRoles() {
   let companyRolesSub = this.roleApiService.getcompanyroles().subscribe(data => {
      if (data.Status === "Success") {
        this.Roles = JSON.parse(data.Data);
      } else {
        alert(data.Message);
      }
    });
    this.subscriptions.push(companyRolesSub);
  }
  getUsers() {
    this.userService.getUsers().subscribe(data => {
      this.usersData = JSON.parse(data.Data);
    });
  }
}
