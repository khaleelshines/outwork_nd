import { Component, OnInit, ViewChild, ElementRef, Renderer, OnDestroy, TemplateRef } from "@angular/core";
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountApiService } from 'src/services/accounts.services';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { Subscription } from 'rxjs';
import { CommonFieldsService } from 'src/services/commonfields.services';
import { teammanagementservice } from 'src/services/teamsmanagement.services';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UtilitiesService } from 'src/services/utilities.services';

@Component({
  selector: 'app-addnewaccount',
  templateUrl: './addnewaccount.component.html',
  styleUrls: ['./addnewaccount.component.css']
})
export class AddnewaccountComponent  implements OnInit {

  accountForm: FormGroup;  
  newaccountIntId:number = 0;
  IsMember: boolean;
  userInfo: any;
  designationCategoryList:any;
  loggedInUserRole: string;
  contactForm: FormGroup;
  submitted = false;
  selectedStage: string;
  selectedSource:string;
  selectedType:string;
  accountResponseInfo:string;
  usersData:any;
  subscriptions:Subscription[] = [];
  industriesList:any = [];
  selIndustryInfo:any;
  industryintiderror:boolean = false;
  loading = false;

  get f() {
    return this.accountForm.controls;
  }
  infoModalRef: BsModalRef;

  constructor(
    private router:Router,
    private accountsService: AccountApiService,
    private teamService:teammanagementservice,
    private auth:AuthService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private commonService:CommonFieldsService,
    public utilities:UtilitiesService,
  ) {
    this.loggedInUserRole = this.auth.getRole();
    if (this.loggedInUserRole === "member") {
      this.IsMember = true;
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        IntUserId: userDetails1.IntUserId
      };
    } }

  
  ngOnInit() {
    this.accountForm = this.formBuilder.group({
      accountname: ["", Validators.compose([Validators.required])],
      industryid:[""],
      subindustry: [""],
      accountsource:[this.selectedSource],
      accounttype:[this.selectedSource],
      website: ["",
      [
         Validators.required, 
       // Validators.pattern("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}$")
         Validators.pattern("[www]+\.([a-z.]{2,6})[/\\w .-]*/?")
        // Validators.pattern("([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?")
       ]],
      companylinkedinurl: [""],
      email: [
        "",
        [
         // Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]
      ],
      mobilenumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      stage: [this.selectedStage,Validators.compose([Validators.required])],
      AssignedToIntId: ["", Validators.required],
      AssignedTeamIntId: [],
      TeamId: [],
      noofemployees:[],
      revenue:[],
      companyaddress:[]
    });
    let teamsSubscription = this.teamService.getTeamMembersWithoutTeamId().subscribe(data => {
      this.usersData = data;
    });
  }

  selectSourceChangeHandler(selectedValue: string) {  
    this.selectedSource = selectedValue;    
  }
  selectTypeChangeHandler(selectedValue: string) {  
    this.selectedType = selectedValue;    
  }

  selectStageChangeHandler(selectedValue: string) {  
    this.selectedStage = selectedValue;
  }
  
  addAccount(template: TemplateRef<any>) {
    this.submitted = true;  
   
    let indusryId = 0;
    if (this.selIndustryInfo == undefined) {      
      this.industryintiderror = true;
      return;
    }else{
      indusryId = this.selIndustryInfo.id;
      this.accountForm.patchValue({ industryid: indusryId});
    }
    if (this.accountForm.invalid) {      
      return;
    } 
    this.loading = true;
    let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
    this.accountForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.accountForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });   
    
    this.accountsService.AddAccount(this.accountForm.value).subscribe(data => {
      if (data.Status === "Success") {
        this.newaccountIntId = parseInt(data.Data);   
        this.accountResponseInfo = "<h6>Do you want to add contacts for this account..!</h6>";       
      } else if(data.Code == "FAL-409"){
       this.accountResponseInfo = "<h6>Account already exists..!</h6>";        
      }
      this.loading = false;
      this.infoModalRef = this.modalService.show(template, { class: "modal-m",backdrop  : 'static' });
      this.onReset();
   });
  }

  onReset() {
    this.submitted = false;
    this.loading = false;
    this.accountForm.reset();
    this.router.navigate(['/accounts']);
  } 

  selectIndustry(){  
    console.log(this.selIndustryInfo);
    this.industryintiderror = false;
    console.log(new Date);
  }

  industryChangeHandler(selectedObject:any){
   let industryArray =[];
      if(selectedObject.term.length >= 3){
      let industrysSub = this.commonService.getCommonFields(1,selectedObject.term).subscribe(data => {
       if(data.Status == "Success"){
        let industries = JSON.parse(data.Data);
        industries.forEach(element => {
         let singleItem:any;         
           singleItem ={
             id :element.id,
             name:element.labeltitle
           };       
        
           industryArray.push(singleItem);
       });
       this.industriesList = industryArray;         
       }
      });
      this.subscriptions.push(industrysSub);
       }   
   }

   getDesignationCategoryList(){
     this.commonService.getCommonFields(4,"").subscribe(data=>{
       if (data.Status === "Success") {
         this.designationCategoryList = JSON.parse(data.Data);
       }
      });
   }

   addContactsProceed(){
     this.infoModalRef.hide();
     this.router.navigate(['/accounts/viewaccount/'+this.newaccountIntId]);
   }


  loadAccounts(){
    this.infoModalRef.hide();
    this.router.navigate(['/accounts/cr-today']);
  }
}
