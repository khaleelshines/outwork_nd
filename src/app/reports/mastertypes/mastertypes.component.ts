import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { GridOptions, CellClickedEvent } from 'ag-grid-community';
import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from 'rxjs';
import { AgGridAngular } from 'ag-grid-angular';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { UserService } from 'src/services/userservice.services';
import { reportsService } from 'src/services/reports.services';

@Component({
  selector: 'app-mastertypes',
  templateUrl: './mastertypes.component.html',
  styleUrls: ['./mastertypes.component.css']
})
export class MastertypesComponent implements OnInit {
  
  private gridApi;
  private gridColumnApi;
  public columnDefs;
  public defaultColDef;
  paginationPageSize: number;
  cacheBlockSize: number;
  public excelStyles;

  public columnDefs1;
  public defaultColDef1;
  paginationPageSize1: number;
  cacheBlockSize1: number;
  //#region Industries
  public industriesReportList:any;
  performancePeriodSelect:string = 'thismonth';
  usersList: any;
  selUserInfo:any;
  mainTab:string = 'industry';
  //#endregion
  //#region Designation
    public designationsReportList:any = [];
  //#endregion


  constructor(private router: Router, private spinner: NgxSpinnerService, public auth: AuthService,
    private userService: UserService,
    private reportService:reportsService,
    ) {
    this.buildColdefs();  
    this.buildColdefs1();
  }

  ngOnInit() {
    this.getUsers();
  }

  mainFilterTabsChanged(selectedTabValue:string){
    if(selectedTabValue == "designation"){
      this.getDesignationReport();
    }
      this.mainTab = selectedTabValue;
    
  }

  gridOptions: GridOptions = {
    pagination: true,
    rowHeight: 40,
    headerHeight: 47,   
    cacheBlockSize: 10
  };

  buildColdefs() {
    this.columnDefs = [
      {
        headerName: 'Industry', 
        field: 'IndustryName',
        sortable: true,
        filter: true,
        width: 300,
        pinned: 'left'
      }, 
      {
        headerName: 'Sub Industry', 
        field: 'SubIndustryName',
        sortable: true,
        filter: true
      }, 
      {
        headerName: '# Accounts', 
        field: 'NoOfAccounts',
        sortable: true,
        filter: true
      }, 
      {
        headerName: '# Contacts', 
        field: 'NoOfContacts',
        sortable: true,
        filter: true
      }, 
      {
        headerName: '# Account Engaged', 
        field: 'AccountsEngaged',
        sortable: true,
        filter: true
      }, 
      {
        headerName: '# Account Not Engaged', 
        field: 'AccountsNotEngaged',    
        sortable: true,
        filter: true
      }, 
      {
        headerName: '# Contacts Engaged', 
        field: 'ContactsEngaged',
        sortable: true,
        filter: true
      }, 
      {
        headerName: '# Contacts Not Engaged', 
        field: 'ContactsNotEngaged',
        sortable: true,
        filter: true
      }, 
      {
        headerName: 'Email Sent', 
        field: 'EmailsSent',
        sortable: true,
        filter: true
      }, 
      {
        headerName: 'Unique Emails', 
        field: 'UniqueEmails',
        sortable: true,
        filter: true
      }, 
      {
        headerName: 'Emails Delivered', 
        field: 'EmailsDelivered',
        sortable: true,
        filter: true
      }, 
      {
        headerName: 'Emails Opened', 
        field: 'EmailsOpened',
        sortable: true,
        filter: true
      }, 
      {
        headerName: 'Open rate', 
        field: 'OpenRate',
        sortable: true,
        filter: true
      }, 
      {
        headerName: 'Emails Bounced', 
        field: 'EmailsBounced',
        sortable: true,
        filter: true
      }, 
      {
        headerName: 'Emails Replied', 
        field: 'EmailsReplied',
        sortable: true,
        filter: true
      }, 
      {
        headerName: 'Auto Email Replies', 
        field: 'AutoEmailsReplied',
        sortable: true,
        filter: true
      }
   ];

   this.excelStyles = [
    {
      id: 'header',
      interior: {
        color: '#f5f6fa',
        pattern: 'Solid',
      },
      // font: {
      //   fontName: 'Calibri Light',
      //   color: '#ffffff',
      //   weight: 600,
      // },
     
    }
  ];

    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,   
    };

    this.cacheBlockSize = 10;
    this.paginationPageSize = 10;
  }

  buildColdefs1() {
    this.columnDefs1 = [
      {
        headerName: 'Designation', 
        field: 'Designation',
        sortable: true,
        filter: true,
        width: 200,
        pinned: 'left'
      }, 
      {
        headerName: 'No of Accounts', 
        field: 'Count',
        sortable: true,
        filter: true
      },  
      {
        headerName: 'Industry', 
        field: 'Industry',
        sortable: true,
        filter: true
      }
   ];

    this.defaultColDef1 = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,   
    };

    this.cacheBlockSize1 = 10;
    this.paginationPageSize1 = 10;
  }




  onGridReady(params) {    
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;    
    this.getIndustiesReport();
  }

  onBtExport() {  
    this.gridApi.exportDataAsExcel({    
      fileName: 'reports.xlsx',
      sheetName:'Industry_wise_report'
    });
  }

  getUsers() {
    let usersArray =[];
    this.userService.getUsers().subscribe(data => {
      if (data.Status === "Success") {
        let userData = JSON.parse(data.Data);
 
        userData.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.IntUserId,
              name:element.Name,
            };       
         
          usersArray.push(singleItem);
        });
        this.usersList = usersArray;
       }
    });
  }

  PeriodChangedHandler(selectedValue:string){
    this.getTeamPerformanceData(selectedValue.toLowerCase());
  }

  getTeamPerformanceData(period:string){
    this.spinner.show();
  }

  resetReports(){   
  }

  //#region Get Industries reports
   getIndustiesReport(){
    this.spinner.show();

    this.reportService.getIndustriesReports().subscribe(data=>{
      if (data.Code === "SUC-200") {
        let teamPerformanceData = JSON.parse(data.Data);
         this.industriesReportList = teamPerformanceData;
               
      }else{
        this.industriesReportList = [];
      }
      this.spinner.hide();  
    });
   
   }
  //#endregion

  //#region Get Industries reports
  getDesignationReport(){
    this.spinner.show();
    if(this.designationsReportList.length <= 0){
    this.reportService.getDesignationReports().subscribe(data=>{
      if (data.Code === "SUC-200") {
        let designationData = JSON.parse(data.Data);
         this.designationsReportList = designationData;
               
      }else{
        this.designationsReportList = [];
      }
      
    });
  }
  this.spinner.hide();  
   }
  //#endregion

  selectMember() {
    console.log(this.selUserInfo);  
  }


}