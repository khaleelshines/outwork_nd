import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { Observable } from 'rxjs';
import {NgbdSortableHeader, SortEvent} from './sortable.directive';
import { CountryService } from './country.service';
// import { Country } from './country';

interface Country {
  id: number;
  name: string;
  flag: string;
  area: number;
  population: number;
}

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})


export class TeamComponent implements OnInit {
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(public service: CountryService) {
    this.countries$ = service.countries$;
    this.total$ = service.total$;
  }

  ngOnInit() {
  }

  countries$: Observable<Country[]>;
  total$: Observable<number>;

  // onSort({column, direction}: SortEvent) {  
  //   this.headers.forEach(header => {
  //     if (header.sortable !== column) {
  //       header.direction = '';
  //     }
  //   });

  //  this.service.sortColumn = column;
  //  this.service.sortDirection = direction;
  // }

}
