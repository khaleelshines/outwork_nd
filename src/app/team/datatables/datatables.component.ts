import { Component, OnInit, PipeTransform, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { Observable, BehaviorSubject, Subject, of } from 'rxjs';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { startWith, map, debounceTime, delay, switchMap, tap } from 'rxjs/operators';
// import { SortDirection } from '../sortable.directive';
import { CountryService1 } from './country1.service';
import { TaskApiService } from 'src/services/taskservice.services';
import { ContactsComponent } from 'src/app/contacts/contacts.component';
import { UserService } from 'src/services/userservice.services';
import { SequenceComponent } from 'src/app/sequence/sequence.component';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { string } from '@amcharts/amcharts4/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CallsApiService } from 'src/services/calls.services';
import Froalaeditor from 'froala-editor';
import { SequenceApiService } from 'src/services/sequenceservice.services';
import { teammanagementservice } from 'src/services/teamsmanagement.services';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';

interface Country {
  TaskId: string;
  TaskTitle: string;
  TaskDescription: string;
  DueDate: number;
  CompletedDate: number;
  TaskType: string;
  AssignedTo: string;
  AssigneeName: string;
  ShortAssigneeName: string;
  Priority: string;
  FirstName: string;
  Designation: string;
  TaskStatus: string;
  SequenceDetailId: number;
  TaskIntId: number;
  ElapsedDuration: string;
  AccountName: string;
  ObjectId: string;
  ObjectIntId: number;
  ContactEmail: string;
}
interface priority {
  priority: string;
  prioritycount: number;
}


@Component({
  selector: 'app-datatables',
  templateUrl: './datatables.component.html',
  styleUrls: ['./datatables.component.css'],
  providers: [DecimalPipe, ContactsComponent, SequenceComponent]
})
export class DatatablesComponent implements OnInit {
  priorityCollection$: Observable<priority[]>;
  countries$: Observable<Country[]> = new BehaviorSubject<Country[]>([]);
  total$: Observable<number>;
  timepickerVisible = false;
  mytime: Date = new Date();
  mytime1: Date = new Date();
  TaskForm: FormGroup;
  callLogForm: FormGroup;
  ManualEmailForm: FormGroup;
  bsValue = new Date();
  usersData: any;
  Contacts: any;
  IsMember: boolean;
  loggedInUserRole: string;
  rescheduleTaskmodalRef: BsModalRef;
  callLogModalRef: BsModalRef;
  confirmationCallLogModalRef: BsModalRef;
  emptyTemplateModal: BsModalRef;
  rescheduleTaskId: string;
  submitted = false;
  options;
  objectData: any;
  contactEmail: string;
  confirmationMessage: string;
  IsDataAvailable: boolean = true;
  userInfo:any;
  @ViewChild('closeAddTaskModal', { static: false }) closeAddTaskModal: ElementRef;
  get f() { return this.TaskForm.controls; }
  constructor(public service: CountryService1, private taskService: TaskApiService, private formBuilder: FormBuilder,
    private contacts: ContactsComponent, private userService: UserService, public auth: AuthService,
    private modalService: BsModalService, private callsService: CallsApiService, private apiService: SequenceApiService,
    private teamService: teammanagementservice) {
    //  this.countries$ = new Observable<[]>();
    //  this.total$ = new Observable<0>();
    //  this.priorityCollection$ = new Observable<[]>();


    // this.countries$ = service.countries$;   
    // this.total$ = service.total$;
    // this.priorityCollection$ = service.priorityCollection$;
    this.loggedInUserRole = this.auth.getRole();
    if (this.loggedInUserRole === 'member' || this.loggedInUserRole === 'employee') {
      this.loggedInUserRole = 'member';
      this.IsMember = true;
      let userDetails = localStorage.getItem('userInfo');
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {name:userDetails1.Name, UserId:userDetails1.UserId}
    }

    if(this.loggedInUserRole === "ta"){
      this.service.TeamAdminTaskReload();
    }else if(this.loggedInUserRole === "member"){
      this.service.MemberTaskReload();
    }else{
      this.service.adminTaskReload();
    }
    
    // this.loggedInUserRole === 'member'?this.service.MemberTaskReload():this.service.addTaskReload();     
    this.countries$ = this.service.countries$;
    this.total$ = this.service.total$;
    this.priorityCollection$ = this.service.priorityCollection$;

    // this.loggedInUserRole === 'member'?this.service.MemberTaskReload():this.service.addTaskReload();   

  }

  ngOnInit() {


    this.getContacts();
    // this.userService.getUsers()
    //   .subscribe(data => {
    //     this.usersData = JSON.parse(data.Data);
    //   });

    this.TaskForm = this.formBuilder.group({
      ObjectId: ['',Validators.required],
      Priority: ['',Validators.required],
      Title:  [ '',Validators.required],
      AssignedTo: ['',Validators.required],
      DueDate: [],
      DueTime: [],
      Description: [],
      ObjectIntId: [],
      TeamId: [],
      AssignedTeamIntId: [],
      AssignedToIntId: [],
      AccountId:[]   
    });

    this.callLogForm = this.formBuilder.group({
      Description: [],
      Title: [],
      ObjectType: [],
      ObjectIntId: [],
      ObjectId: []
    });

    this.ManualEmailForm = this.formBuilder.group({
      to: ['', Validators.required],
      CC: ['', Validators.required],
      Subject: ['', Validators.required],
      HtmlBody: [],
      AssignedTeam: [],
      AssignedTeamIntId: []
    });

    Froalaeditor.DefineIcon('custom-field', { NAME: 'cog', SVG_KEY: 'cogs' });
    Froalaeditor.RegisterCommand('custom-field', {
      title: 'Personalize',
      type: 'dropdown',
      focus: false,
      undo: false,
      refreshAfterCallback: true,
      options: {
        '{{Contact: First Name}}': 'First Name',
        '{{Contact: Last Name}}': 'Last Name',
        '{{Contact: Designation}}': 'Designation',
        '{{Contact: PhoneNumber}}': 'PhoneNumber'
      },
      callback: function (cmd, val) {
        this.html.insert(val);
      }
    });

    this.options = {
      placeholderText: 'Edit Your Content Here!',
      height: 200,
      toolbarBottom: true,
      inlineMode: false,
      toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'custom-field', '|', 'insertLink', 'insertImage', 'specialCharacters', 'color', '|', 'align', 'formatOL', 'formatUL', '|', 'undo', 'redo', 'clearFormatting', 'print'],
    }

    this.teamService.getTeamMembersWithoutTeamId()
      .subscribe(data => {
        this.usersData = data;
      });
  }
  getContacts() {
    this.contacts.getContacts().then(result => {
      this.Contacts = result;
    });
  }

  addTask() {
    this.submitted = true;
   
    if (this.TaskForm.invalid) {
      return;
    }
    let date = new Date(this.bsValue);
    let fullDate = date.setHours(this.mytime.getHours(), this.mytime.getMinutes());
    let timestamp = Math.floor(new Date(fullDate).getTime() / 1000.0);
    let contactObj = this.Contacts.find(x => x.contactid == this.TaskForm.value.ObjectId);
    let userObj = this.usersData.find(y => y.UserId == this.TaskForm.value.AssignedTo);
    this.TaskForm.patchValue({ DueDate: timestamp });
    this.TaskForm.patchValue({ ObjectIntId: JSON.stringify(contactObj.id) });
    this.TaskForm.patchValue({ AccountId: JSON.stringify(contactObj.accountintid) });
    this.TaskForm.patchValue({ AssignedToIntId: JSON.stringify(userObj.IntUserId) });
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.TaskForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.TaskForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    this.taskService.addTask(this.TaskForm.value).subscribe(data => {
      if(this.loggedInUserRole === "ta"){
        this.service.TeamAdminTaskReload();
      }else if(this.loggedInUserRole === "member"){
        this.service.MemberTaskReload();
      }else{
        this.service.adminTaskReload();
      }
      this.countries$ = this.service.countries$;
      this.total$ = this.service.total$;
      this.priorityCollection$ = this.service.priorityCollection$;
      this.closeAddTaskModal.nativeElement.click();
      this.TaskForm.reset();
    });
  }

  markAsCompleted(id: number, taskid: number) {

    this.taskService.updateTaskAsCompleted(id, taskid).subscribe(data => {
      if (data.Status.toLowerCase() === 'success') {
        if(this.loggedInUserRole === "ta"){
          this.service.TeamAdminTaskReload();
        }else if(this.loggedInUserRole === "member"){
          this.service.MemberTaskReload();
        }else{
          this.service.adminTaskReload();
        }
      // this.loggedInUserRole === 'member' ? this.service.MemberTaskReload() : this.service.adminTaskReload();
        this.countries$ = this.service.countries$;
        this.total$ = this.service.total$;
        this.priorityCollection$ = this.service.priorityCollection$;
      }
    });
  }

  rescheduleTaskBtnClick(template: TemplateRef<any>, taskId: string) {
    this.rescheduleTaskId = taskId;
    this.rescheduleTaskmodalRef = this.modalService.show(template, { class: 'second' });
  }
  rescheduleConfirmClick() {
    let date = new Date(this.bsValue);
    let fullDate = date.setHours(this.mytime1.getHours(), this.mytime1.getMinutes());
    let rescheduleDate = Math.floor(new Date(fullDate).getTime() / 1000.0);

    let taskObj = {
      TaskId: this.rescheduleTaskId,
      FieldName: 'duedate',
      FieldValue: rescheduleDate
    };
    this.taskService.updateTaskInfo(taskObj).subscribe(data => {
      if (data.Status.toLowerCase() === 'success') {
        this.rescheduleTaskmodalRef.hide();
        if(this.loggedInUserRole === "ta"){
          this.service.TeamAdminTaskReload();
        }else if(this.loggedInUserRole === "member"){
          this.service.MemberTaskReload();
        }else{
          this.service.adminTaskReload();
        }
       // this.loggedInUserRole === 'member' ? this.service.MemberTaskReload() : this.service.adminTaskReload();
        this.countries$ = this.service.countries$;
        this.total$ = this.service.total$;
        this.priorityCollection$ = this.service.priorityCollection$;
      }
    });

  }

  logACallBtnClick(template: TemplateRef<any>, objectId: string, objectIntId: number) {
    this.objectData = { ObjectId: objectId, ObjectIntId: objectIntId };
    this.callLogModalRef = this.modalService.show(template, { class: 'second' });
  }
  AddLogACallToContact(template: TemplateRef<any>) {
    this.submitted = true;
    if (this.callLogForm.invalid) {
      return;
    }

    this.callLogForm.patchValue({ ObjectId: this.objectData.ObjectId });
    this.callLogForm.patchValue({ ObjectIntId: this.objectData.ObjectIntId });
    this.callLogForm.patchValue({ ObjectType: 'task' });

    this.callsService.addCalls(this.callLogForm.value).subscribe(data => {
      if (data.Status === 'Success') {
        this.callLogModalRef.hide();
        this.confirmationMessage = "Call was logged successfully..!";
        this.confirmationCallLogModalRef = this.modalService.show(template, { class: 'second' });
      }
    });

  }

  sendEmailBtnClick(template: TemplateRef<any>, email: string, contactId: string) {
    this.contactEmail = email;
    this.objectData = { ObjectId: contactId, ObjectIntId: '' };
    this.emptyTemplateModal = this.modalService.show(template, { class: 'modal-lg' });
  }

  sendManualEmail(template: TemplateRef<any>) {

    this.ManualEmailForm.value.to = this.contactEmail;
    let ccList = this.ManualEmailForm.value.CC;
    let ccArray = [];

    if (ccList != "") {
      ccArray.push(ccList);
    }
    this.ManualEmailForm.value.CC = ccArray;

    this.ManualEmailForm.value.contactid = this.objectData.ObjectId;

    this.ManualEmailForm.value.BCC = [];
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.ManualEmailForm.value.AssignedTeam = TeamInfo.TeamId;
    this.ManualEmailForm.value.AssignedTeamIntId = TeamInfo.Id;
    this.apiService.sendManualEmailToContact(this.ManualEmailForm.value).subscribe(data => {
      if (data.Status === 'Success') {
        this.emptyTemplateModal.hide();
        this.confirmationMessage = "An email was sent successfully..!";
        this.confirmationCallLogModalRef = this.modalService.show(template, { class: 'second' });

      }

    });
  }


}
