import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { EmailApiService } from 'src/services/emails.services';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { CallsApiService } from 'src/services/calls.services';
import { MeetingApiService } from 'src/services/meeting.services';
import { NotesApiService } from 'src/services/notes.services';
import { NgxSpinnerService } from 'ngx-spinner';
import { GridOptions, CellClickedEvent } from 'ag-grid-community';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { UtilitiesService } from 'src/services/utilities.services';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.css']
})

export class ActivitiesComponent implements OnInit,OnDestroy {
  
 emails:any;
 calls:any;
 meetingList:any;
 notes:any;
 singleEmailActivity:any;
 notescount: number;
 callscount: number;
 taskscount: number;
 emailscount:number;
 meetingscount:number;
 sequencecount:number;
 activeTab = 'emails';
 public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowGroupPanelShow;
  public pivotPanelShow;
  public sideBar;
  public rowData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
  emailActivityModalRef: BsModalRef;
  public emailTypeFilteredData:any;
  periodSelect:string = "thisweek";
  subscriptions:Subscription [] = [];
  constructor(private emailService: EmailApiService,
    private callsService:CallsApiService,
    private meetingService:MeetingApiService,
    private notesService:NotesApiService,
    private modalService: BsModalService,
    private spinner: NgxSpinnerService,
    private utilities:UtilitiesService) {
      this.sideBar = {
        toolPanels: [
          {
            id: "columns",
            labelDefault: "Columns",
            labelKey: "columns",
            iconKey: "columns",
            toolPanel: "agColumnsToolPanel"
          },
          {
            id: "filters",
            labelDefault: "Filters",
            labelKey: "filters",
            iconKey: "filter",
            toolPanel: "agFiltersToolPanel"
          }
        ],
        defaultToolPanel: ""
      
   } 

  this.columnDefs = [
    {
      headerName: "ID",
      field: "Id",
      hide: true
    },
    {
      headerName: "Subject",
      field: "Subject",
      cellRenderer: function(params) {
        return "<a href='javascript:void(0);'>"+params.data.Subject+"</a>";
    }
  },
    {
      headerName: "Type Of Email",
      field: "LabelType"
    },
    {
      headerName: "Contact Name",
      field: "ContactName",
      cellRenderer: function(params) {
        return (
          '<a href="contacts/viewcontact/' +
          params.data.ObjectIntId +
          '""target="_blank">' +
          params.value +
          "</a>"
        );
      }
    },    
    {
      headerName: "Account Name",
      field: "AccountName"
    },
    {
      headerName: "Assigned To",
      field: "AssigneeName"
    },
    {
      headerName: "Created on",
      field: "CreatedOn"
    },
    {
      headerName: "Read Status",
      field:"IsRead",
      cellRenderer: function(params) {
       if(params.value > 0){
         return "Read";
       }else{
         return "UnRead";
       }
        
      }
    },
    
    {
      headerName: "Reply Status",
      field:"IsReplied",
      cellRenderer: function(params) {
        if(params.value > 0){
          return "Replied";
        }else{
          return "Not Replied";
        }
         
       }
    }
  ];
  this.autoGroupColumnDef = {
    headerName: "Group",
    //width: 200,
    field: "accountname",
    valueGetter: function(params) {
      if (params.node.group) {
        return params.node.key;
      } else {
        return params.data[params.colDef.field];
      }
    },
    headerCheckboxSelection: true,
    cellRenderer: "agGroupCellRenderer",
    cellRendererParams: { checkbox: true }
  };
  this.defaultColDef = {
    editable: false,
    enableRowGroup: true,
    enablePivot: true,
    enableValue: true,
    sortable: true,
    resizable: true,
    filter: true
    //  width: 250
  };
  this.rowSelection = "multiple";
  this.rowGroupPanelShow = "false";
  this.pivotPanelShow = "always";
  this.cacheBlockSize = 10;
  this.paginationPageSize = 10;
}
gridOptions: GridOptions = {
  rowHeight: 50,
  headerHeight: 52
};
onGridReady(params) {
  this.gridApi = params.api;
  this.gridColumnApi = params.columnApi; 
  this.getEmails();
}

onCellClicked($event: CellClickedEvent,template: TemplateRef<any>) {
  if($event.colDef.field == "Subject"){
  this.singleEmailActivity = $event.data;
  this.emailActivityModalRef = this.modalService.show(template, { class: "modal-xl", backdrop: 'static' });
  }
}

onFilterTextBoxChanged(event: any) {
  let searchText = event.currentTarget.value;
  if (searchText.length > 1) {
    this.gridOptions.api.setQuickFilter(searchText);
  } else {
    this.gridOptions.api.setQuickFilter(null);
  }
}
  ngOnInit() {
    this.spinner.show();  
   
    this.notescount = 0;
    this.callscount = 0;
    this.taskscount = 0;
    this.sequencecount = 0;
    this.emailscount = 0;
    this.meetingscount = 0;   

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  getCallsTabs(){
    this.getCalls();
  }
  getMeetingsTab(){
    this.getMeetings();
  }

  EmailTypeChangedHandler(selectedValue:string){
    selectedValue = selectedValue.toLowerCase(); 
    this.periodSelect = "thisweek";
    var thisWeekDates= this.utilities.thisWeekDates();          
    let thisWeekStartDate = this.utilities.dateConvertion(thisWeekDates.startDate);
    let thisWeekEndDate = this.utilities.dateConvertion(thisWeekDates.endDate);      
  
    if (selectedValue == "sent") {
      this.emailTypeFilteredData = this.emails.filter(a=>a.LabelType.toLowerCase() == "sent");     
      
    }else if(selectedValue == "inbox") {
      this.emailTypeFilteredData = this.emails.filter(a=>a.LabelType.toLowerCase() == "inbox");
    }else{
      this.emailTypeFilteredData = this.emails;     
    }
    this.rowData = this.emailTypeFilteredData.filter(        
      i=>new Date(i.CreatedOn) >= new Date(thisWeekStartDate) && new Date(i.CreatedOn) <= new Date(thisWeekEndDate)
    );
    
    this.emailscount = this.rowData.length;
  }

  CreatedChangedHandler(selectedValue:string) {
    selectedValue = selectedValue.toLowerCase();   
    var date = new Date(), y = date.getFullYear(), m = date.getMonth(); 
        if (selectedValue == "all") {
          this.rowData = this.emailTypeFilteredData;
          
        }else if (selectedValue === "thismonth"){
          var currentMonthFirstDay = new Date(y, m, 1);
          var currentMonthLastDay = new Date(y, m + 1, 0);
          var currentMonthStartDate = this.utilities.dateConvertion(currentMonthFirstDay);
          var currentMonthEndDate = this.utilities.dateConvertion(currentMonthLastDay);
          this.rowData = this.emailTypeFilteredData.filter(
            i=>new Date(i.CreatedOn) >= new Date(currentMonthStartDate) && new Date(i.CreatedOn) <= new Date(currentMonthEndDate)
          );
        }else if (selectedValue === "thisweek"){
          var thisWeekDates= this.utilities.thisWeekDates();          
          let thisWeekStartDate = this.utilities.dateConvertion(thisWeekDates.startDate);
          let thisWeekEndDate = this.utilities.dateConvertion(thisWeekDates.endDate); 
     
          this.rowData = this.emailTypeFilteredData.filter(        
            i=>new Date(i.CreatedOn) >= new Date(thisWeekStartDate) && new Date(i.CreatedOn) <= new Date(thisWeekEndDate)
          );
        }else if (selectedValue === "today"){
          let currentDate = new Date();
          let todayDate = this.utilities.dateConvertion(currentDate);      
          this.rowData = this.emailTypeFilteredData.filter(        
            i=>i.CreatedOn === todayDate
          );
        }
          
        this.emailscount = this.rowData.length;
  }

  getEmails() {
    this.emailService.getAllEmails().subscribe(data => {
      this.spinner.hide();
      this.emails = JSON.parse(data.Data);
      let inboxData = this.emails.filter(a=>a.LabelType.toLowerCase() == "inbox");
      var thisWeekDates= this.utilities.thisWeekDates();          
      let thisWeekStartDate = this.utilities.dateConvertion(thisWeekDates.startDate);
      let thisWeekEndDate = this.utilities.dateConvertion(thisWeekDates.endDate); 
 
      this.rowData = inboxData.filter(        
        i=>new Date(i.CreatedOn) >= new Date(thisWeekStartDate) && new Date(i.CreatedOn) <= new Date(thisWeekEndDate)
      );
      this.emailTypeFilteredData = inboxData;  
      this.emailscount = this.rowData.length;
     
    });
  }

  getCalls() {
    this.callsService.getAllCalls().subscribe(data => {
      if (data.Status === 'Success') {
        this.calls = JSON.parse(data.Data);
        this.callscount = this.calls.length;
      }
    });
  }

  getMeetings() {
    this.meetingService.getAllMeetings().subscribe(data => {
      if (data.Status === 'Success') {
      this.meetingList = JSON.parse(data.Data);
      this.meetingscount = this.meetingList.length;
      }
    });
  }

  
  getNotes() {
    this.notesService.getAllNotes()
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.notes = JSON.parse(data.Data);
          this.notescount = this.notes.length;
        }
      });
  }

}
