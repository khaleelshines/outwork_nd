import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { IGetRowsParams, IDatasource, GridOptions, CellClickedEvent } from 'ag-grid-community';
import { NgxSpinnerService } from 'ngx-spinner';
import { AccountApiService } from 'src/services/accounts.services';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountsActionItemsComponent } from 'src/app/custom_components/accounts-action-items/accounts-action-items.component';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { teammanagementservice } from 'src/services/teamsmanagement.services';
import { Subscription } from 'rxjs';
import { CommonFieldsService } from 'src/services/commonfields.services';
import { UtilitiesService } from 'src/services/utilities.services';
import { ContactApiService } from 'src/services/contacts.services';
import { AgGridAngular } from 'ag-grid-angular';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
declare var jQuery: any;
@Component({
  selector: 'app-accountslist',
  templateUrl: './accountslist.component.html',
  styleUrls: ['./accountslist.component.css']
})
export class AccountslistComponent implements OnInit {

  public gridApi;
  public gridColumnApi
  public columnDefs;
  public defaultColDef;
  public autoGroupColumnDef;
  public cacheOverflowSize;
  public maxConcurrentDatasourceRequests;
  public infiniteInitialRowCount;
  public rowData: any;
  public recordscount: any;
  public accountsData: any;
  public rowSelection;
  paginationPageSize: number;
  cacheBlockSize: number;
  mainTab:string = "created";
  periodType:string = "thisweek";
  engSelect:string = "thisweek";
  crdSelect :string = "thisweek";
  globalSearch:any="";
  IsMember: boolean;
  userInfo: any;
  loggedInUserRole: string;
  accountForm: FormGroup;
  accountUpdateForm: FormGroup;
  contactForm: FormGroup;
  submitted = false;
  selectedStage: string;
  selectedSource:string;
  selectedType:string;
  accountResponseInfo:string;
  usersData:any;
  subscriptions:Subscription[] = [];
  industriesList:any = [];
  selIndustryInfo:any;
  selectedDesignationCat:string;
  designationCategoryList:any;
  industryintiderror:boolean = false;
  selDesigInfo:any;
  accountIntId:number = 0;
  newaccountIntId:number = 0;
  accountIntIdsList:any;
  responseInfo:string;
  deleteresponseInfo:string;
  designationsList:any = [];
  assigneeId:number;
  checkboxSelectionCount:number = 0;
  IsUpdateAssigneeEnabled = true;
  errorMessage:string;
  IsErrorOccurred:boolean = false;
   //#region Desgination declaration
   IsNewDesignChecked:boolean = false;
   IsDesignExists:boolean = false;
   IsContactExists:boolean = false; 
   //#endregion
   loading = false;
   modalResponseRef:BsModalRef; 
   deleteAccountmodalRef: BsModalRef;
  currentAssigneeIntId:number;
  defaultOwnerValue:string="";
  recordUpdateData : any;
  private params: any;
  updateindustry:any;
  updateaccountsource:any;
  updateaccounttype:any;
  deleteRecodeId:any;
  updateRecodeId:any;
 //#region dropdown list declaration
 campaigntypeList:any;
 selectedCampaign:string;
 designationArr:any = [];
 accountNameArr:any = [];
 
 //#endregion
  @ViewChild('agGrid', { static: false }) agGrid: AgGridAngular;
  get f() {
    return this.accountForm.controls;
  }

  get uf() {
    return this.accountUpdateForm.controls;
  }
  @ViewChild("successAlertModal", { static: false })  successAlertModal: ElementRef;

  @ViewChild("closeAddAccountModal", { static: false })
  closeAddAccountModal: ElementRef;
  @ViewChild("closeUpdateAccountModal", { static: false })
  closeUpdateAccountModal: ElementRef;
  infoModalRef: BsModalRef;
  @ViewChild("closeAddContactModal", { static: false })
  closeAddContactModal: ElementRef;
  @ViewChild("closeUpdateAssigneeModal", { static: false })
  closeUpdateAssigneeModal: ElementRef;
  get cf() {
    return this.contactForm.controls;
  }
  constructor(
    private router:Router,
    private spinner: NgxSpinnerService,
    private accountsService: AccountApiService,
    private auth:AuthService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private teamService:teammanagementservice,
    private commonService:CommonFieldsService,
    public utilities:UtilitiesService,
    private modalBsService: BsModalService,
    private route:ActivatedRoute,
    private contactsService:ContactApiService
  ) { 
    this.loading = false;
    this.columnDefs = [    
  //          {
  //       field: 'RowSelect',
  //  headerName: ' ',
  //  checkboxSelection: true,
  //  suppressMenu: true,
  //  suppressSorting: true,
  //  headerCheckboxSelection: true,

  //     }, 
      {
        headerName: 'Account Name', field: 'AccountName',
        filter: 'agTextColumnFilter',
        headerCheckboxSelection: true,
        headerCheckboxSelectionFilteredOnly: true,
       checkboxSelection: true,
        filterParams: {
          suppressAndOrCondition: true
        },        
           cellRenderer: (data) => {
          if (data.value != undefined) 
          { 
            return '<a href="javascript:void(0);">' + data.value + '</a>';
           }
        }
       
        },
        {
          headerName: "Industry",
          field: "industry",   
          filter: 'agTextColumnFilter'
        },
        {
          headerName: "Stage",
          field: "Stage"
        },
        {
          headerName: "Last Contacted Date",
          field: "LastContactedDate"
        }, {
          headerName: "Last Contact Type",
          field: "LastContactType"
        },
        {
          headerName: "No Of Contacted",
          field: "NoOfContacted"
        },{
          headerName: "Assigned To",
          field: "AssigneeName"
        },
        {
          headerName: "Created On",
          field: "SyncDate"
        },{
          headerName: "Actions",
          pinned: "right",
          width: 315,
          editable: false,
          cellStyle: { "padding-top": "2px" },
          cellRenderer: function(params){
          return `<div class="">    
          <span class="view-badge d-inline-block" title="Update" data-action-type="rowupdate" data-toggle="modal" data-target='#updateAccountModal'>
              <i class="fa fa-pencil-alt pointer" data-action-type="rowupdate1"></i>
            </span>
            <span class="view-badge d-inline-block" title="Delete" data-action-type="rowdelete" data-toggle="modal" data-target='#confirmationAccountDeleteModal'>
              <i class="fa fa-trash" data-action-type="rowdelete1"></i>
            </span>           
            <span class="view-badge d-inline-block" data-toggle="modal" data-target="#createContactModal">
              <i class="fas fa-id-badge" data-placement="left" data-toggle="tooltip" title="Add Contact"></i>
            </span>
            <span class="view-badge d-inline-block" data-toggle="modal" data-target="#updateAssigneeModal">
              <i class="fas fa-user-cog" data-placement="left" data-toggle="tooltip" title="Update Assignee"></i>
            </span>
          </div> `;
}
         // cellRendererFramework: AccountsActionItemsComponent
        }
      
    ];

    this.autoGroupColumnDef = {     
      headerCheckboxSelection: true,
      cellRenderer: "agGroupCellRenderer",
      cellRendererParams: { checkbox: true }
    };
    this.defaultColDef = {
      editable: true,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true,
    };
    this.rowSelection = 'multiple';
    this.loggedInUserRole = this.auth.getRole();
    if (this.loggedInUserRole === "member") {
      this.IsMember = true;
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        IntUserId: userDetails1.IntUserId
      };
    }else {
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        IntUserId: userDetails1.IntUserId,
        companyIntId:userDetails1.CompanyInfo[0].CompanyIntId
      };
    }
  }
  gridOptions: GridOptions = {
    pagination: true,
    rowModelType: 'infinite',
    cacheBlockSize: 10,
    paginationPageSize: 10,
    rowHeight: 50,
    headerHeight: 52,   
    angularCompileHeaders: true
  }; 

 
  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      const savedModel = this.gridApi.getFilterModel();
      this.spinner.show();
    let contactSub =  this.accountsService.
    getaccountslist(params, this.gridOptions.paginationPageSize,this.globalSearch,
      this.mainTab,this.periodType)
        .subscribe(data => {
          if(data.Code == "SUC-200"){
          this.accountsData = JSON.parse(data.Data);
          
          params.successCallback(
            this.accountsData.ListOfAccounts,
            this.accountsData.AccountsCount
          );          
          }
          this.spinner.hide();          
        });
     
    }
  }

  
  agInit(params: any): void {
    this.params = params;
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridApi.setDatasource(this.dataSource);
    this.deSelectAllSelected();
  }

  onPageSizeChanged(newPageSize) {
    this.deSelectAllSelected();
    this.gridApi.paginationSetPageSize(Number(newPageSize));
    this.gridApi.setDatasource(this.dataSource);
  }
  ngOnInit() {
    this.accountForm = this.formBuilder.group({
      accountname: ["", Validators.compose([Validators.required])],
      industryid:[""],
      subindustry: [""],
      accountsource:[this.selectedSource],
      accounttype:[this.selectedSource],
      website: ["",
      [
         Validators.required, 
       // Validators.pattern("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}$")
        Validators.pattern("[www]+\.([a-z.]{2,6})[/\\w .-]*/?")
        // Validators.pattern("([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?")
       ]],
      companylinkedinurl: [""],
      email: [
        "",
        [
         // Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]
      ],
      mobilenumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      stage: [this.selectedStage,Validators.compose([Validators.required])],
      AssignedToIntId: ["", Validators.required],
      AssignedTeamIntId: [],
      TeamId: [],
      noofemployees:[],
      revenue:[],
      companyaddress:[]
    });
    this.accountUpdateForm = this.formBuilder.group({
      accountname: ['', Validators.compose([Validators.required])],
      industryid:[this.updateindustry],
      subindustry: [''],
      accountsource:[this.updateaccountsource],
      accounttype:[this.updateaccounttype],
      website: ['',
      [
         Validators.required, 
       // Validators.pattern("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}$")
         Validators.pattern("[www]+\.([a-z.]{2,6})[/\\w .-]*/?")
        // Validators.pattern("([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?")
       ]],
      companylinkedinurl: [''],
      email: [
        '',
        [
         // Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]
      ],
      mobilenumber: ['',
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      stage: [this.selectedStage,Validators.compose([Validators.required])],
     AssignedToIntId: [this.assigneeId, Validators.compose([Validators.required])],
     //AssignedTeamIntId: [this.updateAssignedteamintid],
      //TeamId: [this.updateteamid],
      noofemployees:[''],
      revenue:[''],
      companyaddress:[''],
      id:[]
    });

    this.contactForm = this.formBuilder.group({
      firstname: ["", Validators.compose([Validators.required])],
      lastname: [""],
      email: ["",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$")
        ]
      ],
      mobilenumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      assignedtointid: ["", Validators.required],
      stage: [this.selectedStage, Validators.compose([Validators.required])],
      accountintid: [],
      accountname: [],
      TeamId: [],
      designationid:[],
      designation:[],
      designation_category:["",this.selectedDesignationCat],
      personallinkedinurl:[],
      AssignedTeamIntId: [],
      campaigntype:["",this.selectedCampaign]
    });

    this.route.params.subscribe(params=>{
      let dashboardFilter = params['type'];
      if(dashboardFilter != undefined && dashboardFilter != ""){
        switch (dashboardFilter) {
          case 'en-thisweek' || undefined:
            this.engSelect = "thisweek";
            this.mainTab = 'engaged';
            this.EngagedChangedHandler("thisweek");
            break;
          case 'en-thismonth':
            this.engSelect = "thismonth";
            this.mainTab = 'engaged';
            this.EngagedChangedHandler("thismonth");
            break;
          case 'en-today':
            this.engSelect = "today";
            this.mainTab = 'engaged';
            this.EngagedChangedHandler("today");
            break;
          case 'en-lastweek':
            this.engSelect = "lastweek";
            this.mainTab = 'engaged';
            this.EngagedChangedHandler("lastweek");
            break;
          case 'cr-today':
            this.crdSelect = "today";
            this.mainTab = 'created';
            this.CreatedChangedHandler("today");
            break;
          case 'cr-thisweek':
            this.crdSelect = "thisweek";
            this.mainTab = 'created';
            this.CreatedChangedHandler("thisweek");
            break;
          case 'cr-lastweek':
            this.crdSelect = "lastweek";
            this.mainTab = 'created';
            this.CreatedChangedHandler("lastweek");
            break;     
          case 'cr-thismonth':
            this.crdSelect = "thismonth";
            this.mainTab = 'created';
            this.CreatedChangedHandler("thismonth");
            break;     
          default:
            this.engSelect = "thisweek";
            this.mainTab = 'engaged';
            this.EngagedChangedHandler("thisweek");
            break;
        }
      }
     });

    this.getDesignationCategoryList();
    this.getCampaignTypeList();

    let teamsSubscription = this.teamService.getTeamMembersWithoutTeamId().subscribe(data => {
      this.usersData = data;
    });
   if(teamsSubscription){
    this.subscriptions.push(teamsSubscription);
   }
  }

    //#region contact checking
    focusOutEmailTxt(event:any,template: TemplateRef<any>){
      let emailValue = event.target.value;
      if(emailValue != "" && emailValue != undefined){
        this.IsContactExists = false;
        this.contactsService.isContactExists(emailValue,0).subscribe(data=>{
          if(data.Code == "SUC-200"){
              let isContactExisting = JSON.parse(data.Data);
              if(parseInt(isContactExisting) > 0){ 
                this.IsContactExists = true;            
                this.responseInfo = `<div class="alert alert-danger mb-0">     
                  <h6>Email already exist. Please try with another..!</h6> 
                </div>`; 
                this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
                setTimeout(() => {
                  this.modalResponseRef.hide();
                }, 3000);
              }
          }
        });
      }
    }
     //#endregion

  addAccount(template: TemplateRef<any>) {    
    this.submitted = true;  
   
    let indusryId = 0;
    if (this.selIndustryInfo == undefined) {      
      this.industryintiderror = true;
      return;
    }else{
      indusryId = this.selIndustryInfo.id;
      this.accountForm.patchValue({ industryid: indusryId});
    }
    if (this.accountForm.invalid) {
      return;
    } 
    this.loading = true;
    let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
    this.accountForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.accountForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });   
    
    this.accountsService.AddAccount(this.accountForm.value).subscribe(data => {
      if (data.Status === "Success") {    
        this.newaccountIntId = parseInt(data.Data);   
        this.accountResponseInfo = "<h6>Do you want to add contacts for this account..!</h6>";       
      } else if(data.Code == "FAL-409"){
        this.newaccountIntId = 0;
       this.accountResponseInfo = "<h6>Account already exists..!</h6>";        
      }else{
        this.newaccountIntId = 0;
        this.accountResponseInfo = "<h6>Account creation failed..!</h6>"; 
      }
      this.loading = false;
      this.infoModalRef = this.modalService.show(template, { class: "modal-m",backdrop  : 'static' });
      this.onReset();
      this.closeAddAccountModal.nativeElement.click();
   });
  }

  loadAccounts(){
    this.infoModalRef.hide();
    this.periodType = this.crdSelect = 'today';
    this.gridApi.setDatasource(this.dataSource);
  }

  addContactsProceed(){
    this.infoModalRef.hide();
    this.router.navigate(['/accounts/viewaccount/'+this.newaccountIntId]);
  }

  onReset() {
    this.industryintiderror = false;
    this.submitted = false;
    this.loading = false;    
    this.accountForm.reset();
  }

  onResetUpdateForm(){
    this.industryintiderror = false;
    this.submitted = false;
    this.loading = false;    
    this.accountUpdateForm.reset();  
  }

  sctDesCatChangeHandler(selectedValue: string) {  
    this.selectedDesignationCat = selectedValue;
   }

  selectDesignation(){    
    console.log(this.selDesigInfo);      
  }
  
  //#region  common dropdowns
  getCampaignTypeList(){
    this.commonService.getCommonFields(3,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.campaigntypeList = JSON.parse(data.Data);
      }
    });
  }

  compTypeChangeHandler(selectedValue: string) {
    this.selectedCampaign = selectedValue;    
  }
  //#endregion

  desigSelectHandler(event:any){
    let desigArray =[];
    if(event.term.length >= 3){
    this.commonService.getCommonFields(2,event.term).subscribe(data=>{
      if (data.Status === "Success") {
        let designations = JSON.parse(data.Data);
        designations.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.id,
              name:element.labeltitle
            };       
         
            desigArray.push(singleItem);
        });
        this.designationsList = desigArray; 
      }
     });
    }
  }

  addContact(template: TemplateRef<any>) {
    this.submitted = true;  
    if (this.contactForm.invalid) {
      return;
    }
    if(this.IsNewDesignChecked && (this.contactForm.value.designation != null || this.contactForm.value.designation != undefined)){
      this.designationArr = [];
      let newDesignationValue = this.contactForm.value.designation;
      let object ={
        typeid:2,
        labeltitle:newDesignationValue
      };    
      this.commonService.checkMasterTypeExistance(object).subscribe(data=>{
        if(data.Code == "SUC-200"){
          let isDesignExisting = JSON.parse(data.Data);
          if(parseInt(isDesignExisting) > 0){
            this.IsDesignExists = true;
          }else {
            let emailValue = this.contactForm.value.email;  
            this.loading = true;
            if(emailValue){
                this.contactsService.isContactExists(emailValue,0).subscribe(data=>{
                  if(data.Code == "SUC-200"){
                      let isContactExisting = JSON.parse(data.Data);
                      if(parseInt(isContactExisting) > 0){
                        this.IsContactExists = true;        
                        this.loading = false;
                      }else {
                        this.proceedtoCreateContact(this.contactForm, template);
                    }
                  }else {
                      this.proceedtoCreateContact(this.contactForm, template);
                  }
                });
            }else {
              this.proceedtoCreateContact(this.contactForm, template);
            }
          }
        }
      });
    }else {
      let emailValue = this.contactForm.value.email;  
      this.loading = true;
      if(emailValue){
          this.contactsService.isContactExists(emailValue,0).subscribe(data=>{
            if(data.Code == "SUC-200"){
                let isContactExisting = JSON.parse(data.Data);
                if(parseInt(isContactExisting) > 0){
                  this.IsContactExists = true;        
                  this.loading = false;
                }else {
                  this.proceedtoCreateContact(this.contactForm, template);
              }
            }else {
                this.proceedtoCreateContact(this.contactForm, template);
            }
          });
      }else {
        this.proceedtoCreateContact(this.contactForm, template);
      }
    }

    if(this.IsContactExists){
      this.responseInfo = `<div class="alert alert-danger mb-0">     
      <h6>Email already exist. Please try with another..!</h6> 
            </div> `; 
    this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
    setTimeout(() => {
      this.modalResponseRef.hide();
    }, 3000);
    return;
    }
    if(this.IsDesignExists){
      this.responseInfo = `<div class="alert alert-danger mb-0">     
      <h6>Designation already exist. Please select from list..!</h6> 
    </div> `; 
    this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
    setTimeout(() => {
      this.modalResponseRef.hide();
    }, 2000);
    return;
    }

    

    if(this.designationArr[0] && this.designationArr[0].designation==1){
      
      }
  }

  proceedtoCreateContact(contactForm, template: TemplateRef<any>){
    let designationId = "";
    if(this.selDesigInfo == undefined || this.selDesigInfo == null){
      designationId = "";
    }else{
      designationId = this.selDesigInfo.id;
    }

    if(this.IsNewDesignChecked){
      designationId = "";
    }

    this.loading = true;
    let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
    this.contactForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.contactForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    this.contactForm.patchValue({ accountintid: this.accountIntId});
    this.contactForm.patchValue({ designationid: designationId });
    if (this.accountIntId != null) {
      this.contactsService
        .AddContact(this.contactForm.value)
        .subscribe(data => {
          this.loading = false;
          if (data.Status === "Success") {
            this.closeAddContactModal.nativeElement.click();          
            this.onContactReset();
           this.router.navigate(['/accounts/viewaccount/'+this.accountIntId]);
          }
        });
    }
  }


  onContactReset() {
    this.submitted = false;
    this.IsContactExists = false; 
    this.contactForm.reset();
  }

  selectIndustry(){  
     console.log(this.selIndustryInfo);
     this.accountUpdateForm.markAsDirty();
     this.industryintiderror = false;
     console.log(new Date);
   }

   industryChangeHandler(selectedObject:any, actionFrom){
    let industryArray =[];
       if(selectedObject || selectedObject.term.length >= 3 ){
         let searchString = '';
         if(selectedObject.term && selectedObject.term.length >= 3 && actionFrom=='search'){
          searchString = selectedObject.term;
         }else if(selectedObject && actionFrom=='update') {
          searchString = selectedObject;
         }
       let industrysSub = this.commonService.getCommonFields(1,searchString).subscribe(data => {
        if(data.Status == "Success"){
         let industries = JSON.parse(data.Data);
         industries.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.id,
              name:element.labeltitle
            };       
            industryArray.push(singleItem);
        });
        this.industriesList = industryArray; 
          if(actionFrom=='update')   {
            this.selIndustryInfo = industryArray[0];
          }   
        }
       });
       this.subscriptions.push(industrysSub);
        }   
    }

    getDesignationCategoryList(){
      this.commonService.getCommonFields(4,"").subscribe(data=>{
        if (data.Status === "Success") {
          this.designationCategoryList = JSON.parse(data.Data);
        }
       });
    }

  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText.length > 1) {
      this.globalSearch = searchText;   
      
    } else{
      this.globalSearch ="";
    }
    this.gridApi.setDatasource(this.dataSource);
  }

    //#region  Designations code block
    checkDesignValue(event: any) {
      let isChecked = event.target.checked;
      if (isChecked === true) {
        this.IsNewDesignChecked = true;
        this.designationsList = [];
        this.selDesigInfo = null;      
  
        if(this.contactForm.value.designation != "" || this.contactForm.value.designation != undefined){
          let newDesignationValue = this.contactForm.value.designation;
          let object ={
            typeid:2,
            labeltitle:newDesignationValue
          };    
          this.commonService.checkMasterTypeExistance(object).subscribe(data=>{
            if(data.Code == "SUC-200"){
              let isDesignExisting = JSON.parse(data.Data);
                if(parseInt(isDesignExisting) > 0){
                  this.IsDesignExists = true;
                }
            }
          });
        }
      } else {
        this.IsNewDesignChecked = false;
        this.IsDesignExists = false;      
      }    
    } 
  
 
    resetdesigs(){
      this.designationsList = []; 
      this.selDesigInfo = null;
    }
    focusOutDesignTxt(event:any,template: TemplateRef<any>){
      let newDesignationValue = event.target.value;
    if(newDesignationValue != "" && newDesignationValue != undefined){
      this.IsDesignExists = false;
      let object ={
        typeid:2,
        labeltitle:newDesignationValue
      };      
      this.commonService.checkMasterTypeExistance(object).subscribe(data=>{
         if(data.Code == "SUC-200"){
           let isDesignExisting = JSON.parse(data.Data);
            if(parseInt(isDesignExisting) > 0){
              this.IsDesignExists = true;
              this.responseInfo = `<div class="alert alert-danger mb-0">     
              <h6>Designation already exist. Please select from list..!</h6> 
            </div> `; 
            this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
            setTimeout(() => {
              this.modalResponseRef.hide();
            }, 2000);
            }
         }
      });
    }
    }
     //#endregion
    

  // Assignee update
  AssigneeChangeHandler(selectedValue: string) {  
    let assingneeId = selectedValue;
    if(assingneeId != ""){
      if(parseInt(assingneeId) == this.currentAssigneeIntId){
        this.IsErrorOccurred = false;
        this.errorMessage = "Already assingned with same name";
      }else{
      this.IsErrorOccurred = true;
    this.assigneeId = parseInt(assingneeId);
      }
    }else{
      this.IsErrorOccurred = false;
      this.errorMessage = "Please select assingnee";      
    }
   }

   updateAssignee(){
     let assingeeIntId = this.assigneeId;
     let objectIdsArray = [];
     
     if(this.accountIntIdsList != null && this.accountIntIdsList.length > 0){
      this.accountIntIdsList.forEach(element => {
        objectIdsArray.push(element.Id);
        });
     }else{
      let objectId = this.accountIntId;
      objectIdsArray.push(objectId);
     }
    
     let inputObject = {
      ObjectType: 'account',
      ObjectIds:objectIdsArray,
      AssignedToId:assingeeIntId
     }
     this.contactsService.updateAssignee(inputObject).subscribe(data => {
      this.closeUpdateAssigneeModal.nativeElement.click();    
      this.gridApi.setDatasource(this.dataSource);
      this.accountIntIdsList = [];
      this.IsUpdateAssigneeEnabled = true;
      this.setDefaultOwner();
      this.deSelectAllSelected();
    });
   }

   setDefaultOwner(){
     this.defaultOwnerValue = "";
     this.IsErrorOccurred = true;
   }

   updateBulkAssignees(){
    const newSelectedNodes = this.agGrid.api.getRenderedNodes();
    const selectedData = newSelectedNodes.map( node => node.data );

    //const selectedNodes = this.agGrid.api.getSelectedNodes();
    //const selectedData = selectedNodes.map( node => node.data );
    this.accountIntIdsList = selectedData;
   }

  // end Assignee update

  selectSourceChangeHandler(selectedValue: string) {  
    this.selectedSource = selectedValue;    
  }

  selectTypeChangeHandler(selectedValue: string) {  
    this.selectedType = selectedValue;    
  }
  selectStageChangeHandler(selectedValue: string) {  
    this.selectedStage = selectedValue;
  }

  onCellClicked($event: CellClickedEvent,) {
    let id = $event.data.Id;
    this.currentAssigneeIntId = $event.data.AssignedToIntId;  
    this.accountIntId = id;
    if($event.colDef.field == "AccountName"){    
     
     this.router.navigate(['/accounts/viewaccount/'+id]);
    }
    //else if($event.colDef.headerName == "Actions"){    
     // let accountIntId = $event.data.Id;
     // let actionitem = $event.event.target.getAttribute("data-action-type")
     // let actionType = $event.event.target.dataset.actionType;
    //  }
  }

onRowSelected(event) { 
  let selectionStatus = event.node.selected;
  if(selectionStatus == true){
    this.checkboxSelectionCount += 1;
  }else{
    this.checkboxSelectionCount -= 1;
  }
   if(this.checkboxSelectionCount > 0){
    this.IsUpdateAssigneeEnabled = false;
   }else{this.IsUpdateAssigneeEnabled = true;}
  }

  mainFilterTabsChanged(clickedText:string,mainTab:string) {     
     this.mainTab = mainTab.toLowerCase();
     this.engSelect = this.crdSelect = this.periodType = "thisweek";
     this.gridApi.setDatasource(this.dataSource);
 
   }
   EngagedChangedHandler(clickedText) {
    this.mainTab = "engaged";
     clickedText = clickedText.toLowerCase();
     this.periodType = clickedText;
     this.gridApi.setDatasource(this.dataSource);
   }
   CreatedChangedHandler(clickedText) {
     clickedText = clickedText.toLowerCase();
    this.mainTab = "created";
     this.periodType = clickedText;
     this.gridApi.setDatasource(this.dataSource);
   }

   onPaginationChanged(event:any){
     let eventValue = event;
     if(eventValue.newPage == true){     
     this.IsUpdateAssigneeEnabled = true;
     this.deSelectAllSelected();
     }
   }
   
   toggleSelectAll(){
    var checked: boolean;
    if (jQuery("#accountsgridSelectAll"). is(":checked")) { 
      checked = true;
    }else {
      checked = false;
    }
    let len = 10;
    this.gridApi.forEachNode(node => {
      for (let i=0; i<len; i++) {
          node.setSelected(checked);
      }
    });
    this.IsUpdateAssigneeEnabled = true;
  }
  
  deSelectAllSelected(){    
    this.gridApi.forEachNode(function (node) {
      node.setSelected(false);
    });
    jQuery("#accountsgridSelectAll").prop("checked", false);
  }

  resetIndustrie(){
    this.selIndustryInfo = null;
    this.industriesList = [];
  }

  public onRowClicked(e) {
    if (e.event.target !== undefined || e.event.target !== null) {
      let actionType = e.event.target.getAttribute("data-action-type");
      this.recordUpdateData = e.node.data;
      this.resetIndustrie();
      
      if(actionType == 'rowupdate' || actionType == 'rowupdate1'){
        this.updateRecodeId = this.recordUpdateData.Id;
        if(this.recordUpdateData.industry){
          this.industryChangeHandler(this.recordUpdateData.industry, 'update');
        }

        let formFields = {
          accountname:this.recordUpdateData.AccountName,
          industryid:this.recordUpdateData.industryid,
          subindustry:this.recordUpdateData.subindustry, 
          accountsource:this.recordUpdateData.accountsource, 
          accounttype:this.recordUpdateData.AccountType,    
          website:this.recordUpdateData.website?this.recordUpdateData.website:'',
          companylinkedinurl:this.recordUpdateData.companylinkedinurl,
          email:this.recordUpdateData.Email,
          mobilenumber:this.recordUpdateData.mobilenumber,
          stage:this.recordUpdateData.Stage,
          AssignedToIntId:parseInt(this.recordUpdateData.AssignedToIntId)?this.recordUpdateData.AssignedToIntId:null,
          noofemployees:this.recordUpdateData.noofemployees,
          revenue:this.recordUpdateData.revenue,
          companyaddress:this.recordUpdateData.companyaddress,
          id:this.recordUpdateData.Id
        }; 

        this.selectedStage = this.recordUpdateData.Stage;
        this.assigneeId = parseInt(this.recordUpdateData.AssignedToIntId);

        this.updateindustry = this.recordUpdateData.industry;
        this.updateaccountsource = this.recordUpdateData.accountsource;
        this.updateaccounttype = this.recordUpdateData.AccountType;

        this.accountUpdateForm.patchValue({ stage: this.selectedStage});
        this.accountUpdateForm.patchValue({ accountsource: this.updateaccountsource}); 
        this.accountUpdateForm.patchValue({ accounttype: this.updateaccounttype});
        this.accountUpdateForm.patchValue({ id: this.recordUpdateData.Id});
        this.accountUpdateForm.setValue(formFields);
        
         
      }else if(actionType == 'rowdelete' || actionType == 'rowdelete1'){
        this.deleteRecodeId = this.recordUpdateData.Id;
        //this.deleteBtnAction("confirmationAccountDeleteModal");
        //this.deleteAccountmodalRef = this.modalService.show('confirmationAccountDeleteModal', {
          //class: "second"
        //});
      }
    }
  }

  deleteBtnAction(template: TemplateRef<any>){
    this.deleteAccountmodalRef = this.modalService.show(template, { class: "modal-m",backdrop  : 'static' });
  }

  updateAccount(template: TemplateRef<any>) {
    this.submitted = true;  
    let indusryId = 0;
    if (this.selIndustryInfo == undefined) {      
      this.industryintiderror = true;
      return;
    }else{
      indusryId = this.selIndustryInfo.id;
      this.accountUpdateForm.patchValue({ industryid: indusryId});
    }
    
    if (this.accountUpdateForm.invalid) {      
      return;
    }
    
    this.loading = true;
    let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
    this.accountUpdateForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.accountUpdateForm.patchValue({ AssignedTeamIntId: TeamInfo.Id }); 

    this.accountsService.UpdateAccount(this.accountUpdateForm.value).subscribe(data => {
      if (data.Status === "Success") {    
        this.newaccountIntId = parseInt(this.updateRecodeId);   
        this.accountResponseInfo = "<h6>Do you want to add contacts for this account..!</h6>";       
      } else if(data.Code == "FAL-409"){
        this.newaccountIntId = 0;
       this.accountResponseInfo = "<h6>Account already exists..!</h6>";        
      }else{
        this.newaccountIntId = 0;
        this.accountResponseInfo = "<h6>Account updation failed..!</h6>"; 
      }
      this.onResetUpdateForm();
      this.loading = false;
      this.closeUpdateAccountModal.nativeElement.click();
      this.infoModalRef = this.modalService.show(template, { class: "modal-m",backdrop  : 'static' });
   });
  }

  deleteAccountYes(template: TemplateRef<any>) {    
          let formData = new FormData();
          formData.append('record_id', this.deleteRecodeId);
          formData.append('table_name', 'accounts'); 
          formData.append('action', 'delete'); 
    let deleteObject = {
      "tablename":"accounts",
      "ids":[this.deleteRecodeId]
    };
    this.spinner.show();
    this.accountsService.DeleteAccount(deleteObject).subscribe(data => {
      if (data.Status === 'Success') {
        this.deleteresponseInfo = `
        <div class="confirmation-content success">
          <div class="icon confirm text-center">
          <i class="far fa-2x fa-check-circle text-success"></i>
          <h4>Deleted Successfully!!</h4>
          </div>            
        </div> `;   
        this.spinner.hide();
        this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
        setTimeout(() => {
         this.modalResponseRef.hide();
         this.gridApi.setDatasource(this.dataSource);
        }, 2000);
      }
    });    
  }

}