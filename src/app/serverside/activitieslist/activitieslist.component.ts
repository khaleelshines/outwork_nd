import { Component, OnInit, TemplateRef } from '@angular/core';
import { IDatasource, IGetRowsParams, GridOptions, CellClickedEvent } from 'ag-grid-community';
import { NgxSpinnerService } from 'ngx-spinner';
import { EmailApiService } from 'src/services/emails.services';
import { CallsApiService } from 'src/services/calls.services';
import { MeetingApiService } from 'src/services/meeting.services';
import { NotesApiService } from 'src/services/notes.services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-activitieslist',
  templateUrl: './activitieslist.component.html',
  styleUrls: ['./activitieslist.component.css']
})
export class ActivitieslistComponent implements OnInit {

  //#region ag-grid
  public gridApi;
  public gridColumnApi
  public columnDefs;
  public defaultColDef;
  public autoGroupColumnDef;
  public cacheOverflowSize;
  public maxConcurrentDatasourceRequests;
  public infiniteInitialRowCount;
  public rowData: any;
  public recordscount: any;
  public rowSelection;
  paginationPageSize: number;
  cacheBlockSize: number;
  //#endregion

  //#region filter options
  activeTab = 'emails';
  emailType = 'inbox';
  periodSelect:string = "thisweek";
  globalSearch:any="";
  //#endregion
  //#region modals
  emailActivityModalRef: BsModalRef;
  //#endregion

  //#region main declaration
  public emailsData: any;
  emails:any;
  calls:any;
  meetingList:any;
  notes:any;
  singleEmailActivity:any;
  notescount: number;
  callscount: number;
  taskscount: number;
  emailscount:number;
  meetingscount:number;
  sequencecount:number;
 
  //#endregion

  constructor(
    private spinner: NgxSpinnerService,
    private emailService: EmailApiService,
    private callsService:CallsApiService,
    private meetingService:MeetingApiService,
    private notesService:NotesApiService,
    private modalService: BsModalService,
    private router:Router,
  ) {

    this.columnDefs = [
      {
        headerName: "ID",
        field: "Id",
        hide: true
      },
      {
        headerName: "Subject",
        field: "Subject",
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
        cellRenderer: function(params) {
          if (params.value != undefined) 
          { 
          return "<a href='javascript:void(0);'>"+params.value+"</a>";
          }
      }
    },
      {
        headerName: "Type Of Email",
        field: "LabelType",
        filter:false,
        sortable:false
      },
      {
        headerName: "Contact Name",
        field: "ContactName",
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
        cellRenderer: (data) => {
          if (data.value != undefined) 
          {                    
         
          return '<a href="javascript:void(0);">' + data.value + '</a>';
           }
        }
      },    
      {
        headerName: "Account Name",
        field: "AccountName",
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
      },
      {
        headerName: "Assigned To",
        field: "AssigneeName",
        filter: 'agTextColumnFilter',
        filterParams: {
          suppressAndOrCondition: true
        },
      },
      {
        headerName: "Created on",
        field: "CreatedOn",
        filter:false
      },
      {
        headerName: "Read Status",
        field:"IsRead",
        filter:false,
        cellRenderer: function(params) {
         if(params.value > 0){
           return "Read";
         }else{
           return "UnRead";
         }
          
        }
      },
      
      {
        headerName: "Reply Status",
        field:"IsReplied",
        filter:false,
        cellRenderer: function(params) {
          if(params.value > 0){
            return "Replied";
          }else{
            return "Not Replied";
          }
           
         }
      }
    ];
  
    this.defaultColDef = {
      editable: false,
      enableRowGroup: true,
      enablePivot: true,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true
      //  width: 250
    };
   }

  
    gridOptions: GridOptions = {
    pagination: true,
    rowModelType: 'infinite',
    cacheBlockSize: 10,
    paginationPageSize: 10,
    rowHeight: 50,
    headerHeight: 52,   
    angularCompileHeaders: true
  }; 

 
  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      const savedModel = this.gridApi.getFilterModel();     
      this.spinner.show();
      this.emailService.
        getEmailActivitiesList(params, this.gridOptions.paginationPageSize, this.globalSearch,
          this.emailType, this.periodSelect)
        .subscribe(data => {
          if (data.Code == "SUC-200") {
            this.emailsData = JSON.parse(data.Data);

            params.successCallback(
              this.emailsData.ListOfEmails,
              this.emailsData.EmailsCount
            );
          }
          this.emailscount = this.emailsData.EmailsCount;
          if(this.gridOptions.paginationPageSize == 200){
            setTimeout(() => {
              this.spinner.hide();
            }, 10000);
          }else{
            this.spinner.hide();
          }         
          
        });

    }
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridApi.setDatasource(this.dataSource);
  }

  onPageSizeChanged(newPageSize) {
    this.gridApi.paginationSetPageSize(Number(newPageSize));
    this.gridApi.setDatasource(this.dataSource);
  }
  ngOnInit() {
    this.notescount = 0;
    this.callscount = 0;
    this.taskscount = 0;
    this.sequencecount = 0;
    this.emailscount = 0;
    this.meetingscount = 0;   
  }

  //#region Calls code
  getCallsTabs(){
    this.getCalls();
  }
  getMeetingsTab(){
    this.getMeetings();
  }
  getCalls() {
    this.callsService.getAllCalls().subscribe(data => {
      if (data.Status === 'Success') {
        this.calls = JSON.parse(data.Data);
        this.callscount = this.calls.length;
      }
    });
  }

  getMeetings() {
    this.meetingService.getAllMeetings().subscribe(data => {
      if (data.Status === 'Success') {
      this.meetingList = JSON.parse(data.Data);
      this.meetingscount = this.meetingList.length;
      }
    });
  }

  
  getNotes() {
    this.notesService.getAllNotes()
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.notes = JSON.parse(data.Data);
          this.notescount = this.notes.length;
        }
      });
  }
  //#endregion
 

  //#region filter options
  EmailTypeChangedHandler(selectedValue:string){
    selectedValue = selectedValue.toLowerCase(); 
    this.periodSelect = "thisweek";  
    this.emailType = selectedValue; 
    this.gridApi.setDatasource(this.dataSource);
  }

  CreatedChangedHandler(selectedValue:string) {
    selectedValue = selectedValue.toLowerCase();   
    this.periodSelect = selectedValue;
    this.gridApi.setDatasource(this.dataSource);
  }

  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText.length > 0) {
      this.globalSearch = searchText;   
      this.gridApi.setDatasource(this.dataSource);
    } else{
      this.globalSearch ="";
      this.gridApi.setDatasource(this.dataSource);
    }
    
  }
  //#endregion

  //#region columns clicked functionality

  onCellClicked($event: CellClickedEvent,template: TemplateRef<any>) {
    let contactIntId = $event.data.ObjectIntId
    if($event.colDef.field == "Subject"){
    this.singleEmailActivity = $event.data;
    this.emailActivityModalRef = this.modalService.show(template, { class: "modal-xl", backdrop: 'static' });
    }else if($event.colDef.field == "ContactName"){    
      this.router.navigate(['/contacts/viewcontact/'+contactIntId]);
      }
  }

  //#endregion

}
