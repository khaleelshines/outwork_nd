import { Component, OnInit, OnDestroy, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import "ag-grid-enterprise";
import { ContactApiService } from '../../../services/contacts.services';
import { IGetRowsParams, IDatasource, GridOptions, CellClickedEvent } from 'ag-grid-community';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { ContactsActionsComponent } from '../../custom_components/contacts-actions/contacts-actions.component';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AccountApiService } from 'src/services/accounts.services';
import { UserService } from 'src/services/userservice.services';
import { teammanagementservice } from 'src/services/teamsmanagement.services';
import { UtilitiesService } from 'src/services/utilities.services';
import { CommonFieldsService } from 'src/services/commonfields.services';
import { AuthService } from '../../guards/authguard/auth.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AgGridAngular } from 'ag-grid-angular';
declare var jQuery: any;
import { Location } from '@angular/common';

@Component({
  selector: 'app-addnewcontact',
  templateUrl: './addnewcontact.component.html',
  styleUrls: ['./addnewcontact.component.css']
})
export class AddnewcontactComponent implements OnInit {
  IsNewDesignChecked:boolean = false;
  IsDesignExists:boolean = false;   
  IsContactExists:boolean = false; 
  IsAccountExists:boolean = false;
  contactForm: FormGroup;
  submitted = false;
  loggedInUserRole: string;
  IsMember: boolean = false;
  userInfo: any;
  accountsList:any = [];
  selAccountInfo:any;
  selDesigInfo:any;
  designationsList12:any = [];
  designationsList:any;
  designationCategoryList:any;
  selectedStage: string;
  selectedDesignationId:number;
  selectedDesignationCat:string;
  usersData: any;
  subscriptions:Subscription [] = [];
  responseInfo:string;
  modalResponseRef:BsModalRef;  
  accountintiderror: boolean = false;
  accountnameerror: boolean = false;
  IsChecked: boolean = false;
  selectedAccountId: string;
  selectedOwnerId: string;
  campaigntypeList:any;
  selectedCampaign:string;

  
  designationArr:any = [];
  accountNameArr:any = [];
  loading:boolean = false;

  get f() { return this.contactForm.controls; }

  constructor(private contactsService: ContactApiService,
    private router:Router,
    private spinner: NgxSpinnerService,
    private accounts: AccountApiService,
    private userService: UserService,
    private teamService: teammanagementservice,
    private auth: AuthService,
    public utilities: UtilitiesService,
    private route:ActivatedRoute,
    private formBuilder: FormBuilder,
    private location: Location,
    private modalBsService: BsModalService,
    private commonfieldsService:CommonFieldsService) {        
      this.loading = false;
    this.loggedInUserRole = this.auth.getRole();
    if (this.loggedInUserRole === "member") {
      this.IsMember = true;
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        name: userDetails1.Name,
        IntUserId: userDetails1.IntUserId
      };
    }
  }

  desigSelectHandler(event:any){
    let desigArray =[];
    if(event.term.length >= 3){
    this.commonfieldsService.getCommonFields(2,event.term).subscribe(data=>{
      if (data.Status === "Success") {
        let designations = JSON.parse(data.Data);
        designations.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.id,
              name:element.labeltitle
            };       
         
            desigArray.push(singleItem);
        });
        this.designationsList12 = desigArray; 
      }
     });
    }
  }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      firstname: ["", Validators.compose([Validators.required])],
      lastname: [""],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$")
        ]
      ],
      mobilenumber: ["",
      Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(20)
      ])],
      assignedtointid: ["", Validators.required],
      stage: [this.selectedStage, Validators.compose([Validators.required])],
      accountintid: [],
      accountname: [],    
      TeamId: [],
      designationid:[],   
      designation:[],         
      designation_category:["",this.selectedDesignationCat],
      personallinkedinurl:[],
      AssignedTeamIntId: [],
      campaigntype:["",this.selectedCampaign]
    });
    
    this.getDesignationCategoryList();
    this.getCampaignTypeList();
   
   let teamSub = this.teamService.getTeamMembersWithoutTeamId().subscribe(data => {
      this.usersData = data;
    });
    this.subscriptions.push(teamSub);

  }   

  resetaccounts(){
    this.accountsList = []; 
    this.selAccountInfo = null;
  }

  checkValue(event: any) {
    let isChecked = event.target.checked;
    if (isChecked === true) {
      this.IsChecked = true;
      this.resetaccounts();
      if(this.contactForm.value.accountname !== null &&
        (this.contactForm.value.accountname !== "" || this.contactForm.value.accountname !== undefined)){
        let newAccountValue = this.contactForm.value.accountname;
        this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
          if(data.Code == "SUC-200"){
              let accountsList = JSON.parse(data.Data);
              if(accountsList.length > 0){
                this.IsAccountExists = true;
              }
          }
        });
      }   
    } else {
      this.IsChecked = false;
      this.accountnameerror = false; 
      this.IsAccountExists = false;  
    }
    /*
    if(this.submitted){
      if(isChecked === true){
        this.IsChecked = true;
        this.accountnameerror = true;
        this.accountintiderror = false;
      }else{
        this.IsChecked = false;
        this.accountnameerror = false;
        this.accountintiderror = true;
      }
    } */
  } 

  //#region  account
  focusOutAccountTxt(event:any,template: TemplateRef<any>){
    let newAccountValue = event.target.value;
    if(newAccountValue != "" && newAccountValue != undefined){
      this.IsAccountExists = false;       
      this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
        if(data.Code == "SUC-200"){
            let accountsList = JSON.parse(data.Data);
            if(accountsList.length > 0){
              this.IsAccountExists = true;
              this.responseInfo = `<div class="alert alert-danger mb-0">     
                <h6>Account already exist. Please select from list..!</h6> 
              </div> `; 
              this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
              setTimeout(() => {
                this.modalResponseRef.hide();
              }, 2000);
            }
        }
      });
    }
  }
 //#endregion
  resetdesigs(){
    this.designationsList = []; 
    this.selDesigInfo = null;
  }
  
  //#region  Designations code block
  checkDesignValue(event: any) {
   let isChecked = event.target.checked;
   if (isChecked === true) {
     this.IsNewDesignChecked = true;
     this.designationsList = [];
     this.selDesigInfo = null;      

     if(this.contactForm.value.designation != ""){
       let newDesignationValue = this.contactForm.value.designation;
       let object ={
         typeid:2,
         labeltitle:newDesignationValue
       };    
       this.commonfieldsService.checkMasterTypeExistance(object).subscribe(data=>{
         if(data.Code == "SUC-200"){
            let isDesignExisting = JSON.parse(data.Data);
            if(parseInt(isDesignExisting) > 0){
              this.IsDesignExists = true;
            }
         }
       });
     }
   } else {
    this.IsDesignExists = false;
     this.IsNewDesignChecked = false;
   }    
 }


 focusOutDesignTxt(event:any,template: TemplateRef<any>){
  let newDesignationValue = event.target.value;
  if(newDesignationValue != "" && newDesignationValue != undefined){
    this.IsDesignExists = false;
    let object ={
      typeid:2,
      labeltitle:newDesignationValue
    };    
    this.commonfieldsService.checkMasterTypeExistance(object).subscribe(data=>{
      if(data.Code == "SUC-200"){
          let isDesignExisting = JSON.parse(data.Data);
          if(parseInt(isDesignExisting) > 0){
            this.IsDesignExists = true;
            this.responseInfo = `<div class="alert alert-danger mb-0">     
              <h6>Designation already exist. Please select from list..!</h6> 
            </div> `; 
            this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
            setTimeout(() => {
              this.modalResponseRef.hide();
            }, 2000);
          }
      }
    });
  }
}

  getDesignationCategoryList(){
    this.commonfieldsService.getCommonFields(4,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.designationCategoryList = JSON.parse(data.Data);
      }
    });
  }
  
  StageChangedHandler(clickedText) {
    clickedText = clickedText.toLowerCase();   
  }

  selectAccount(){
    this.accountintiderror = false;  
    this.accountnameerror = false; 
     console.log(this.selAccountInfo);
   }

   onAccountKeyup(txtAccountName:string){
     if(txtAccountName != ""){
       this.accountnameerror = false;
     }
   }

  accountChangeHandler(selectedObject:any){
    let accountsArray =[];
       if(selectedObject.term.length >= 3){
       let accountsSub = this.accounts.getBasicAccounts(selectedObject.term).subscribe(data => {
        if(data.Status == "Success"){
         let accounts = JSON.parse(data.Data);
         accounts.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.Id,
              name:element.AccountName
            };       
         
            accountsArray.push(singleItem);
        });
        this.accountsList = accountsArray;         
        }
       });
       this.subscriptions.push(accountsSub);
        }
    // if(selectedValue != ""){
    //   this.accountintiderror = false;
    // }
    }
    selectDesignation(){
      this.accountintiderror = false;  
      this.accountnameerror = false; 
      console.log(this.selDesigInfo);   
      
    } 

    
    addContact(template: TemplateRef<any>) {  
      this.submitted = true;
      if (
        this.IsChecked &&
        (this.contactForm.value.accountname === "" ||
          this.contactForm.value.accountname === null)
      ) {
        this.accountnameerror = true;
        this.accountintiderror = false;        
        this.contactForm.patchValue({ accountintid: null });
        return;
        // this.contactForm.value.accountname = null;
      } else if (
        !this.IsChecked &&
        (this.selAccountInfo == undefined || this.selAccountInfo.id == undefined ||
        this.selAccountInfo.id == null)
      ) {
        this.accountnameerror = false;
        this.accountintiderror = true;
        return;
      }

      if (this.contactForm.invalid) {
        return;
      }


      if(this.IsNewDesignChecked && (this.contactForm.value.designation !== null || this.contactForm.value.designation !== undefined)){
        this.designationArr = [];
          let newDesignationValue = this.contactForm.value.designation;
          let object ={
            typeid:2,
            labeltitle:newDesignationValue
          };    
          this.commonfieldsService.checkMasterTypeExistance(object).subscribe(data=>{
            if(data.Code == "SUC-200"){
              let isDesignExisting = JSON.parse(data.Data);
              if(parseInt(isDesignExisting) > 0){
                this.IsDesignExists = true;
              }else {
                // !this.IsAccountExists &&
                if(this.IsChecked && 
                  (this.contactForm.value.accountname !== null || this.contactForm.value.accountname !== undefined)){
                    this.loading = true;
                    let newAccountValue = this.contactForm.value.accountname;
                    this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
                      if(data.Code == "SUC-200"){
                        let accountsList = JSON.parse(data.Data);
                        if(accountsList.length > 0){
                          this.IsAccountExists = true;
                          this.loading = false;
                        }else {
                          this.aftervalidaton(this.contactForm, template);   
                        }
                      }
                    });
                }else {
                  this.aftervalidaton(this.contactForm, template);
                } 
              }
            }
          });
      }else {
        // !this.IsAccountExists &&
        if(this.IsChecked && 
          (this.contactForm.value.accountname !== null || this.contactForm.value.accountname !== undefined)){
            this.loading = true;
            let newAccountValue = this.contactForm.value.accountname;
            this.accounts.IsAccountExists(newAccountValue).subscribe(data=>{
              if(data.Code == "SUC-200"){
                let accountsList = JSON.parse(data.Data);
                if(accountsList.length > 0){
                  this.IsAccountExists = true;
                  this.loading = false;
                }else {
                  this.aftervalidaton(this.contactForm, template);   
                }
              }
            });
        }else {
          this.aftervalidaton(this.contactForm, template);
        }   
      }
            
      if(this.IsDesignExists){
        this.responseInfo = `<div class="alert alert-danger mb-0">     
        <h6>Designation already exist. Please select from list..!</h6> 
      </div> `; 
      this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 2000);
      return;
      }

      if(this.IsContactExists){
        this.responseInfo = `<div class="alert alert-danger mb-0">     
                <h6>Email already exist. Please try with another..!</h6> 
              </div> `;
            this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
          setTimeout(() => {
            this.modalResponseRef.hide();
          }, 2000);
          return;
      }

      if(this.IsAccountExists){
        this.responseInfo = `<div class="alert alert-danger mb-0">     
        <h6>Account already exist. Please select from list..!</h6> 
      </div> `; 
      this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
      setTimeout(() => {
        this.modalResponseRef.hide();
      }, 2000);
      return;
      }

    
      if(this.designationArr[0] && this.designationArr[0].designation==1){
           
      }
    }


    aftervalidaton(contactForm,template){
      let emailValue = contactForm.value.email;
      this.loading = true;
      if(emailValue){
          this.contactsService.isContactExists(emailValue,0).subscribe(data=>{
            if(data.Code == "SUC-200"){
                let isContactExisting = JSON.parse(data.Data);
                if(parseInt(isContactExisting) > 0){
                  this.IsContactExists = true;  
                  this.loading = false;
                }else {
                  this.proceedtoCreateContact(contactForm, template);
              }
            }
          });
      }else {
        this.proceedtoCreateContact(contactForm, template);
      }
    }

    proceedtoCreateContact(contactForm, template: TemplateRef<any>){
      let designationId = "";
      if(this.selDesigInfo == undefined || this.selDesigInfo == null){
        designationId = "";
      }else{
        designationId = this.selDesigInfo.id;
      }

      let accountIntId = 0;
      if(this.selAccountInfo == undefined || this.selAccountInfo == null){
        accountIntId = 0;
      }else{
        accountIntId = this.selAccountInfo.id;
      }

      let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
      contactForm.patchValue({ TeamId: TeamInfo.TeamId });
      contactForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
      contactForm.patchValue({ accountintid: accountIntId });
      contactForm.patchValue({ designationid: designationId });
      if (
        contactForm.value.accountintid != null ||
        contactForm.value.accountname != null
      ) {
        this.contactsService
          .AddContact(this.contactForm.value)
          .subscribe(data => {
            this.loading = false;
            if (data.Status === "Success") {          
              this.responseInfo = `<div class="alert alert-success mb-0">     
              <h6>Contact added successfully..!</h6> 
            </div> `;                
            this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
            this.onReset();
            setTimeout(() => {
              this.modalResponseRef.hide();
              this.router.navigate(['/contacts']);
            }, 1000);
            }else{
              this.responseInfo = `<div class="alert alert-danger mb-0">     
              <h6>Failed to add contact..!</h6> 
            </div> `;      
            this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
            this.onReset();
            setTimeout(() => {
              this.modalResponseRef.hide();             
            }, 2000);       
            }
          });      
      }
    }

      

    //#region contact checking
   focusOutEmailTxt(event:any,template: TemplateRef<any>){
    let emailValue = event.target.value;
    if(emailValue != "" && emailValue != undefined){
      this.IsContactExists = false;
      this.contactsService.isContactExists(emailValue,0).subscribe(data=>{
        if(data.Code == "SUC-200"){
            let isContactExisting = JSON.parse(data.Data);
            if(parseInt(isContactExisting) > 0){ 
              this.IsContactExists = true;            
              this.responseInfo = `<div class="alert alert-danger mb-0">     
                <h6>Email already exist. Please try with another..!</h6> 
              </div> `; 
              this.modalResponseRef = this.modalBsService.show(template, { class: "modal-m" });
              setTimeout(() => {
                this.modalResponseRef.hide();
              }, 3000);
            }
        }
      });
    }
  }
    
  getCampaignTypeList(){
    this.commonfieldsService.getCommonFields(3,"").subscribe(data=>{
      if (data.Status === "Success") {
        this.campaigntypeList = JSON.parse(data.Data);
      }
    });
  }

  compTypeChangeHandler(selectedValue: string) {
    this.selectedCampaign = selectedValue;    
  }

    onReset() {
      this.submitted = false;
      this.accountnameerror = false;
      this.accountintiderror = false;
      this.IsContactExists = false;
      this.IsDesignExists = false;
      this.IsAccountExists = false;
      this.contactForm.reset();
    }
    selectStageChangeHandler(selectedValue: string) {
      this.selectedStage = selectedValue;
    }

    selectAccountChangeHandler(selectedValue: string) {
      this.selectedAccountId = selectedValue;
    }
  
    selectOwnerChangeHandler(selectedValue: string) {
      this.selectedOwnerId = selectedValue;
    }
    
   sctDesCatChangeHandler(selectedValue: string) {  
    this.selectedDesignationCat = selectedValue;
   }
   
  
  previousPage(){
    this.location.back();
  } 

 //#endregion
}