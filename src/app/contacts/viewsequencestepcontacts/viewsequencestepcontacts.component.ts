import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContactApiService } from 'src/services/contacts.services';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { GridOptions } from 'ag-grid-community';
import { Location } from '@angular/common';

@Component({
  selector: 'app-viewsequencestepcontacts',
  templateUrl: './viewsequencestepcontacts.component.html',
  styleUrls: ['./viewsequencestepcontacts.component.css']
})
export class ViewsequencestepcontactsComponent implements OnInit,OnDestroy {

  subscriptions: Subscription [] = [];
  public seqId:number;
  public stepId:number;
  public emailType:string;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowGroupPanelShow;
  public pivotPanelShow;
  public sideBar;
  public rowData: any;
  public contactsData: any;
  paginationPageSize: number;
  cacheBlockSize: number;
  constructor(
    private coontactService:ContactApiService,
    private route: ActivatedRoute,
    private location: Location
  ) { 

    this.sideBar = {
      toolPanels: [
        {
          id: "columns",
          labelDefault: "Columns",
          labelKey: "columns",
          iconKey: "columns",
          toolPanel: "agColumnsToolPanel"
        },
        {
          id: "filters",
          labelDefault: "Filters",
          labelKey: "filters",
          iconKey: "filter",
          toolPanel: "agFiltersToolPanel"
        }
      ],
      defaultToolPanel: ""
    };
    this.columnDefs = [    
     
      {
        headerName: "Contact Name",
        field: "contactname",   
        cellRenderer: function(params) {
          return (
            '<a href="contacts/viewcontact/' +
            params.data.contactid +
            '""target="_blank">' +
            params.value +
            "</a>"
          );
        },    
      },
      {
        headerName: "Account Name",
        field: "AccountName"
      },    
      {
        headerName: "Industry",
        field: "Industry"
      },  
      {
        headerName:"Scheduled Date",
        field:"ImplementationDate",
        sortable:true
      },  
      {
        headerName: "Email Address",
        field: "email"
      },
      {
        headerName: "Designation",
        field: "designation"
      },
       {
        headerName: "Mobile Number",
        field: "mobilenumber",
      }, 
    ];
    this.autoGroupColumnDef = {
      headerName: "Group",
      //  width: 200,
      
      field: "customername",
      valueGetter: function(params) {
        if (params.node.group) {
          return params.node.key;
        } else {
          return params.data[params.colDef.field];
        }
      },
      headerCheckboxSelection: true,
      cellRenderer: "agGroupCellRenderer",
      cellRendererParams: { checkbox: true }
    };
    this.defaultColDef = {
      editable: false,
      enableRowGroup: true,
      enablePivot: false,
      enableValue: true,
      sortable: true,
      resizable: true,
      filter: true

    };
 
    this.rowSelection = "multiple";
    this.rowGroupPanelShow = "false";
    this.pivotPanelShow = "always";
    this.cacheBlockSize = 10;
    this.paginationPageSize = 10; 
  }
  
  gridOptions: GridOptions = {
    rowHeight: 52,
    headerHeight: 44
  };
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;  
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.seqId = params["seqid"];
      this.stepId = params["stepid"];
      this.emailType = params["type"];           
    });
    this.getStepContacts(this.seqId,this.stepId,this.emailType);
  }

  onFilterTextBoxChanged(event: any) {
    let searchText = event.currentTarget.value;
    if (searchText != "") {
      this.gridOptions.api.setQuickFilter(searchText);
    } else {
      this.gridOptions.api.setQuickFilter(null);      
    }
  }

  previousPage(){
    this.location.back();
  }

  getStepContacts(seqId:number,stepId:number,type:string){
    let contactsSub = this.coontactService.getSequenceStepContacts(seqId,stepId,type).subscribe(data=>{
       if(data.Code == "SUC-200"){
         let contactsList = JSON.parse(data.Data);
         this.rowData = contactsList;
       }
    });
    this.subscriptions.push(contactsSub);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
