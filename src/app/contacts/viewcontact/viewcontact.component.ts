import { Component, OnInit, ViewChild, ElementRef, TemplateRef, OnDestroy, Injectable } from '@angular/core';
import { NotesApiService } from 'src/services/notes.services';
import { ContactApiService } from 'src/services/contacts.services';
import { SequenceApiService } from 'src/services/sequenceservice.services';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'src/services/DataService';
import { CallsApiService } from 'src/services/calls.services';
import { TaskApiService } from 'src/services/taskservice.services';
import { ContactsComponent } from '../contacts.component';
import { SequenceComponent } from 'src/app/sequence/sequence.component';
import { UserService } from 'src/services/userservice.services';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import Froalaeditor from 'froala-editor';
import { EmailApiService } from 'src/services/emails.services';
import { MeetingApiService } from 'src/services/meeting.services';
import { listenToTriggers } from 'ngx-bootstrap/utils/triggers';
import { templateService } from 'src/services/templates.services';
import 'froala-editor/js/plugins/link.min.js';
import 'froala-editor/js/plugins/image.min.js';
import 'froala-editor/js/plugins/file.min.js';
import { UtilitiesService } from 'src/services/utilities.services';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { OpportunityApiService } from 'src/services/opportunities.services';

@Component({
  providers: [ContactsComponent, SequenceComponent],
  selector: 'app-viewcontact',
  templateUrl: './viewcontact.component.html',
  styleUrls: ['./viewcontact.component.css']
})

export class ViewcontactComponent implements OnInit,OnDestroy {
  selectedUser: any;
  submitted = false;
  notesForm: FormGroup;
  callsForm: FormGroup;
  TaskForm: FormGroup;
  MeetingsForm: FormGroup;
  EmailForm: FormGroup;
  ManualEmailForm: FormGroup;
  notes: any;
  id: any;
  orderId: any;
  calls: any;
  tasks: any;
  Contacts: any;
  activityresponse: any;
 // activitycount: any;
  notescount: any;
  callscount: any;
  taskscount: any;
  sequences: any;
  selectedSequence: any;
  sequenceslist: any;
  selectedOption: any;
  sequencecount: any;
  activitycount:number;
  selectedTask: any;
  display = 'none';
  activeTab = 'activity';
  templateModal: BsModalRef;
  emptyTemplateModal: BsModalRef;
  ExistingTemplatesModal: BsModalRef;
  options;
  templateModalData: any;
  contactEmail: string;
  contactId: string;
  sequenceDetailedId: number;
  emailscount: any;
  meetingscount: any;
  emails: any;
  usersData: any;
  bsValue = new Date();
 // mytime: Date = new Date();
  meetingtime: Date = new Date();
  Hours: any;
  Mins: any;
  Days: any;
  meetingList: any;
  templates: any;
  nonFilteredTemplates: any;
  singleTemplateData: any;
  templateFormObject: any;
  mailResponseTemplate:BsModalRef;
  IsSequenceNotSelected:boolean= false;
  loading:boolean = false;
  NoInvitees:boolean = false;
  ActivityInfo:any;
  emailOptions:any;
  contactInfo:any;
  contactname:string;
  ErrorMessageInfo:any;
  IsLinkedToSequence:number;
  subscriptions: Subscription[] = [];
  monthDays = [];
  hourMins = [];
  dayHours = [];

  //#region meetings
  responseInfo:string = '';
  meetingId:number;
  //#endregion

  //#region opportunity
  opportunity:any;
  oppocount:number=0;
  //#endregion

 // IsSubscribedToEmail:any;
  get f() { return this.notesForm.controls; }
  get cf() { return this.callsForm.controls; }
  get mf() { return this.MeetingsForm.controls; }
  get ef() { return this.ManualEmailForm.controls; }
  get tef() { return this.EmailForm.controls; }
  @ViewChild('closeAddNotesModal', { static: false }) closeAddNotesModal: ElementRef;
  @ViewChild('closeAddCallsModal', { static: false }) closeAddCallsModal: ElementRef;
  @ViewChild('closeAddTaskModal', { static: false }) closeAddTaskModal: ElementRef;
  @ViewChild('closeAddSequenceModal', { static: false }) closeAddSequenceModal: ElementRef;
  @ViewChild('closeSendEmailModal', { static: false }) closeSendEmailModal: ElementRef;
  @ViewChild('closeMeetingsModal', { static: false }) closeMeetingsModal: ElementRef;

  clickedId = 0;
  constructor(private formBuilder: FormBuilder, private router: Router, private notesService: NotesApiService,
    private route: ActivatedRoute, private data: DataService, private callsService: CallsApiService,
    public utilities: UtilitiesService,  private location: Location,private spinner: NgxSpinnerService,
    private opportunityService:OpportunityApiService,
    private taskService: TaskApiService, private contacts: ContactsComponent, private modalService: BsModalService,
    private contactService: ContactApiService, private apiService: SequenceApiService, private emailService: EmailApiService,
    private userService: UserService, private meetingService: MeetingApiService, private templateApiService: templateService) { 
        this.monthDays = new Array(30).fill(1).map((x,i)=>i);
        this.dayHours = new Array(24).fill(1).map((x,i)=>i);
        this.hourMins = new Array(59).fill(1).map((x,i)=>i);
    }

  ngOnInit() {
    this.spinner.show();

    // setTimeout(() => {
    //   this.spinner.hide();
    // }, 2000);
    
    this.notescount = 0;
    this.callscount = 0;
    this.taskscount = 0;
    this.sequencecount = 0;
    this.emailscount = 0;
    this.meetingscount = 0;
    this.oppocount=0;
    this.notesForm = this.formBuilder.group({
      Description: ['',Validators.compose([Validators.required])],
      ObjectId: [],
      ObjectIntId: [],
      AccountId: [],
      AssignedTeam: [],
      AssignedTeamIntId: []
    });
    this.callsForm = this.formBuilder.group({
      Description: [],
      Title: ['',Validators.compose([Validators.required])],
      ObjectId: [],
      ObjectIntId: [],
      AccountId: [],
      AssignedTeam: [],
      AssignedTeamIntId: []
    });
    this.TaskForm = this.formBuilder.group({
      ObjectId: [],
      Priority: [],
      Title: [],
      AssignedTo: [],
      DueDate: [],
      DueTime: [],
      Description: [],
      AccountId: [],
      TeamId: [],
      AssignedTeamIntId: [],
      ObjectIntId: [],
      AssignedToIntId: []
    });

    this.MeetingsForm = this.formBuilder.group({
      Invitees:  ['',Validators.compose([Validators.required])],
      Title:  ['',Validators.compose([Validators.required])],
      Description: [],
      ObjectId: [],
      RemainderDate: [],
      ObjectIntId: [],
      AccountId: [],
      AssignedTeam: [],
      AssignedTeamIntId: [],
      Days: [],
      Hours: [],
      Mins: [],
      Date: []
    });

    this.EmailForm = this.formBuilder.group({
      To: [''],
      CC: [''],
      Subject: ['', Validators.required],
      HtmlBody: [],
      AssignedTeam: [],
      AssignedTeamIntId: [],
      SequenceDetailId:[]
    });

    this.ManualEmailForm = this.formBuilder.group({
      To: [],
      CC: [],
      Subject: ['', Validators.required],
      HtmlBody: [],
      AssignedTeam: [],
      AssignedTeamIntId: [],
      SequenceDetailId:[],
      ContactId:[],
      BCC:[]
    });
   
    this.route.params.subscribe(params => {
      
      this.id = params["id"];
      this.clickedId = this.id;      
     this.getSequences();
      this.getSingleContactInfo(this.id);
      this.getSequenceData();  
      
    });
   // this.getContacts();

    let userSubscription = this.userService.getUsers()
      .subscribe(data => {
        this.usersData = JSON.parse(data.Data);
      });
    this.subscriptions.push(userSubscription);
    Froalaeditor.DefineIcon('custom-field', { NAME: 'cog', SVG_KEY: 'cogs' });
    Froalaeditor.RegisterCommand('custom-field', {
      title: 'Personalize',
      type: 'dropdown',
      focus: false,
      undo: false,
      refreshAfterCallback: true,
      options: {
        '{{Contact: First Name}}': 'First Name',
        '{{Contact: Last Name}}': 'Last Name',
        '{{Contact: Designation}}': 'Designation',
        '{{Contact: PhoneNumber}}': 'PhoneNumber',
        "{{Account: AccountName}}": "AccountName",
      },
      callback: function (cmd, val) {
        this.html.insert(val);
      }
    });

    this.options = {
      placeholderText: 'Edit Your Content Here!',
      height: 200,
      toolbarBottom: true,
      inlineMode: false,
      toolbarButtons: ['bold', 'italic', 'underline','custom-field','strikeThrough','insertLink','insertImage','insertFile'],   
      imageUploadToS3: {
        bucket: 'outworksales',       
        region: 's3-ap-south-1',
        keyStart: 'images/',
        acl: 'public-read',
        accessKey:'AKIAZHEALBC223YK2MJ6',
        secretKey:'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'      
      },
      fileUploadToS3: {
        bucket: 'outworksales',       
        region: 's3-ap-south-1',
        keyStart: 'documents/',
        params: {
          acl: 'public-read', // ACL according to Amazon Documentation.
          accessKey:'AKIAZHEALBC223YK2MJ6',
          secretKey:'lNP36ZHX4x5jkG7xCbLdeYeUlWGKERPKuK59cprI'  
        }
      }
    }

    this.emailOptions = {
      placeholderText: 'Edit Your Content Here!',
      height: 200,
      toolbarBottom: true,
      inlineMode: false,
      toolbarButtons: ['bold', 'italic', 'underline','custom-field','strikeThrough','insertLink','insertImage','insertFile'],   
      imageUploadToS3: {
        bucket: 'outworksales',       
        region: 's3-ap-south-1',
        keyStart: 'images/',
        acl: 'public-read',
        accessKey:'AKIAZHEALBC223YK2MJ6',
        secretKey:'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'      
      },
      fileUploadToS3: {
        bucket: 'outworksales',       
        region: 's3-ap-south-1',
        keyStart: 'documents/',
        params: {
          acl: 'public-read', // ACL according to Amazon Documentation.
          accessKey:'AKIAZHEALBC223YK2MJ6',
          secretKey:'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'  
        }
      }
    }

    this.getActivities(this.id);
   // this.getContactData();
  } 

  ngOnDestroy(): void {
   this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  getSingleContactInfo(contactIntId:number){
    
  let singleContactSub =  this.contactService.getContactActivityCount(contactIntId).subscribe(data => {
      if (data.Status === 'Success') {
        this.activityresponse = JSON.parse(data.Data);
        this.contactEmail = this.activityresponse.ContactInfo.email;      
        this.contactId = this.activityresponse.ContactInfo.contactid;        
        this.contactname = this.activityresponse.ContactInfo.contactname;
       this.IsLinkedToSequence = this.activityresponse.ContactInfo.islinkedtosequence == true ? 1:0;
        // this.clickedContactInfo = {contactid:this.contactId,from:'contacts'};
        console.log("khaleel:"+this.activityresponse.ContactInfo.IsValidEmailAddress);
        for (var singleActivityResponse of this.activityresponse.ActivityCountInfo) {
          if (singleActivityResponse.ContactType == "notes") {
            this.notescount = singleActivityResponse.Count;
          }
          if (singleActivityResponse.ContactType == "calls") {
            this.callscount = singleActivityResponse.Count;
          }
          if (singleActivityResponse.ContactType == "tasks") {
            this.taskscount = singleActivityResponse.Count;
          }
          if (singleActivityResponse.ContactType == "emails") {
            this.emailscount = singleActivityResponse.Count;
          }
          if (singleActivityResponse.ContactType == "meetings") {
            this.meetingscount = singleActivityResponse.Count;
          }
          if (singleActivityResponse.ContactType == "opportunity") {
            this.oppocount = singleActivityResponse.Count;
          }

        }
      }
    });
   this.subscriptions.push(singleContactSub);
   this.spinner.hide();
  }

  

  getSequenceData(){
  let linkedContactSub =  this.apiService.getSequenceLinkedToContact(this.clickedId)
        .subscribe(data => {
          if (data.Status === 'Success') {
            this.sequences = JSON.parse(data.Data);
            this.sequencecount = this.sequences.length;
          }
        });
  this.subscriptions.push(linkedContactSub);
  }

  addNotes() {
    this.submitted = true;
    if (this.notesForm.invalid) {
      return;
    }
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.notesForm.patchValue({ ObjectId: this.activityresponse.ContactInfo.contactid });
    this.notesForm.patchValue({ ObjectIntId: this.id });
    this.notesForm.patchValue({ AccountId: this.activityresponse.ContactInfo.accountintid });
    this.notesForm.patchValue({ AssignedTeam: TeamInfo.TeamId });
    this.notesForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });

    this.notesService.addNotes(this.notesForm.value).subscribe(data => {
      if (data.Status === 'Success') {
        console.log('Completed');
        this.closeAddNotesModal.nativeElement.click();
        this.getNotes();
        this.getActivities(this.id);
        this.notesForm.reset();
      }
    });
  }

  addCalls() {
    this.submitted = true;

    if (this.callsForm.invalid) {
      return;
    }
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.callsForm.patchValue({ ObjectId: this.activityresponse.ContactInfo.contactid });
    this.callsForm.patchValue({ AccountId: this.activityresponse.ContactInfo.accountintid });
    this.callsForm.patchValue({ ObjectIntId: this.id });
    this.callsForm.patchValue({ AssignedTeam: TeamInfo.TeamId });
    this.callsForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    this.callsService.addCalls(this.callsForm.value).subscribe(data => {
      console.log(JSON.stringify(data));
      if (data.Status === 'Success') {
        this.closeAddCallsModal.nativeElement.click();
        this.getCalls();
        this.callsForm.reset();
        this.submitted = false;
      }
    });
  }
 
  onselectedhours(event) {
    this.Hours = event.target.value;
  }

  onselecteddays(event) {
    this.Days = event.target.value;
  }

  onselectedmins(event) {
    this.Mins = event.target.value;
  }

  inviteesClick(){
    this.NoInvitees = false;
  }

  //#region  Meetings
  markAsCompleted(meetingIntId:number,template: TemplateRef<any>){    
   this.meetingId = meetingIntId;
    this.responseInfo = `<div class="alert alert-success mb-0">     
    <h6>Do you want to complete this meeting..!</h6></div>
     `;   
  this.mailResponseTemplate = this.modalService.show(template, { class: 'modal-m', backdrop  : 'static' });

    
  }

  completeConfirmation(){
    let inputObj ={
      MeetingId:this.meetingId
    };
this.meetingService.markMeetingDone(inputObj).subscribe(data => {
      this.closeMeetingsModal.nativeElement.click();
      if (data.Status === 'Success') { 
           
        this.getMeetings();      
      }      
      this.mailResponseTemplate.hide();
    });
  }
  addMeeting() {
    this.submitted = true;
    let array = [];
   if(this.selectedUser != undefined && this.selectedUser.length > 0){
    array = JSON.parse("[" + this.selectedUser + "]");
   }else{
   this.NoInvitees = true;
   }
   
 // console.log("submitted "+this.submitted);
  //console.log("Invities error "+JSON.stringify(this.mf.Invitees.errors.required));


    if (this.MeetingsForm.invalid) {
      return;
    }
    
    let date = new Date(this.bsValue);
    let fullDate = date.setHours(this.meetingtime.getHours(), this.meetingtime.getMinutes());
    let timestamp = Math.floor(new Date(fullDate).getTime() / 1000.0);
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.MeetingsForm.patchValue({ Invitees: array });
    this.MeetingsForm.patchValue({ ObjectId: this.activityresponse.ContactInfo.contactid });
    this.MeetingsForm.patchValue({ AccountId: this.activityresponse.ContactInfo.accountintid });
    this.MeetingsForm.patchValue({ ObjectIntId: this.id });
    this.MeetingsForm.patchValue({ AssignedTeam: TeamInfo.TeamId });
    this.MeetingsForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    this.MeetingsForm.patchValue({ Date: timestamp });

    this.meetingService.addMeeting(this.MeetingsForm.value).subscribe(data => {
      this.closeMeetingsModal.nativeElement.click();
      if (data.Status === 'Success') {       
        this.getMeetings();
        this.MeetingsForm.reset();
      }      
    });
  }
  //#endregion
  

  getContacts() {
  this.contacts.getContacts().then(result => {
      this.Contacts = result;
    });   
  }



  getNotes() {
    this.notesService.getNotes(this.id)
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.notes = JSON.parse(data.Data);
        }
      });
  }

  getCalls() {
    this.callsService.getCalls(this.id).subscribe(data => {
      if (data.Status === 'Success') {
        this.calls = JSON.parse(data.Data);
      }
    });
  }

  getEmails() {
    this.emailService.getEmails(this.id).subscribe(data => {
      this.emails = data;
    })
  }

  gettasks(contactId:number) {
    this.taskService.getTaskByObjectId(contactId)
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.tasks = JSON.parse(data.Data);           
        }
      });
  }

  getOpportunityById(){
    let opportunityInfoSub = this.opportunityService.GetOpportunityByObjectIntId(this.id).subscribe(data => {
       if (data.Status === 'Success') {
         this.opportunity = JSON.parse(data.Data);
       }
     });
     this.subscriptions.push(opportunityInfoSub);
   }

  getMeetings() {
    this.meetingService.getMeetingsByObjectId(this.id).subscribe(data => {
      this.meetingList = data;
    });
  }

  public getSequences() {
  let sequernceSubscription =  this.apiService.getSequences().subscribe(data => {
    if(data.length > 0){
      this.sequenceslist = data.filter(a=>a.StepsCount > 0 && a.SequenceStatus != 'Inactive');
    }
     // this.closeAddSequenceModal.nativeElement.click();
    });
 this.subscriptions.push(sequernceSubscription);
  }
getActivities(contactIntId:number){
 let activitiesSub = this.contactService.contactActivity(contactIntId).subscribe(data=>{
    if(data.Code === "SUC-200"){
      let contactActivityInfo = data.Data;
      this.ActivityInfo = JSON.parse(contactActivityInfo);
      this.activitycount = this.ActivityInfo.length;
    }   
  });
  this.subscriptions.push(activitiesSub);
}


  onSelect(hero: any): void {
    this.selectedSequence = hero;
    this.router.navigate(['viewsequence/' + this.selectedSequence.SequenceIdInt]);
  }

  onSelectTask(task: any): void {
    this.selectedTask = task;
    this.taskService.updateTaskAsCompleted(this.selectedTask.SequenceDetailId, this.selectedTask.TaskIntId).subscribe(data => {
      if (data.Status === 'Success') {
        this.gettasks(this.id);
      }
    })
  }
  sequenceSelection(event:any){
   let value = event.target.value;
   if(value != "" || value != undefined){
    this.IsSequenceNotSelected = false;
   }
  }
  assignsequence() {
    let sequenceId = (<HTMLInputElement>document.getElementById("sequenceList")).value;   
    if(sequenceId === "" || sequenceId === undefined){
      this.IsSequenceNotSelected = true;
    }else{
      this.IsSequenceNotSelected = false;
      // this.selectedOption
    this.contactService.assignSequence(this.activityresponse.ContactInfo.contactid, sequenceId).subscribe(data => {
      // if (data.Status === 'Success') {
        console.log("added successfully");
        this.closeAddSequenceModal.nativeElement.click();
        this.getSequenceData();   

    });
  }
  }

  sendEmailBtnClick(template: TemplateRef<any>, sequenceId, source) {
    //  let sequenceIdCons = 105;
    
    if (sequenceId != "" && source == 'task') {
      this.sequenceDetailedId = sequenceId;
      this.apiService.getSequenceDetailsToContact(sequenceId).subscribe(data => {
        if (data.Status === 'Success') {
          let templateData = JSON.parse(data.Data);
          this.templateModalData = {
            toemail: this.contactEmail,
            subject: templateData.Subject,
            body: templateData.HtmlTemplate
          };
          this.templateModal = this.modalService.show(template, { class: 'modal-lg' });
        }
      });
    } else {
      this.emptyTemplateModal = this.modalService.show(template, { class: 'modal-lg' });
    }

  }

  sendEmail() {
   
   // this.loading = true;
   // this.EmailForm.value.to = this.contactEmail;
    this.EmailForm.value.To = this.templateModalData.toemail;
    let ccList = this.EmailForm.value.CC;
    let ccArray = [];
    if (ccList != "") {
      ccArray.push(ccList);
    }
    this.EmailForm.value.CC = ccArray;   
    
    if(this.EmailForm.value.Subject === null || this.EmailForm.value.Subject === ""){
      this.EmailForm.patchValue({ Subject: this.templateModalData.subject });
    }
    if(this.EmailForm.value.HtmlBody === null || this.EmailForm.value.HtmlBody === ""){
      this.EmailForm.patchValue({ HtmlBody: this.templateModalData.body });
    }
 //  this.EmailForm.value.Subject = this.EmailForm.value.Subject == "" ? this.templateModalData.subject : this.EmailForm.value.Subject;
 //  this.EmailForm.value.HtmlBody = this.EmailForm.value.HtmlBody == "" ? this.templateModalData.body : this.EmailForm.value.HtmlBody;
  
    // if (this.EmailForm.invalid) {
    //   return;
    // }
    this.submitted = true;    
    if (this.EmailForm.invalid) {
      return;
    }
 
    this.EmailForm.value.contactid = this.contactId;
    this.EmailForm.value.SequenceDetailId = this.sequenceDetailedId;
    this.EmailForm.value.BCC = [];
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.EmailForm.value.AssignedTeam = TeamInfo.TeamId;
    this.EmailForm.value.AssignedTeamIntId = TeamInfo.Id; 
    
    this.apiService.sendEmailToContact(this.EmailForm.value).subscribe(data => {     
      this.templateModal.hide();
      if (data.Status === 'Success') {        
        this.gettasks(this.id);
        this.getActivities(this.id);
      }
    });
  }

  sendManualEmail(template: TemplateRef<any>) {   
    this.responseInfo = "";
    this.submitted = true;    
    if (this.ManualEmailForm.invalid) {
      return;
    }
    this.loading = true;
    this.ManualEmailForm.value.To = this.contactEmail;
    let ccList = this.ManualEmailForm.value.CC;
    let ccArray = [];
    if (ccList != "") {
      ccArray.push(ccList);
    }
    this.ManualEmailForm.value.CC = ccArray;
    this.ManualEmailForm.value.ContactId = this.contactId;
    this.ManualEmailForm.value.BCC = [];
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.ManualEmailForm.value.AssignedTeam = TeamInfo.TeamId;
    this.ManualEmailForm.value.AssignedTeamIntId = TeamInfo.Id; 
    this.ManualEmailForm.value.SequenceDetailId = 0;

    this.apiService.sendManualEmailToContact(this.ManualEmailForm.value).subscribe(data => {
      this.loading = false;
      this.closeSendEmailModal.nativeElement.click();
      if (data.Code === 'SUC-200') {      
        this.getEmails();
        this.getActivities(this.id);
      }else if(data.Code === "SUC-307"){
        this.ErrorMessageInfo = "This contact is unsubscribed mails from Outreach";
        this.mailResponseTemplate = this.modalService.show(template, { class: 'modal-m', backdrop  : 'static' });

      }else if(data.Code === "SUC-308"){
        this.ErrorMessageInfo = "Mail delivery failed due to mail configuration issue, Please contact support team.";
        this.mailResponseTemplate = this.modalService.show(template, { class: 'modal-m', backdrop  : 'static' });

      }else if(data.Code === "E-404"){
        this.ErrorMessageInfo = "Email address not found";
        this.mailResponseTemplate = this.modalService.show(template, { class: 'modal-m', backdrop  : 'static' });
        this.getSingleContactInfo(this.id);
      }
      this.onReset();
     
    });
  }

  OpenExistingTemplates(template: TemplateRef<any>) {
    this.getTemplates();
    this.ExistingTemplatesModal = this.modalService.show(template, { class: 'modal-lg' });
  }

  onReset() {
    this.submitted = false;
    this.ManualEmailForm.reset();   
  }

  SearchTemplateData(event: any) {
    let searchTerm = event.target.value;
    if (searchTerm.length >= 2) {
      this.templates = this.nonFilteredTemplates.filter(a => {
        const term = searchTerm.toLowerCase();
        return a.TemplateName.toLowerCase().includes(term)
          || a.Subject.toLowerCase().includes(term)
          || a.ElapsedDuration.toLowerCase().includes(term);
      });
    } else {
      this.templates = this.nonFilteredTemplates;
    }
  }

  clearSearch(event: any) {
    if (event.type === "mouseup") {
      this.templates = this.nonFilteredTemplates;
    }
  }

  getTemplates(): any {
    this.templateApiService.getTemplates()
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.templates = JSON.parse(data.Data);
          this.nonFilteredTemplates = this.templates;
        } else {
          this.templates = undefined;
        }
      });
  }

  SelectTemplate(template: TemplateRef<any>, templateId: string) {   
    
    let templatesList = this.nonFilteredTemplates;
    let singleTemplateData = templatesList.filter(a => a.TemplateId === templateId);
    let formFields = { Subject: singleTemplateData[0].Subject, HtmlBody: singleTemplateData[0].HtmlTemplateBody};

    this.ManualEmailForm.patchValue(formFields);
    this.ExistingTemplatesModal.hide();
  }

  getContactData(){
   let contactDataSub = this.contactService.getContactInfo(this.clickedId).subscribe(data=>{
      if (data.Code === 'SUC-200') {
    this.contactInfo = JSON.parse(data.Data);  
      }
    });
    this.subscriptions.push(contactDataSub);
  }
  
  previousPage(){
    this.location.back();
  }


  resetForm() {
    this.submitted = false;
    this.callsForm.reset();
    this.MeetingsForm.reset();
    this.notesForm.reset();
  }
}
