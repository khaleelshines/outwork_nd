import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ContactApiService } from 'src/services/contacts.services';
import { CommonFieldsService } from 'src/services/commonfields.services';
import { teammanagementservice } from 'src/services/teamsmanagement.services';

@Component({
  selector: 'app-accounts-actions',
  templateUrl: './accounts-actions.component.html',
  styleUrls: ['./accounts-actions.component.css']
})
export class AccountsActionsComponent implements OnInit {
  
  data: any;
  params: any;   
  accountIntId:number;
  subscriptions:Subscription [] = [];


  constructor(
    private contactsService: ContactApiService,
    private teamService: teammanagementservice,
    private commonfieldsService:CommonFieldsService,
    private formBuilder: FormBuilder,
  ) { }
  agInit(params) {
    this.params = params;
    this.data = params.value;
    if(params.data != undefined){
    this.accountIntId = params.data.Id;
    }
  }
  ngOnInit() { 

  }
  sendEmail() {
    let rowData = this.params;
    let i = rowData.rowIndex;
   // alert("Email request clicked..!");
  }

  

 

 
}
