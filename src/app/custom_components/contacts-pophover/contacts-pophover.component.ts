import { Component } from '@angular/core';
import { ITooltipAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-contacts-pophover',
  template:  `
  <div class="custom-tooltip" [style.background-color]="data.color">
      <p><span>{{data.contactname}}</span></p>
      <p><span>Email: </span>{{data.email}}</p>
      <p><span>Designation: </span>{{data.designation}}</p>
      <p><span>LinkedIn Profile: </span><br/>{{data.personallinkedinurl}}</p>
  </div>`,
  styles: [
    `
        :host {
            position: absolute;
            width: 210px;
            height: 110px;
            border: 1px solid cornflowerblue;
            overflow: hidden;
            pointer-events: none;
            transition: opacity 1s;
        }

        :host.ag-tooltip-hiding {
            opacity: 0;
        }

        .custom-tooltip p {
            margin: 5px;
            white-space: nowrap;
        }

        .custom-tooltip p:first-of-type {
            font-weight: bold;
        }
    `
]
})
export class ContactsPophoverComponent implements ITooltipAngularComp  {

  private params: any;
  public data: any;

  agInit(params): void {
      this.params = params;


      this.data = params.api.getDisplayedRowAtIndex(params.rowIndex).data;
      this.data.color = this.params.color || 'white';
  }


}
