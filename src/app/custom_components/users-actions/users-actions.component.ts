import { Component, OnInit } from '@angular/core';
import { UsermanagementComponent } from 'src/app/usermanagement/usermanagement.component';
import { UserService } from 'src/services/userservice.services';


@Component({
  selector: 'app-users-actions',
  templateUrl: './users-actions.component.html',
  styleUrls: ['./users-actions.component.css']
})
export class UsersActionsComponent implements OnInit {
  data: any;
  params: any;
  disableIcon:any;
  usersData:any;
  isDeleted:any;
  disableActionField:any;


  constructor( private userService: UserService) { }
  agInit(params) {
    this.params = params;
    this.data =  params.value;
    }
  ngOnInit() {
     
    if(this.params.data.StatusFlag){
      this.disableIcon=true;
    }
    if(this.params.data.Deleted){
      this.isDeleted=true;
      this.disableActionField=true;

    }
  }

  deleteUser(){
    let rowData = this.params;
    let i = rowData.rowIndex;
    let UserData: any = {
      UserId : this.params.data.UserId,
      StatusType:"DELETED"
     } ;
     this.updateUserActions(UserData); 
     this.isDeleted=true;
    //alert("delete user request clicked..!");
  }

   myClickHandler() {
    if (this.params.data.StatusFlag) {
     this.enableUser();
    }
    else
    this.disableUser();
}

  enableUser(){
    let rowData = this.params;
    let i = rowData.rowIndex;
    let UserData: any = {
     UserId : this.params.data.UserId,
     StatusType:"ENABLED"
    } ;
    this.updateUserActions(UserData); 
    this.disableIcon=false;
    //alert("enable user request clicked..!");
  }

  disableUser(){
    let rowData = this.params;
    let i = rowData.rowIndex;
    let UserData: any = {
      UserId : this.params.data.UserId,
      StatusType:"DISABLED"
     } ;
     this.updateUserActions(UserData); 
     this.disableIcon=true;
    //alert("disable user request clicked..!");
  }

  updateUserActions(UserData:any){
    this.userService.userActions(UserData).subscribe(data => {
      if (data.Status === 'Success') {
       console.log(data.Data)
      } else {
        console.log(data.Message);
      }
    });
  }

}
