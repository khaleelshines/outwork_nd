import { Component, OnInit, ElementRef, ViewChild, TemplateRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ContactApiService } from 'src/services/contacts.services';
import { ContactsComponent } from 'src/app/contacts/contacts.component';
import { NotesApiService } from 'src/services/notes.services';
import { MeetingApiService } from 'src/services/meeting.services';
import { TaskApiService } from 'src/services/taskservice.services';
import { CountryService1 } from 'src/app/team/datatables/country1.service';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { teammanagementservice } from 'src/services/teamsmanagement.services';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { SequenceComponent } from 'src/app/sequence/sequence.component';
import { ActivatedRoute } from '@angular/router';
import { OpportunityApiService } from 'src/services/opportunities.services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { OpportunityService1 } from '../opportunity.service';
import { UtilitiesService } from 'src/services/utilities.services';

interface Country {
  TaskId: string;
  TaskTitle: string;
  TaskDescription: string;
  DueDate: number;
  CompletedDate: number;
  TaskType: string;
  AssignedTo: string;
  AssigneeName: string;
  ShortAssigneeName: string;
  Priority: string;
  FirstName: string;
  Designation: string;
  TaskStatus: string;
  SequenceDetailId: number;
  TaskIntId: number;
  ElapsedDuration: string;
  AccountName: string;
  ObjectId: string;
  ObjectIntId: number;
  ContactEmail: string;  
}
interface priority {
  priority: string;
  prioritycount: number;
}

interface Opportunity { 
  OpportunityId:string;
  CompanyName: string;
  ContactName: string;
  ContactDesignation:string;
  AccountName:string;
  ShortContactName:string;
  Title:string;
  Stage:string;
  Status:string;
  TargetDate:string;
  Value:number;
  CreatedDate:string;
}

interface stage{
  stage:string;
  stagecount:number;
}

@Component({
  selector: 'app-viewopportunity',
  templateUrl: './viewopportunity.component.html',
  styleUrls: ['./viewopportunity.component.css'],
  providers: [ContactsComponent, SequenceComponent]
})
export class ViewopportunityComponent implements OnInit,OnDestroy {
  priorityCollection$: Observable<priority[]>;
  countries$: Observable<Country[]> = new BehaviorSubject<Country[]>([]);
  total$: Observable<number>;
  stageCollection$: Observable<stage[]>;
  opportunities$: Observable<Opportunity[]> = new BehaviorSubject<Opportunity[]>([]);
  bsValue = new Date();
  submitted=false;
  notesForm:any;
  TaskForm:any;
  MeetingsForm:any;
  opportunityForm:any;
  editopportunityForm:any;
  Contacts:any;
  notes:any;
  id:any;
  activityresponse: any;
  selectedUser:any;
  NoInvitees:boolean = false;
  mytime: Date = new Date();
  Hours: any;
  Mins: any;
  Days: any;
  meetingList: any;
  usersData:any;
  loggedInUserRole:any;
  tasks:any;
  IsMember: boolean;
  userInfo:any;
  Users:any;
  notescount: any;
  meetingscount: any;
  taskscount: any;
  opportunity:any;
  editOpportunityTemplateModal:any;
  UserIntId:any;
  editModalRef: BsModalRef;
  activeTab:string = "notes";
  subscriptions:Subscription [] = [];
  contactIntId:number;
  
  get mf() { return this.MeetingsForm.controls; }

  @ViewChild('closeAddNotesModal', { static: false }) closeAddNotesModal: ElementRef;
  @ViewChild('closeAddTaskModal', { static: false }) closeAddTaskModal: ElementRef;
  @ViewChild('closeAddOpportunityModal', { static: false }) closeAddOpportunityModal: ElementRef;
  @ViewChild('closeAddMeetingModal', { static: false }) closeAddMeetingModal: ElementRef; 

  constructor(private formBuilder: FormBuilder,private contactService: ContactApiService, 
    private contacts: ContactsComponent, private notesService: NotesApiService,
    private meetingService: MeetingApiService,private taskService: TaskApiService,
    public tservice: CountryService1,private teamService: teammanagementservice,public auth: AuthService,
    private route: ActivatedRoute,private opportunityService: OpportunityApiService,
    public utilities: UtilitiesService,
    private modalService: BsModalService,public oppService: OpportunityService1) { 

      this.loggedInUserRole = this.auth.getRole();
  }
 

  ngOnInit() {

    this.notescount = 0;
    this.taskscount = 0;
    this.meetingscount = 0;

    this.opportunityForm = this.formBuilder.group({
      ContactIntId:[],
      Title: [],
      Value: [],
      Email: [],
      PhoneNo: [],
      Stage:[],
      AssignedToId:[],
      Status:[],
      TargetDate:[],
      TeamIntId:[],
      TeamId:[],
      AssignedToIntId: []    
    });

    this.notesForm = this.formBuilder.group({
      Description: [],
      ObjectId: [],
      ObjectIntId: [],
      AccountId: [],
      AssignedTeam: [],
      AssignedTeamIntId: []
    });

    this.TaskForm = this.formBuilder.group({
      ObjectId: [],
      Priority: [],
      Title: [],
      AssignedTo: [],
      DueDate: [],
      DueTime: [],
      Description: [],
      AccountId: [],
      TeamId: [],
      AssignedTeamIntId: [],
      ObjectIntId: [],
      AssignedToIntId: []
    });

    this.MeetingsForm = this.formBuilder.group({
      Invitees:  ['',Validators.compose([Validators.required])],
      Title:  ['',Validators.compose([Validators.required])],
      Description: [],
      ObjectId: [],
      RemainderDate: [],
      ObjectIntId: [],
      AccountId: [],
      AssignedTeam: [],
      AssignedTeamIntId: [],
      Days: [],
      Hours: [],
      Mins: [],
      Date: []
    });

    this.route.params.subscribe(params => {
      this.id = params["id"];
     let oppoActivitySub = this.opportunityService.GetOpportunityActivityCount(this.id).subscribe(data => {
        if (data.Status === 'Success') {
          this.activityresponse = JSON.parse(data.Data);
           this.contactIntId = this.activityresponse.ContactInfo.id;
          for (var singleActivityResponse of this.activityresponse.ActivityCountInfo) {
            if (singleActivityResponse.OpportunityType == "notes") {
              this.notescount = singleActivityResponse.Count;
            }
            if (singleActivityResponse.OpportunityType == "tasks") {
              this.taskscount = singleActivityResponse.Count;
            }
            if (singleActivityResponse.OpportunityType == "meetings") {
              this.meetingscount = singleActivityResponse.Count;
            }

          }
        }
      });
      this.subscriptions.push(oppoActivitySub);
    })

    this.getContacts();
    this.getNotes();
    // if(this.loggedInUserRole === "ta"){
    //   this.teamService.getTeamMembersWithoutTeamId()
    //   .subscribe(data => {
    //     this.usersData = data;
    //   });
    
    //  }
    //  else 
     if(this.loggedInUserRole === "member"){     
      this.IsMember = true;
      let userDetails = localStorage.getItem('userInfo');
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        Name : userDetails1.Name, 
        UserId:userDetails1.UserId,
        UserIntId:userDetails1.IntUserId
      };
    }
    else{
     let teamMembersSub = this.teamService.getTeamMembersWithoutTeamId()
      .subscribe(data => {
        this.usersData = data;
      });
      this.subscriptions.push(teamMembersSub);
    }

  let teamUsersSub =  this.teamService.getTeamMembersWithoutTeamId()
    .subscribe(data => {
      this.Users = data;
    });
   this.subscriptions.push(teamUsersSub);
    this.getOpportunityById();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  editButtonClick(template: TemplateRef<any>,opportunity:any){
    this.editModalRef=this.modalService.show(template);
  }

  getContacts() {
    this.contacts.getContacts().then(result => {
      this.Contacts = result;
    });
  }

  getNotes() {
    this.notesService.getNotes(this.id)
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.notes = JSON.parse(data.Data);
        }
      });
  }

  
  gettasks() {
    this.taskService.getTaskByObjectId(this.contactIntId)
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.tasks = JSON.parse(data.Data);
          //let tasks1 = JSON.parse(data.Data);
          //this.tasks = tasks1.ListOfTasks;
        }
      });
  }

  getMeetings() {
    this.meetingService.getMeetingsByObjectId(this.id).subscribe(data => {
      if (data) {
      this.meetingList = data;
      }
    });
  }
 getOpportunityById(){
 let opportunityInfoSub = this.opportunityService.GetOpportunityByOpportunityId(this.id).subscribe(data => {
    if (data.Status === 'Success') {
      this.opportunity = JSON.parse(data.Data);
    }
  });
  this.subscriptions.push(opportunityInfoSub);
}

  addNotes() {
    this.submitted = true;
    if (this.notesForm.invalid) {
      return;
    }
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.notesForm.patchValue({ ObjectId: this.activityresponse.OpportunityInfo.OpportunityId });
    this.notesForm.patchValue({ ObjectIntId: this.id });
    this.notesForm.patchValue({ AccountId: this.activityresponse.ContactInfo.id });
    this.notesForm.patchValue({ AssignedTeam: TeamInfo.TeamId });
    this.notesForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });

    this.notesService.addNotes(this.notesForm.value).subscribe(data => {
      if (data.Status === 'Success') {
        console.log('Completed');
        this.closeAddNotesModal.nativeElement.click();
        this.getNotes();
      }
    });
  }
  
  inviteesClick(){
    this.NoInvitees = false;
  }

  addMeeting() {
    
    this.submitted = true;
    let array = [];
   if(this.selectedUser != undefined && this.selectedUser.length > 0){
    array = JSON.parse("[" + this.selectedUser + "]");
   }else{
   this.NoInvitees = true;
   }   
    
    //var array = JSON.parse("[" + this.selectedUser + "]");

    if (this.MeetingsForm.invalid) {
      return;
    }
    let date = new Date(this.bsValue);
    let fullDate = date.setHours(this.mytime.getHours(), this.mytime.getMinutes());
    let timestamp = Math.floor(new Date(fullDate).getTime() / 1000.0);
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.MeetingsForm.patchValue({ Invitees: array });
    this.MeetingsForm.patchValue({ ObjectId: this.activityresponse.OpportunityInfo.OpportunityId});
    this.MeetingsForm.patchValue({ AccountId: this.activityresponse.ContactInfo.id });
    this.MeetingsForm.patchValue({ ObjectIntId: this.id });
    this.MeetingsForm.patchValue({ AssignedTeam: TeamInfo.TeamId });
    this.MeetingsForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    this.MeetingsForm.patchValue({ Date: timestamp });

    this.meetingService.addMeeting(this.MeetingsForm.value).subscribe(data => {
      this.closeAddMeetingModal.nativeElement.click();
      if (data.Status === 'Success') {     
        this.getMeetings();
        this.MeetingsForm.reset();
      }
    });
  }

  addTask() {

    if (this.TaskForm.invalid) {
      return;
    }
    let date = new Date(this.bsValue);
    let fullDate = date.setHours(this.mytime.getHours(), this.mytime.getMinutes());
    let timestamp = Math.floor(new Date(fullDate).getTime() / 1000.0);
    let userObj = this.usersData.find(y => y.UserId == this.TaskForm.value.AssignedTo);
    this.TaskForm.patchValue({ DueDate: timestamp });
    this.TaskForm.patchValue({ ObjectId: this.activityresponse.OpportunityInfo.OpportunityId });
    this.TaskForm.patchValue({ ObjectIntId: this.id });
    this.TaskForm.patchValue({ AccountId: this.activityresponse.ContactInfo.id });
    this.TaskForm.patchValue({ AssignedToIntId: JSON.stringify(userObj.IntUserId) });
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    this.TaskForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.TaskForm.patchValue({ AssignedTeamIntId: TeamInfo.Id });
    this.taskService.addTask(this.TaskForm.value).subscribe(data => {
      this.loggedInUserRole === 'ta'?this.tservice.TeamAdminTaskReload():this.tservice.adminTaskReload();   
      this.countries$ = this.tservice.countries$;
      this.total$ = this.tservice.total$;
      this.priorityCollection$ = this.tservice.priorityCollection$;
      this.closeAddTaskModal.nativeElement.click();
    });
  }

  phoneKeyPressHandler(event: any) {
    const pattern = /[0-9\+\-\ ]/;  
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  onselecteddays(event:any){
  }
  onselectedhours(event:any){
  }
  onselectedmins(event:any){
  }
  addOpportunity() {
    if (this.opportunityForm.invalid) {
      return;
    }
    
    let TeamInfo = JSON.parse(localStorage.getItem('teaminfo'));
    let date = new Date(this.bsValue);
    let fullDate = date.setHours(this.mytime.getHours(), this.mytime.getMinutes());
    let timestamp = Math.floor(new Date(fullDate).getTime() / 1000.0);
    if(this.loggedInUserRole ==='ta'||this.loggedInUserRole ==='admin'){
    let userObj = this.Users.find(y => y.UserId == this.opportunityForm.value.AssignedToId);
    this.UserIntId=JSON.stringify(userObj.IntUserId) ;
    }
    else
    this.UserIntId=this.userInfo.UserIntId;
    this.opportunityForm.patchValue({ TargetDate: timestamp });
    this.opportunityForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.opportunityForm.patchValue({ TeamIntId: TeamInfo.Id });
    this.opportunityForm.patchValue({ AssignedToIntId:  this.UserIntId });
    this.opportunityService.AddOpportunity(this.opportunityForm.value).subscribe(data => {
      if(this.loggedInUserRole === 'ta'){
      this.oppService.TeamAdminOpportunityReload();
      }else if(this.loggedInUserRole === "member"){
        this.oppService.MemberOpportunityReload();
      }
      else{
      this.oppService.addOpportunityReload();   
      }
      this.opportunities$ = this.oppService.opportunities$;
      this.total$ = this.oppService.total$;
      this.stageCollection$ = this.oppService.stageCollection$;
      if (data.Status === 'Success') {
        this.closeAddOpportunityModal.nativeElement.click();
      } else {
      //  alert(data.Message);
      }
    });
  }


  updateOpportunity(){
    if(this.loggedInUserRole ==='ta'||this.loggedInUserRole ==='admin'){
    let userObj = this.Users.find(y => y.UserId == (<HTMLInputElement>document.getElementById("owner")).value);
    this.UserIntId=JSON.stringify(userObj.IntUserId) ;
    }
    else
    this.UserIntId=this.userInfo.UserIntId;
    let updateObj={
      AssignedToIntId:this.UserIntId ,
      Id:this.id,
      Stage: (<HTMLInputElement>document.getElementById("stage")).value,
      Status:(<HTMLInputElement>document.getElementById("status")).value,
      Title:(<HTMLInputElement>document.getElementById("title")).value,
      Email:(<HTMLInputElement>document.getElementById("email")).value,
      PhoneNo:(<HTMLInputElement>document.getElementById("phoneno")).value,
      AssignedToId:(<HTMLInputElement>document.getElementById("owner")).value,
      TargetDate:(<HTMLInputElement>document.getElementById("targetdate")).value,
      Value:(<HTMLInputElement>document.getElementById("value")).value
    }
    this.opportunityService.UpdateOpportunity(updateObj).subscribe(data => {
      if(data.Status === 'Success')
      {
        this.editModalRef.hide();
         this.getOpportunityById();
      }
    });
  }

  }
