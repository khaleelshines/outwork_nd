import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { GridOptions } from "ag-grid-community";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ContactApiService } from "src/services/contacts.services";
import { AccountApiService } from "src/services/accounts.services";
import { AccountsActionsComponent } from "../custom_components/accounts-actions/accounts-actions.component";
import { UserService } from "src/services/userservice.services";
import { OpportunityApiService } from "src/services/opportunities.services";
import { Observable, BehaviorSubject, Subscription } from "rxjs";
import { BsModalRef } from "ngx-bootstrap/modal";
import { AuthService } from "../guards/authguard/auth.service";
import { OpportunityService1 } from "./opportunity.service";
import { teammanagementservice } from "src/services/teamsmanagement.services";
import { ContactsComponent } from "../contacts/contacts.component";
import { SequenceComponent } from "../sequence/sequence.component";
import { DecimalPipe } from "@angular/common";
import { NgxSpinnerService } from "ngx-spinner";

interface Opportunity {
  OpportunityId: string;
  CompanyName: string;
  ContactName: string;
  ContactDesignation: string;
  AccountName: string;
  ShortContactName: string;
  Title: string;
  Stage: string;
  Status: string;
  TargetDate: string;
  Value: number;
  CreatedDate: string;
}

interface stage {
  stage: string;
  stagecount: number;
}

@Component({
  selector: "app-opportunities",
  templateUrl: "./opportunities.component.html",
  styleUrls: ["./opportunities.component.css"],
  providers: [DecimalPipe, ContactsComponent, SequenceComponent]
})
export class OpportunitiesComponent implements OnInit,OnDestroy {
  opportunityForm: FormGroup;
  opportunities: any;
  Contacts: any;
  Users: any;
  submitted = false;
  public rowData: any;
  bsValue = new Date();
  stageCollection$: Observable<stage[]>;
  opportunities$: Observable<Opportunity[]> = new BehaviorSubject<Opportunity[]>([]);
  total$: Observable<number>;
  mytime: Date = new Date();
  mytime1: Date = new Date();
  IsMember: boolean;
  loggedInUserRole: string;
  headerValues: any;
  memberName: string;
  userInfo: any;
  UserIntId: string;
  subscriptions:Subscription [] = [];
  //#region contacts dropdown
  contactsList:any;
  selContactInfo:any;
  contactObj:any;
  contactnamerequired:boolean = false;
  //#endregion

  get f() {
    return this.opportunityForm.controls;
  }
  @ViewChild("closeAddOpportunityModal", { static: false })
  closeAddOpportunityModal: ElementRef;

  constructor(
    public oppService: OpportunityService1,
    private formBuilder: FormBuilder,
    private router: Router,
    private opportunityService: OpportunityApiService,
    private userService: UserService,
    private contactService: ContactApiService,
    public auth: AuthService,
    private contacts: ContactsComponent,
    private teamService: teammanagementservice,
    private spinner: NgxSpinnerService
  ) {
    this.loggedInUserRole = this.auth.getRole();
    this.headerValues = [
      "",
      "Contact Name",
      "Comapny Name",
      "Title",
      "Created On",
      "Value",
      "Stage",
      "Status",
      "Target Date"
    ];

    if (this.loggedInUserRole === "ta") {
      this.oppService.TeamAdminOpportunityReload();
    } else if (this.loggedInUserRole === "member") {
      this.oppService.MemberOpportunityReload();
    } else {
      this.oppService.addOpportunityReload();
    }

    this.opportunities$ = this.oppService.opportunities$;
    this.total$ = this.oppService.total$;
    this.stageCollection$ = this.oppService.stageCollection$;
  }


  phoneKeyPressHandler(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  ngOnInit() {
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 2000);

    if (!window.localStorage.getItem("token")) {
      this.router.navigate(["login"]);
    }

    this.getContacts();

    // if (this.loggedInUserRole === "ta") {
    //   this.teamService.getTeamMembersWithoutTeamId().subscribe(data => {
    //     this.Users = data;
    //   });
    // } else 
    if (this.loggedInUserRole === "member") {
      this.IsMember = true;
      let userDetails = localStorage.getItem("userInfo");
      let userDetails1 = JSON.parse(userDetails);
      this.userInfo = {
        Name: userDetails1.Name,
        UserId: userDetails1.UserId,
        UserIntId: userDetails1.IntUserId
      };
    } else {
     let teamMembersSub = this.teamService.getTeamMembersWithoutTeamId().subscribe(data => {
        this.Users = data;
      });
      this.subscriptions.push(teamMembersSub);
    }
    this.opportunityForm = this.formBuilder.group({
      ContactIntId: [''],
      Title: ['', Validators.required],
      Value: ['', Validators.required],
      Email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$")
        ]],
      PhoneNo: [],
      Stage: ['', Validators.compose([Validators.required])],
      AssignedToId: ['', Validators.compose([Validators.required])],
      Status: ['', Validators.compose([Validators.required])],
      TargetDate: [],
      TeamIntId: [],
      TeamId: [],
      AssignedToIntId: []
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }

  //#region contacts dropdown
  selectContact(){    
     console.log(this.selContactInfo);
   }

  contactChangeHandler(selectedObject: any) {
    let searchTerm = selectedObject.term?selectedObject.term:'';   
    let contactsArray = [];
    if (searchTerm != undefined && searchTerm.length >= 3) {
      this.contactnamerequired = false;
      let accountsSub = this.contactService.getContactsbyname(searchTerm,0).subscribe(data => {
        if (data.Status == "Success") {
          let accounts = JSON.parse(data.Data);
          accounts.forEach(element => {
            let singleItem: any;
            singleItem = {
              id: element.id,
              name: element.contactname,
              accountid: element.accountintid
            };
            contactsArray.push(singleItem);
          });
          this.contactsList = contactsArray;
          
        }
      });
      this.subscriptions.push(accountsSub);
    }

  }

    getContactInfoById(contactIntId:number):any{
     this.contactService.getContactView(contactIntId).subscribe(data => {
        if(data.Status == "Success"){
        this.contactObj = JSON.parse(data.Data);         
       // this.contactsList = contactsArray;         
        }
       });
    }
  //#endregion

 

  getContacts() {
   let contactsSub = this.contactService.getContactsrawdata().subscribe(data => {
      let contactedContactsMasterInfo: any = JSON.parse(data.Data);
      this.Contacts = contactedContactsMasterInfo.ListOfContactedContacts;
      console.log(this.Contacts);
    });
    this.subscriptions.push(contactsSub);
  }

  contactedFilter(clickedText) {}

  addOpportunity() {
    this.submitted = true;
    let accountIntId = 0;
    let ContactIntId = 0;
    if(this.selContactInfo != undefined && this.selContactInfo.id > 0){  
      accountIntId = this.selContactInfo.accountid;
      ContactIntId = this.selContactInfo.id;
     
    }else{
      this.contactnamerequired = true;
      return;
    }
    if (this.opportunityForm.invalid) {
      return;
    }

    this.opportunityForm.patchValue({ ContactIntId: ContactIntId });
    let TeamInfo = JSON.parse(localStorage.getItem("teaminfo"));
    let date = new Date(this.bsValue);
    let fullDate = date.setHours(
      this.mytime.getHours(),
      this.mytime.getMinutes()
    );
    let timestamp = Math.floor(new Date(fullDate).getTime() / 1000.0);
    if (this.loggedInUserRole === "ta" || this.loggedInUserRole === "admin") {
      let userObj = this.Users.find(
        y => y.UserId == this.opportunityForm.value.AssignedToId
      );
      this.UserIntId = JSON.stringify(userObj.IntUserId);
    } else {
      this.UserIntId = this.userInfo.UserIntId;
    }
    this.opportunityForm.patchValue({ TargetDate: timestamp });
    this.opportunityForm.patchValue({ TeamId: TeamInfo.TeamId });
    this.opportunityForm.patchValue({ TeamIntId: TeamInfo.Id });
    this.opportunityForm.patchValue({ AssignedToIntId: this.UserIntId });
    this.opportunityService
      .AddOpportunity(this.opportunityForm.value)
      .subscribe(data => {
        if (this.loggedInUserRole === "ta") {
          this.oppService.TeamAdminOpportunityReload();
        } else if (this.loggedInUserRole === "member") {
          this.oppService.MemberOpportunityReload();
        } else {
          this.oppService.addOpportunityReload();
        }
        this.opportunities$ = this.oppService.opportunities$;
        this.total$ = this.oppService.total$;
        this.stageCollection$ = this.oppService.stageCollection$;
        if (data.Status === "Success") {
          this.onReset();
          this.closeAddOpportunityModal.nativeElement.click();
        } else {
          //  alert(data.Message);
        }
      });
  }

  onReset() {
    this.submitted = false;
    this.bsValue = new Date();
    this.opportunityForm.reset();
    this.contactsList = [];
    this.selContactInfo = [];
}
}
