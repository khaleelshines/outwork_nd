import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  TemplateRef,
  OnDestroy
} from "@angular/core";
import { templateService } from "src/services/templates.services";
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import Froalaeditor from "froala-editor";
import "froala-editor/js/plugins/link.min.js";
import "froala-editor/js/plugins/image.min.js";
import "froala-editor/js/plugins/file.min.js";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { JsonPipe } from "@angular/common";
import { GridOptions, CellClickedEvent } from 'ag-grid-community';
import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from 'rxjs';
import { AgGridAngular } from 'ag-grid-angular';
import { TemplategridactionsComponent } from '../templates/templategridactions/templategridactions.component';

@Component({
  selector: "app-templates",
  templateUrl: "./templates.component.html",
  styleUrls: ["./templates.component.css"]
})
export class TemplatesComponent implements OnInit,OnDestroy {
  templates: any;
  singleTemplateData: any;
  nonFilteredTemplates: any;
  templateForm: FormGroup;
  templateEditForm: FormGroup;
  submitted = false;
  options;
  deleteTemplatemodalRef: BsModalRef;
  updateTemplatemodalRef: BsModalRef;
  clickedTemplateId: string;
  subscriptions:Subscription [] = [];

  
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public autoGroupColumnDef;
  public defaultColDef;
  public rowSelection;
  public rowGroupPanelShow;
  public pivotPanelShow;
  public sideBar;
  public rowData: any;
  public contactsData: any;
  paginationPageSize: number;
  cacheBlockSize: number;

  //#region template
  responseInfo:string;

  //#endregion

  //#region modal
  modalResponseRef:BsModalRef;
  //#endregion
  get f() {
    return this.templateForm.controls;
  }
  get f1() {
    return this.templateEditForm.controls;
  }
  @ViewChild("closeAddTemplateModal", { static: false })
  closeAddTemplateModal: ElementRef;

  constructor(
    private templateApiService: templateService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: BsModalService,
    private spinner: NgxSpinnerService,
    private modalBsServiceRef:BsModalService,
  ) {
    
    this.columnDefs = [
          {
            headerName: "Template Name",
            field: "TemplateName",
            width: 450,
            // headerCheckboxSelection: true,    
            // checkboxSelection: true,
          },
          {
            headerName: "Template Subject",
            field: "Subject",
            width: 400
          },    
          {
            headerName: "Created On",
            field: "ElapsedDuration",
            width: 200
          },{
            headerName:'Actions',
            width: 250,
            cellRendererFramework: TemplategridactionsComponent
          }
        ];

        this.defaultColDef = {
          editable: false,
          enableRowGroup: true,
          enablePivot: false,
          enableValue: true,
          sortable: true,
          resizable: true,
          filter: true
    
        };
     
        this.rowSelection = "multiple";
        this.rowGroupPanelShow = "false";
        this.pivotPanelShow = "always";
        this.cacheBlockSize = 10;
        this.paginationPageSize = 10; 

  }

  gridOptions: GridOptions = {
    rowHeight: 50,
    headerHeight: 52,
  };
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  //  this.getContactsData();
  }

  ngOnInit() {
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 2000);

    this.templateForm = this.formBuilder.group({
      TemplateName: ["", Validators.required],
      Subject: ["", Validators.required],
      HtmlTemplateBody: ["", Validators.required],
      MailType: "contacttemplate"
    });

    this.templateEditForm = this.formBuilder.group({
      TemplateName: ["", Validators.required],
      Subject: ["", Validators.required],
      HtmlTemplateBody: ["", Validators.required]
    });

    this.getTemplates();

    Froalaeditor.DefineIcon("custom-field", { NAME: "cog", SVG_KEY: "cogs" });
    Froalaeditor.RegisterCommand("custom-field", {
      title: "Personalize",
      type: "dropdown",
      focus: false,
      undo: false,
      refreshAfterCallback: true,
      options: {
        "{{Contact: First Name}}": "First Name",
        "{{Contact: Last Name}}": "Last Name",
        "{{Contact: Designation}}": "Designation",
        "{{Contact: PhoneNumber}}": "PhoneNumber",
        "{{Account: AccountName}}": "AccountName",
        "<a href>unsubscribe now</a>": "Unsubscribe"
      },
      callback: function(cmd, val) {
        this.html.insert(val);
      }
    });

    this.options = {
      key: "bTYPASIBGMWC1YLMP==",
      placeholderText: "Edit Your Content Here!",
      height: 200,
      toolbarBottom: true,
      inlineMode: false,
      toolbarButtons: [
        "bold",
        "italic",
        "underline",
        "custom-field",
        "strikeThrough",
        "insertLink",
        "insertImage",
        "insertFile"
      ],
      imageUploadToS3: {
        bucket: "outworksales",
        region: "s3-ap-south-1",
        keyStart: "images/",
        acl: "public-read",
        accessKey: "AKIAZHEALBC223YK2MJ6",
        secretKey: "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"
      },
      fileUploadToS3: {
        bucket: "outworksales",
        region: "s3-ap-south-1",
        keyStart: "documents/",
        params: {
          acl: "public-read", // ACL according to Amazon Documentation.
          accessKey: "AKIAZHEALBC223YK2MJ6",
          secretKey: "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"
        }
      }
    };
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }

  SearchTemplateData(event: any) {
    let searchTerm = event.target.value;
    if (searchTerm.length >= 2) {
      this.templates = this.nonFilteredTemplates.filter(a => {
        const term = searchTerm.toLowerCase();
        return (
          a.TemplateName.toLowerCase().includes(term) ||
          a.Subject.toLowerCase().includes(term) ||
          a.ElapsedDuration.toLowerCase().includes(term)
        );
      });
    } else {
      this.templates = this.nonFilteredTemplates;
    }
  }

  clearSearch(event: any) {
    //alert(event.type);
    //if (event.type === "mouseup") {
      let searchText = event.currentTarget.value;
      if (searchText != "") {
        this.gridOptions.api.setQuickFilter(searchText);
      } else {
        this.gridOptions.api.setQuickFilter(null);
        // this.gridOptions.api.onFilterChanged();
      }
     // this.templates = this.nonFilteredTemplates;
    //}
  }

  IsSubscribeChange(event: any) {
    let isChecked = event.target.checked;
    if (isChecked === true) {
    } else {
    }
  }

  getTemplates(): any {
  let templatesSub =  this.templateApiService.getTemplates().subscribe(data => {
      if (data.Status === "Success") {
        this.templates = JSON.parse(data.Data);
        this.rowData = JSON.parse(data.Data);
        this.nonFilteredTemplates = this.templates;
      } else {
        this.templates = undefined;
      }
    });
    this.subscriptions.push(templatesSub);
  }

  AddTemplate(template: TemplateRef<any>) {
    this.submitted = true;
    if (this.templateForm.invalid) {
      return;
    }
    this.templateForm.patchValue({ MailType: "contacttemplate" });
    this.templateApiService
      .postTemplate(this.templateForm.value)
      .subscribe(data => {
        if (data.Status === "Success") {         
          this.templateApiService.getTemplates().subscribe(data => {
            if (data.Status === "Success") {
              this.templates = JSON.parse(data.Data);
              this.nonFilteredTemplates = this.templates;
              this.onReset();
            }
          });
        }else if(data.Code === 'ERR-4001' && data.Data === 'duplicate'){
          this.responseInfo = `<div class="alert alert-danger mb-0">     
          <h6>Template name is already exists, Please try with another one..!</h6> 
                </div> `;  
        this.modalResponseRef = this.modalBsServiceRef.show(template, { class: "modal-m" });
        setTimeout(() => {
          this.modalResponseRef.hide();
        }, 3000);     
        }      
        this.closeAddTemplateModal.nativeElement.click();
        
 
      });
  }
  UpdateTemplate() {
    this.submitted = true;
    if (this.templateEditForm.invalid) {
      return;
    }

    let editFormObject = {
      TemplateName: (<HTMLInputElement>document.getElementById("templateName")).value,
      Subject: (<HTMLInputElement>document.getElementById("subject")).value,
      HtmlTemplateBody: (<HTMLInputElement>document.getElementById("body")).value,
      TemplateId: this.clickedTemplateId
    };
    // alert(JSON.stringify(editFormObject));
    this.templateApiService.UpdateTemplate(editFormObject).subscribe(data => {
      if (data.Status === "Success") {
        this.closeAddTemplateModal.nativeElement.click();
        this.templateApiService.getTemplates().subscribe(data => {
          if (data.Status === "Success") {
            this.templates = JSON.parse(data.Data);
          }
          this.updateTemplatemodalRef.hide();
          this.getTemplates();
        });
      }else{        
        alert("Template updatation failed..!");
      }
    });
  }

  deleteTemplateBtn(template: TemplateRef<any>, templateId: string) {
    this.clickedTemplateId = templateId;
    this.deleteTemplatemodalRef = this.modalService.show(template, {
      class: "second"
    });
  }

  deleteTemplateYes() {
    this.templateApiService
      .deleteTemplate(this.clickedTemplateId)
      .subscribe(data => {
        if (data.Status.toLowerCase() === "success") {
          this.deleteTemplatemodalRef.hide();
          this.getTemplates();
        }
      });
  }

  onReset() {
    this.submitted = false;
    this.templateForm.reset();
  } 
}
