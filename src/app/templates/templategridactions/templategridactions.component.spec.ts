import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplategridactionsComponent } from './templategridactions.component';

describe('TemplategridactionsComponent', () => {
  let component: TemplategridactionsComponent;
  let fixture: ComponentFixture<TemplategridactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplategridactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplategridactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
