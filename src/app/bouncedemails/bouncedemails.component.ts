import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.services';
import { EmailApiService } from 'src/services/emails.services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bouncedemails',
  templateUrl: './bouncedemails.component.html',
  styleUrls: ['./bouncedemails.component.css']
})
export class BouncedemailsComponent implements OnInit,OnDestroy {
  
 emailAddress:string;
 isValidateEmail:boolean=true;
 responseStatus:boolean = false;
 updateCountStatus:boolean =false;
 isEmailAddressConfirmed:boolean = false;
 askForUnsubscribe:boolean = true;
 primaryInfoContainer:boolean = true;
 email:string = "";
 subscriptions:Subscription [] = [];
  constructor(
    private route:ActivatedRoute,
    private utilities:UtilitiesService,
    private emailservice:EmailApiService
  ) { }

 ngOnInit() {
 this.updateCountStatus = false;
 //  let emailSubscription = this.route.params.subscribe(params => {
     // this.emailAddress = params["email"];  
      // let emailValidation = this.utilities.EmailValidatorWithReturn(this.emailAddress);  
      // if(!emailValidation){
      //    this.isValidateEmail = false;
      // }else{
      //   this.isValidateEmail = true;
      //      this.insertBouncedEmail(this.emailAddress);
      // }
  //  });

   // this.subscriptions.push(emailSubscription);
  }

  ngOnDestroy(): void {
  //  this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  txtEmailValidate(){
    this.isValidateEmail = true;
  }

  yesToConfirmEmail(){   
   this.isEmailAddressConfirmed = true;
   this.askForUnsubscribe = false;
  }

  CloseWindow(){
    window.open("", "_self").close();
    }

  unSubscribeEmail(){    
    this.emailAddress = this.email;
    let emailValidation = this.utilities.EmailValidatorWithReturn(this.email);  
      if(!emailValidation){
         this.isValidateEmail = false;
      }else{
        this.isValidateEmail = true;
           this.insertBouncedEmail(this.emailAddress);
      }
  }

  insertBouncedEmail(emailAddress:string){
    this.emailservice.insertBouncedEmail(emailAddress).subscribe(data=>{
       if(data.Status === "Success" && data.Code === "SUC-200"){
         this.responseStatus = true;
       }else if(data.Status === "Success" && data.Code === "UP-200"){
        this.responseStatus = false;
        this.updateCountStatus = true;
       }
    });
    this.primaryInfoContainer = false;

  }

}
