export class ApiResponse {
    Code: string;
    Status: string;
    Data: string;
    Message: string;
    Description: string;
    HelpUrl: string;
}
