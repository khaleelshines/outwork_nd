import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
@Injectable()
export class MeetingApiService {

    constructor(private http: HttpClient) { }
    // baseUrl = 'http://localhost:60000/admin/v1/meeting';
    //  baseUrl = 'http://outworkapi.useguild.com:9500/admin/v1/company';
    baseUrl = environment.baseUrl;
    addMeeting(Meetings: any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/meeting', Meetings);
    }

    markMeetingDone(inputObject:any): Observable<ApiResponse> {
        return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/meeting/completed', inputObject);
    }

    getMeetingsByObjectId(id: any): Observable<ApiResponse> {
        return this.http.get<any>(this.baseUrl + 'admin/v1/meeting?objectId=' + id).pipe(map(res => {
            if (res.Code == "SUC-200") {
                return JSON.parse(res.Data);
            }
        }));
    }

    getMeetingsByAccountId(id:number):Observable<ApiResponse>{
        return this.http.get<ApiResponse>(this.baseUrl+"admin/v1/meeting?accountintid="+id);
        }

    getAllMeetings():Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl+"admin/v1/meeting/all");
    }

}