import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class OpportunityApiService {
  
  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl

  AddOpportunity(Opportunity:any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/opportunity', Opportunity);
  }
  GetOpportunities():Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/opportunity');
  }
  GetOpportunitiesWithFileters(inputObj):Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/opportunity/filters',inputObj);
  }
  GetOpportunityActivityCount(id: any): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/opportunity/activitycount?opportunityid=' + id);
  }
  GetOpportunityByOpportunityId(id: any):Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/opportunity/id?opportunityId='+id);
  }
  GetOpportunityByObjectIntId(id: number):Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/opportunity?objectintid='+id);
  }
  UpdateOpportunity(updateObj:any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/opportunity/update', updateObj);
  }
}
