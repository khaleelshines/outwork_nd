import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class CallsApiService {
    constructor(private http: HttpClient) { }
    baseUrl = environment.baseUrl;


    addCalls(Calls: any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/calls', Calls);
    }

    getCalls(id: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/calls?contactIntId=' + id);
    }

    getAllCalls(): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/calls/all');
    }

    getCallsByAccountId(id: any): Observable<ApiResponse> {
        return this.http.get<any>(this.baseUrl + 'admin/v1/calls?accountintid=' + id).pipe(map(res => {
            if (res.Code == "SUC-200") {
                return JSON.parse(res.Data);
            }
        }));
    }
}
