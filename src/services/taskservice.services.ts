import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { BaseApiService } from './environment.servicebaseurls';
import { environment } from 'src/environments/environment';

@Injectable()
export class TaskApiService {

    constructor(private http: HttpClient) { }
    //  baseUrl = BaseApiService.BaseUrl;
    baseUrl = environment.baseUrl;
    //  baseUrl = 'http://localhost:60000/admin/v1/task';    
    // baseUrl = 'http://outworkapi.useguild.com:9500/admin/v1/task';


    getTasks(): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/task');
    }

    getTasksWithFilters(inputObj): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/task/filters', inputObj);
    }

    addTask(Tasks: any): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/task', Tasks);
    }

    updateTaskInfo(taskdata: any): Observable<ApiResponse> {
        return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/task/update/taskinfo', taskdata);
    }
    getTaskByObjectId(id: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/task?objectintid=' + id);
    }
    getTaskByAccountId(id: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/task?accountintid=' + id);
    }

    updateTaskAsCompleted(id: any, taskintid: any): Observable<ApiResponse> {
        return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/task/markascomplete?sequenceDetailId=' + id + "&taskintid=" + taskintid, null);
    }

}

