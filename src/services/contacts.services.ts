import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { BaseApiService } from './environment.servicebaseurls';
import { environment } from 'src/environments/environment';

@Injectable()
export class ContactApiService {


  constructor(private http: HttpClient) { }  
  baseUrl = environment.baseUrl;
  AddContact(Contacts: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/contacts', Contacts);
  }

  getContacts(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts');
  }

  // Khaleel test call for ag-grid
  getContactslist(params, pagelimit,gSearchValue,mainTab,periodType): Observable<ApiResponse> {
    let sortData = params.sortModel.length > 0 ? params.sortModel[0] : "";
    let sortcol = sortData == "" ? "" : sortData.colId;
    let sortdir = sortData == "" ? "" : sortData.sort;
    let searchcol = Object.keys(params.filterModel).length > 0 ? Object.keys(params.filterModel)[0] : "";
    let searchkey = searchcol != "" ? params.filterModel[searchcol] : "";
    let searchvalue = searchkey != "" ? searchkey.filter : "";
    let searchtype = searchkey != "" ? searchkey.type : "";
    let serverinputObj ={
      startvalue:params.startRow,
      limit:pagelimit,
      sortcol:sortcol,
      sortdir:sortdir,
      searchcol:searchcol,
      searchkey:searchvalue,
      searchtype:searchtype,
      globalsearch:gSearchValue,
      periodcol:mainTab,
      periodtype:periodType
    };
    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/contacts/contactsinfo',serverinputObj);
   
  }

  updateContactInfo(contactdata: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/contacts/update/contactinfo', contactdata);
  }

  getContactInfo(contactid): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/contactviewdata?contactid=' + contactid);
  }

  getContactsByTimeFilter(type): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/date?type=' + type + '&startDate=&endDate=');
  }

  getContactsStageInfo(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/stageinfo');
  }

  getContactsrawdata(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/info');
  }

  getContactView(contactintid:number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/contactview?contactintid='+contactintid);
  }

  getContactsbyname(term:string,id:number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/filter?term='+term+'&id='+id);
  }

  getSequenceContacts(sequenceIntId:number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/sequencecontacts?sequenceintid='+sequenceIntId);
  }

  getStepContacts(sequenceIntId:number,stepid:number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/sequencestep/contacts?sequenceintid='+sequenceIntId+'&stepintid='+stepid);
  }

  getSequenceStepContacts(sequenceIntId:number,stepid:number,type:string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/sequence/stepcontacts?sequenceintid='+sequenceIntId+'&stepid='+stepid+'&type='+type);
  }

  // Testing call for server side processing
  getCustomers(): Observable<ApiResponse> {
    // const headers = new HttpHeaders({ 'Content-Type': 'application/json','utoken':'6A510FC649D55EEBEEC41CD7DAA9BB912125FCD5C6DD4CBA7055B7B491A0AE409486B808E8364C213477FC9BAA9B0F42B2EDAA3121FD3253F4594D11FA1E2E4D6228B98CD2A6C9370AD40E5329FDF812E023AD6A2A2129A6EE1C3528A4B85D7A9AF3B3359483676E3C947711DAB463FF0D43F5D51836F72E2FE976ECA6218CD04EFC8AB1217A185854356F867656DEC2793B869F07B33477F7D885C3ACBD805AAC35ED9BA4D425CDA43FE4C461B4571288431B8D1BDC0BABCF84C3AF2F6B3B1F1FD5919032EA35F1E19FACD0045709375213B515334117FA71F7A866D084FBF433484F3BF4D9584FF59B5029503B0AA006635A3ECA666EDB04C47ED4223C3FC92737DFFAED540E5CCC8DF987BFA5FF729778AC4C41DEB997480E5C45C793C4E05E906F587735F626E645F8172B23DF6D4552E62E9E0DF9E012829D7EC6C8BD99'});    
    return this.http.get<ApiResponse>('http://api.outwork.in/services/customer/admin/customers?groupid=dec590285a7e4c4ba07623abbe3edfd0&id=0&limit=50&key=');

  }


  assignSequence(objectId: any, sequenceId: any): Observable<ApiResponse> {
    // return this.http.post<ApiResponse>(this.sequenceBaseUrl+'/linksequencetocontact' + '?objectId=' + objectId + '&sequenceId=' + sequenceId, null);

    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/sequence/linksequencetocontact' + '?objectId=' + objectId + '&sequenceId=' + sequenceId, null);
  }

  searchContactByName(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts');
  }

  getContactActivityCount(id: any): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/activitycount?contactid=' + id);
  }

  updateAssignee(inputObject: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/contacts/update/assignee',inputObject);
  }

  contactActivity(contactid:any): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/activity?contactintid='+contactid);
  }

  isContactExists(email:string, recordId:number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/contacts/iscontactexists?email='+email+'&id='+recordId);
  }

  UpdateContact(Contacts: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/contacts/update', Contacts);
  }  

  DeleteContact(inputObj: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/contacts/delete', inputObj);
  }

}

