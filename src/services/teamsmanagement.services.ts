import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ApiResponse } from 'src/model/apiresponse.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class  teammanagementservice{
constructor(private http:HttpClient){}
baseUrl = environment.baseUrl;

getteams():Observable<ApiResponse>{
return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/teams/management');
}

addteam(postteam):Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.baseUrl+'admin/v1/teams',postteam);
}

deleteteam(teamobj):Observable<ApiResponse>{
    return this.http.put<ApiResponse>(this.baseUrl+'admin/v1/teams/deleteteam',teamobj);
}

getteammembers(teamid):Observable<any>{
        return this.http.get<any>(this.baseUrl+'admin/v1/teams/members?teamid='+teamid).pipe(map(res => {
            if (res.Code == "SUC-200") {
                return JSON.parse(res.Data);
            }
        }));
}

getTeamMembersWithoutTeamId():Observable<any>{
    return this.http.get<any>(this.baseUrl+'admin/v1/teams/members').pipe(map(res => {
        if (res.Code == "SUC-200") {
            return JSON.parse(res.Data);
        }
    }));
}

getTeamInfoById(teamid:number):Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/teams/teaminfo?teamid='+teamid)
}

getnonteammembers(usersList:any):Observable<ApiResponse>{
  return this.http.post<ApiResponse>(this.baseUrl+'admin/v1/teams/nonmembers',usersList);
}

addmemberstoteam(membersList:any):Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.baseUrl+'admin/v1/teams/members',membersList)
}

deletemembersfromteam(memberids:any):Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.baseUrl+'admin/v1/teams/deleteteammember', memberids);
}

updateteamadmin(userid,teamid):Observable<ApiResponse>{
    return this.http.put<ApiResponse>(this.baseUrl+'admin/v1/teams/update/teamadmin?teamid='+teamid+'&ownerid='+userid,null);
}


}