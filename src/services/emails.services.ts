import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class EmailApiService {

    constructor(private http: HttpClient) { }
  
    baseUrl = environment.baseUrl;
    getEmails(id: any): Observable<any> {
        return this.http.get<any>(this.baseUrl + 'admin/v1/email?objectId=' + id).pipe(map(res => {
            if (res.Code == "SUC-200") {
                return JSON.parse(res.Data);
            }
        }));
    }

    getEmailsByAccountId(id: number): Observable<any> {
        return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/email?accountintid=' + id);
    }

    getAllEmails():Observable<ApiResponse>{
        return this.http.get<ApiResponse>(this.baseUrl+"admin/v1/email/all");
     }

     getEmailActivitiesList(params, pagelimit,gSearchValue,emailType,periodType):Observable<ApiResponse>{
        let sortData = params.sortModel.length > 0 ? params.sortModel[0] : "";
        let sortcol = sortData == "" ? "" : sortData.colId;
        let sortdir = sortData == "" ? "" : sortData.sort;
        let searchcol = Object.keys(params.filterModel).length > 0 ? Object.keys(params.filterModel)[0] : "";
        let searchkey = searchcol != "" ? params.filterModel[searchcol] : "";
        let searchvalue = searchkey != "" ? searchkey.filter : "";
        let searchtype = searchkey != "" ? searchkey.type : "";
        let serverObject ={
          startvalue:params.startRow,
          limit:pagelimit,
          sortcol:sortcol,
          sortdir:sortdir,
          searchcol:searchcol,
          searchkey:searchvalue,
          searchtype:searchtype,
          globalsearch:gSearchValue,
          periodcol:emailType,
          periodtype:periodType
        };
        return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/email/activities',serverObject);
     }

    unSubscribeMails(contactid: string): Observable<ApiResponse> {
        return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/contacts/unsubscribeemail?contactId='+contactid,null);
    }


    updateEmailURLContentClicks(id:string,count:number):Observable<ApiResponse>{
        return this.http.put<ApiResponse>(this.baseUrl+"admin/v1/email/emailurl/clicked?urlid="+id+"&count="+count,null);
     }

    getEmailURLContentInfo(id:string):Observable<ApiResponse>{
       return this.http.get<ApiResponse>(this.baseUrl+"admin/v1/email/contenturlinfo?urlid="+id);
    }

    insertBouncedEmail(email:string):Observable<ApiResponse>{
        return this.http.post<ApiResponse>(this.baseUrl+'admin/v1/email/bounced?email='+email,null);
    }
}