import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { UserService } from './userservice.services';

@Injectable()
export class UtilitiesService {
isInValidEmail = false;

constructor(private http: HttpClient,public datepipe: DatePipe,
  private userService:UserService) { }

// focusOutFunction
EmailValidator(event: any) {
    let email = event.currentTarget.value;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isvalid = re.test(String(email).toLowerCase());
    if (isvalid == false) {
      this.isInValidEmail = true;
    } else {
      this.isInValidEmail = false;
    }
  }

  EmailValidatorWithReturn(emailAddress: string):boolean {    
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isvalid = re.test(String(emailAddress).toLowerCase());
   
    return isvalid;
  }

  //  users List
  getUsers():any {
    let usersArray =[];
    this.userService.getUsers().subscribe(data => {
      if (data.Status === "Success") {
        let userData = JSON.parse(data.Data);
 
        userData.forEach(element => {
          let singleItem:any;         
            singleItem ={
              id :element.IntUserId,
              name:element.Name,
            };       
         
          usersArray.push(singleItem);
        });
        return usersArray;
       }
    });
  }

  //phoneKeyPressHandler
  phoneNumberValidator(event: any) {
   // const pattern = /[0-9\+\-\ ]/;
    const pattern = /[0-9-\+\ ()]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }    
  }

  // short name from Full name
  getShortName(fullName) { 
    if(fullName != null && fullName != undefined){
    let firstLettersArray = fullName.split(' ').filter(m=>m!='').map(n => n[0]);
    let finalString:string;
    if(firstLettersArray.length > 2){
        finalString = firstLettersArray[0]+firstLettersArray[1];
    }else{
      finalString = firstLettersArray.join('');
    }
     return finalString;
  }
  }

  // check url and add
  checkUrl(url:string){
    var pattern = /^((http|https):\/\/)/;
    if(!pattern.test(url)) {
        url = "https://" + url;
    }
    return url;
  }

  // Time difference from two timestamps
  getDifference(date2:number,date1:number){
    var diff = date2.valueOf() - date1.valueOf();
     var diffInHours = diff/1000/60/60;
     return diffInHours;
  }

  // order by ascending
  sortBy(arrayList:any,field: string):any {
    return arrayList.sort((a: any, b: any) => {
         if (a[field] < b[field]) {
             return -1;
         } else if (a[field] > b[field]) {
             return 1;
         } else {
             return 0;
         }
     });    
 }

 // date conversion to standard format
 dateConvertion(inputDate:any):string{
  return this.datepipe.transform(inputDate, 'yyyy-MM-dd');
}

// date conversion to standard format dd-MM-yyyy
customDateConvertion(inputDate:any):string{
  return this.datepipe.transform(inputDate, 'dd-MM-yyyy');
}


thisWeekDates():any{
  var today = new Date();
  var day = today.getDay();

  var StartDate = new Date();
  var EndDate = new Date();
  StartDate.setHours(0,0,0,0);
  EndDate.setHours(0,0,0,0);
  StartDate.setDate(today.getDate()-day+1);
  EndDate.setDate(today.getDate()-day+7);
  return {
      startDate:StartDate,
      endDate: EndDate
  };
}

lastWeekDates():any{
  var today = new Date();
  var day = today.getDay();

  var StartDate = new Date();
  var EndDate = new Date();
  StartDate.setHours(0,0,0,0);
  EndDate.setHours(0,0,0,0);
  StartDate.setDate(today.getDate()-day-6);
  EndDate.setDate(today.getDate()-day);
  return {
      startDate:StartDate,
      endDate: EndDate
  };
}



}