import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from 'src/model/apiresponse.model';
import { environment } from 'src/environments/environment';


@Injectable()
export class RoleApiService {
  constructor(private http: HttpClient) { }

  baseUrl = environment.baseUrl;

   getcompanyroles(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl+'/admin/v1/roles/companyroles');
   }
  
}
