import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';


@Injectable()
export class ProfileApiService {

    constructor(private http: HttpClient) { }
    baseUrl = environment.baseUrl
    //baseUrl = 'http://localhost:60000/admin/v1/company';
     //baseUrl = 'http://outworkapi.useguild.com:9500/admin/v1/company';
   
    getCompanyProfileInfoById(): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/company/companyinfo');
    }

    updateCompanyProfileInfo(companyInfo:any): Observable<ApiResponse>{
        return this.http.put<ApiResponse>(this.baseUrl+'admin/v1/company/updatecompanyinfo', companyInfo);
        
    }
   

}
