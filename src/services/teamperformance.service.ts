import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ApiResponse } from 'src/model/apiresponse.model';
import { Observable } from 'rxjs';


@Injectable()
export class  teamPerformanceService{
constructor(private http:HttpClient){}
baseUrl = environment.baseUrl;

getTeamPerformance(period:string):Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/teams/performance?period='+period);
    }
}