import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ApiResponse } from 'src/model/apiresponse.model';
import { Observable } from 'rxjs';


@Injectable()
export class  reportsService{
constructor(private http:HttpClient){}
baseUrl = environment.baseUrl;

    getIndustriesReports():Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/reports');
    }
    getDesignationReports():Observable<ApiResponse>{
        return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/reports/designation');
        }
}