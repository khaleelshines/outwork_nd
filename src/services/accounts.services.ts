import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';


@Injectable()
export class AccountApiService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl;
  // baseUrl = 'http://outworkapi.useguild.com:9500/admin/v1/accounts';
  //baseUrl= 'http://outworkapi.useguild.com:9500/admin/v1/accounts';


  AddAccount(Accounts: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/accounts', Accounts);
  }

  getAccounts(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts');
  }

  getaccountslist(params, pagelimit,gSearchValue,mainTab,periodType): Observable<ApiResponse> {
    let sortData = params.sortModel.length > 0 ? params.sortModel[0] : "";
    let sortcol = sortData == "" ? "" : sortData.colId;
    let sortdir = sortData == "" ? "" : sortData.sort;
    let searchcol = Object.keys(params.filterModel).length > 0 ? Object.keys(params.filterModel)[0] : "";
    let searchkey = searchcol != "" ? params.filterModel[searchcol] : "";
    let searchvalue = searchkey != "" ? searchkey.filter : "";
    let searchtype = searchkey != "" ? searchkey.type : "";
    let serverinputObj ={
      startvalue:params.startRow,
      limit:pagelimit,
      sortcol:sortcol,
      sortdir:sortdir,
      searchcol:searchcol,
      searchkey:searchvalue,
      searchtype:searchtype,
      globalsearch:gSearchValue,
      periodcol:mainTab,
      periodtype:periodType

    };
    return this.http.post<ApiResponse>(this.baseUrl + 'admin/v1/accounts/list',serverinputObj);  

  }

  getBasicAccounts(term:string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/filter?term='+term);
  }
  IsAccountExists(name:string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/isaccountexists?accoutname='+name);
  }
  getAccountsStageInfo(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/stageinfo');
  }

  getAccountActivityCount(id: any): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/activitycount?accountintid=' + id);
  }

  getAccountActivityData(id: any): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'admin/v1/accounts/activitydata?accountintid=' + id);
  }

  getAccountsMasterTypes():Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl+"admin/v1/accounts/mastertypes");
  }

  UpdateAccount(Accounts: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/accounts/update', Accounts);
  }
  DeleteAccount(inputObj: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + 'admin/v1/contacts/delete', inputObj);
  }
}

