import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from 'src/model/apiresponse.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class templateService{
constructor (private http:HttpClient){}
// baseUrl = BaseApiService.BaseUrl;
baseUrl = environment.baseUrl;
getTemplates():Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl+"admin/v1/templates?mailtype=contacttemplate");
}

postTemplate(templateObj:any):Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.baseUrl+"admin/v1/templates/template",templateObj);
}

UpdateTemplate(templateObj:any):Observable<ApiResponse>{
    return this.http.put<ApiResponse>(this.baseUrl+"admin/v1/templates/updatetemplate",templateObj);
}

deleteTemplate(templateId:string):Observable<ApiResponse>{
    return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/templates/deletetemplate?templateid='+templateId);
}
}